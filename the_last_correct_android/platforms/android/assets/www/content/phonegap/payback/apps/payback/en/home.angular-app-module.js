




;(function (angular, undefined) {

    "use strict";

    // Cache killer is used to ensure we get the very latest content after an app update
    var cacheKiller = '?ck=' + (new Date().getTime());

    /**
     * Main app module
     */
     var PHONE_REGEXP = /^[(]{0,1}[0-9]{3}[)\.\- ]{0,1}[0-9]{3}[\.\- ]{0,1}[0-9]{4}$/;
    angular.module('PaybackApp', 




[
'ionic',
'ngCordova',
'ngRoute',
'cqContentSync',
'cqContentSyncUpdate',
'cqAppControllers',
'cqAppNavigation',
'tabSlideBox',
'angucomplete',
'vAccordion',
'ionicLazyLoad',
'ngIOS9UIWebViewPatch',
'ngAnimate',
'slick'
]
)

        .run(function($animate) {
        			$animate.enabled(false);
        })

        .controller('AppController', ['$scope', '$rootScope', 'cqContentSyncUpdate',
            function ($scope, $rootScope, cqContentSyncUpdate) {
                
                    
                    $scope.wcmMode = false;
                

                // URI for requesting OTA updates
                var contentSyncUpdateUri = 'https://mobi.payback.in/content/phonegap/payback/content/payback/payback-app-cli.zip';

                // Initialize the app updater
                cqContentSyncUpdate.setContentSyncUpdateConfiguration(contentSyncUpdateUri);
            }
        ])
        .controller('brandCtrl', ['$scope', '$rootScope', 'cqContentSyncUpdate','$compile','$filter',
            function ($scope, $rootScope, cqContentSyncUpdate,$compile,$filter) {
                
                    
                    $scope.wcmMode = false;
                


            }
        ])
        .controller('htmlController', ['$scope', '$rootScope', 'cqContentSyncUpdate','$compile','$filter',
            function ($scope, $rootScope, cqContentSyncUpdate,$compile,$filter) {
                
                    
                    $scope.wcmMode = false;
                


            }
        ])
        .directive('dir', function($compile, $parse) {
    return {
      restrict: 'E',
      link: function(scope, element, attr) {
        scope.$watch(attr.content, function() {
          element.html($parse(attr.content)(scope));
          $compile(element.contents())(scope);
        }, true);
      }
    }
  })
  .directive('dynamic', function ($compile) {
     return {
       restrict: 'A',
       replace: true,
       link: function (scope, ele, attrs) {
         scope.$watch(attrs.dynamic, function(html) {
           ele.html(html);
           $compile(ele.contents())(scope);
         });
       }
     };
   })

  .filter('datefilter', function($filter)
{
    return function(input)
    {
        if(input == null){ return ""; }
        var _date = $filter('date')(new Date(input), 'yyyy-MM-ddTHH:mm:ss');
        return _date.toUpperCase();
    };
})

.filter('storefilter', function() {
   return function( items, userAccessLevel ) {

      var filtered = [];
      var j=0;
      angular.forEach(items, function(item,i) {
		  if(i < items.length - 1 && i>0) {
			j = i-1;

            if(items[j]['pbstore_data']['PartnerShortName']!= items[i]['pbstore_data']['PartnerShortName'])
            {

                filtered.push(item);
            }
		}else
          {
				if(i==0)
                {
					filtered.push(item);
                }
          }



      });
      return filtered;
    };
})

.directive('paybacksmallcarousel',['$interval', function($interval) {
    return {
        template: '<ng-include src="getSmallTemplateUrl()"/>',
        restrict: 'E',
        scope: {
            smalldynamicid: '@',
            smalltitle:'@',
            smallheadingtitlecolor : '@',
            smallheadingtitlefont : '@',
            autoplaysmallcarousel : '@',
            smallheadingtitlealignment : '@',
            smallheadingstripcolor : '@',
            appExports :'@'
        },
        controller: function ($scope,$attrs,externalService,$timeout,$rootScope) {
            var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
            if ( app ) {
                $scope.appExports = true;
            }
            else {
                $scope.appExports = false;
            }
			$scope.smallCarouselServerUrl = localStorage.getItem("serverUrl");
            if(!localStorage.getCacheItem($attrs.smalldynamicid+"_smallCarousel")) {


                if ($scope.smallCarouselServerUrl == null || $scope.smallCarouselServerUrl == '') {
                    $scope.Url = $attrs.smallurl;
                } else {
                    $scope.Url = $scope.smallCarouselServerUrl+$attrs.smallurl;
                }
                var smallcarouselDataPromise =  externalService.carouselService($scope.Url,"",'GET',$attrs.smallresourcepath);

                smallcarouselDataPromise.then(function(smallcarouselResult) {
                    $scope.smallCarouselArray = smallcarouselResult["smallCarousel"]["carouselItemArray"];
                    if($scope.smallCarouselArray) {
                        localStorage.setCacheItem(smallcarouselResult["smallCarousel"]['dynamicId']+"_smallCarousel", JSON.stringify(smallcarouselResult), { hours: 2 });
                        $scope.smallCarouselArray = smallcarouselResult;
                    }
                });
            }
            else {
				var smallCarouselValue = localStorage.getCacheItem($attrs.smalldynamicid+"_smallCarousel");
				$scope.smallCarouselArray = JSON.parse(smallCarouselValue);
            }

            $scope.redirection = function(isExternal,path,isWeakLoginRequired) {
                // if(window.ADB){
                //     ADB.trackAction('smallCarousel');
                // }

                if(isWeakLoginRequired && isWeakLoginRequired=='true') {
                    var weakToken  =  localStorage.getItem('weaklogintoken');
                    var strongToken = localStorage.getItem('stronglogintoken');
                    if(localStorage.getItem("PBCardNum")) {
                        var cardNumber = localStorage.getItem("PBCardNum");
                    }
                    if(weakToken == null&&strongToken==null) {
                        $rootScope.redirectionIsExternal = isExternal;
                        $rootScope.redirectedFromCarousel = true;
                        $rootScope.weakLoginRedirection(path);
                    }
                    else {
                        $rootScope.trackBanner(path);
                        if(path.indexOf('TEMPXYZ786') > -1) {
                            var timeStamp = Date.now();
                            if(cardNumber.length <= 10) {
                                var tokenValue ="PBA_oooooo"+cardNumber+"_"+timeStamp+"_";
                            }
                            else {
                                var tokenValue ="PBA_"+cardNumber+"_"+timeStamp+"_";
                            }
                            path = path.replace('TEMPXYZ786',tokenValue);
                        }
                        if(isExternal == "true"){
                            if(path.indexOf('http') > -1 && path.indexOf('rewards') > -1){
                                var tokenVal = localStorage.getItem('stronglogintoken');
                                if(tokenVal){
                                    tokenVal = tokenVal.substring(1, tokenVal.length-1);
                                    if(path.indexOf('?') > -1){
                                        path = path + "&tokendata=" + tokenVal;
                                    }else{
                                        path = path + "?tokendata=" + tokenVal;
                                    }
                                }
                            }
                            window.open(path,"_system");
                        }
                        else {
                            $rootScope.go(path);
                        }
                    }
                }
                else if(isExternal == "true") {
                    //$window.location.href = path;
                    $rootScope.trackBanner(path);
                    if(path.indexOf('http') > -1 && path.indexOf('rewards') > -1){
                        var tokenVal = localStorage.getItem('stronglogintoken');
                        if(tokenVal){
                            tokenVal = tokenVal.substring(1, tokenVal.length-1);
                            if(path.indexOf('?') > -1){
                                path = path + "&tokendata=" + tokenVal;
                            }else{
                                path = path + "?tokendata=" + tokenVal;
                            }
                        }
                    }
                    window.open(path,"_system");
                }
                else {
                    $rootScope.go(path);
                }
            }

            $scope.getSmallTemplateUrl = function() {
                if(!localStorage.getItem("serverUrl")) {
                    return "/content/phonegap/payback/content/payback/phonegap/www/templates/smalltemplate.html";
                }
                else {
                    return "../../../../../../templates/smalltemplate.html";
                }
            }
        },

        link: function(scope, element, attrs) {

        }
    }
}])







.directive('paybackcarousel',['$interval', function($interval) {
    return {
        template: '<ng-include src="getCarouselTemplateUrl()"/>',
        restrict: 'E',
        scope: {
            dynamicid: '@',
            heading:'@',
            headingtitlecolor : '@',
            headingtitlefont : '@',
            autoplaycarousel : '@',
            headingtitlealignment : '@',
            headingstripcolor : '@',
            appExports :'@'
        },
        controller: function ($scope,$attrs,externalService,$timeout,$rootScope) {
            var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
            if ( app ) {
                $scope.appExports = true;
            }
            else {
                $scope.appExports = false;
            }
			$scope.carouselServer = localStorage.getItem("serverUrl");
            //if(!localStorage.getCacheItem($attrs.dynamicid+"_carousel")) {

                if ($scope.carouselServer == null || $scope.carouselServer == '') {
                    $scope.carouselUrl = $attrs.url;
                } else {
                    $scope.carouselUrl = $scope.carouselServer+$attrs.url;
                }
                var carouselDataPromise =  externalService.carouselService($scope.carouselUrl,"",'GET',$attrs.resourcepath);

                carouselDataPromise.then(function(carouselResult) {
                    $scope.carouselArray = carouselResult["carousel"]["carouselItemArray"];
                    if($scope.carouselArray) {
                        //localStorage.setCacheItem(carouselResult["carousel"]['dynamicId']+"_carousel", JSON.stringify(carouselResult), { hours: 2 });
                        $scope.carouselArray = carouselResult;
                    }
                });
            /*
            }
            else {
                $scope.carouselArray = JSON.parse(localStorage.getCacheItem($attrs.dynamicid+"_carousel"));
            }
            */

			$scope.redirectionCarousel = function(isExternal,path, isWeakLoginRequired,isSmartSaver) {
                if(isWeakLoginRequired && isWeakLoginRequired=='true') {
                    var weakToken  =  localStorage.getItem('weaklogintoken');
                    var strongToken = localStorage.getItem('stronglogintoken');
                    if(localStorage.getItem("PBCardNum")) {
                        var cardNumber = localStorage.getItem("PBCardNum");
                    }
                    if(weakToken == null&&strongToken==null) {
                        $rootScope.weakLoginRedirection(path);
                        $rootScope.redirectionIsExternal = isExternal;
                        $rootScope.redirectedFromCarousel = true;
                    }
                    else {
                        $rootScope.trackBanner(path);
                        if(path.indexOf('TEMPXYZ786') > -1) {
                            var timeStamp = Date.now();
                            if(cardNumber.length <= 10) {
                                var tokenValue = "PBA_oooooo"+cardNumber+"_"+timeStamp+"_";
                            }
                            else {
                                var tokenValue = "PBA_"+cardNumber+"_"+timeStamp+"_";
                            }
                            path = path.replace('TEMPXYZ786',tokenValue);
                        }
                        if(isExternal == "true"){
                            if(path.indexOf('http') > -1 && path.indexOf('rewards') > -1){
                                var tokenVal = localStorage.getItem('stronglogintoken');
                                if(tokenVal){
                                    tokenVal = tokenVal.substring(1, tokenVal.length-1);
                                    if(path.indexOf('?') > -1){
                                        path = path + "&tokendata=" + tokenVal;
                                    }else{
                                        path = path + "?tokendata=" + tokenVal;
                                    }
                                }
                            }
                            window.open(path,"_blank",'location=no');
                        }
                        else {
                            if(isSmartSaver == 'yes') {
                                $rootScope.goPartnerSite('smartSaver',path);
                            }
                            else {
                                $rootScope.go(path);
                            }
                        }
                    }
                }
                else if(isExternal == "true") {
                    //$window.location.href = path;
                    $rootScope.trackBanner(path);
                    if(path.indexOf('http') > -1 && path.indexOf('rewards') > -1){
                        var tokenVal = localStorage.getItem('stronglogintoken');
                        if(tokenVal){
                            tokenVal = tokenVal.substring(1, tokenVal.length-1);
                            if(path.indexOf('?') > -1){
                                path = path + "&tokendata=" + tokenVal;
                            }else{
                                path = path + "?tokendata=" + tokenVal;
                            }
                        }
                    }
                    window.open(path,"_blank",'location=no');
                }
                else {
                    if(isSmartSaver == 'yes')
                    {
                        $rootScope.goPartnerSite('smartSaver',path);

                    }else
                    {
                        $rootScope.go(path);
                    }
                }
            }

            $scope.getCarouselTemplateUrl = function() {
                if(!localStorage.getItem("serverUrl")) {
                    return "/content/phonegap/payback/content/payback/phonegap/www/templates/carouseltemplate.html";
                }
                else {
                    return "../../../../../../templates/carouseltemplate.html";
                }
            }
        },

        link: function(scope, element, attrs) {

        }
    }
}])


.directive('popularstore', function() {
    return {
        template: '<ng-include src="getContentUrl()"/>',
        restrict: 'E',
        scope: {

            componenttitle : '@',
            viewalllink : '@',
            viewtype : '@',
            popularheadingtitlecolor: '@',
            popularheadingtitlefont: '@',
            popularheadingtitlealignment: '@',
            popularheadingstripcolor: '@',
            popstoresweakloginrequired: '@',
            appExports :'@'
        },
        controller: function ($scope,$attrs,externalService,$timeout,$rootScope,$http) {
            var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
            if ( app ) {
                $scope.appExports = true;
            }
            else {
                $scope.appExports = false;
            }

			$scope.popularURl = localStorage.getItem("serverUrl");


            $scope.popularGo = function(popularUrl) {
			    $rootScope.go(popularUrl);
            }

            $scope.redirectToPopularStores = function(popStoresUrl, popWeakLoginRequired){
                var cardNumber = '';
                if(popWeakLoginRequired && popWeakLoginRequired=='true'){
                    var weakToken  =  localStorage.getItem('weaklogintoken');
                    var strongToken = localStorage.getItem('stronglogintoken');
                    if(localStorage.getItem("PBCardNum")) {
                        cardNumber = localStorage.getItem("PBCardNum");
                    }
                    if(weakToken == null && strongToken==null) {
                        $rootScope.redirectedFromCarousel = true;
                        $rootScope.weakLoginRedirection(popStoresUrl);
                    }
                    else {
                        $rootScope.trackBanner(popStoresUrl);

                        if(popStoresUrl.indexOf('http') > -1 && popStoresUrl.indexOf('rewards') > -1){
                            var tokenVal = localStorage.getItem('stronglogintoken');
                            if(tokenVal){
                                tokenVal = tokenVal.substring(1, tokenVal.length-1);
                                if(popStoresUrl.indexOf('?') > -1){
                                    popStoresUrl = popStoresUrl + "&tokendata=" + tokenVal;
                                }else{
                                    popStoresUrl = popStoresUrl + "?tokendata=" + tokenVal;
                                }
                            }
                        }
                        if(popStoresUrl.indexOf('TEMPXYZ786') > -1) {
                            var timeStamp = Date.now();
                            if(cardNumber.length <= 10) {
                                var tokenValue ="PBA_oooooo"+cardNumber+"_"+timeStamp+"_";
                            }
                            else {
                                var tokenValue ="PBA_"+cardNumber+"_"+timeStamp+"_";
                            }
                            popStoresUrl = popStoresUrl.replace('TEMPXYZ786',tokenValue);
                            window.open(popStoresUrl,"_system");

                        }else if(popStoresUrl != '') {
                            window.open(popStoresUrl,"_system");
                        }
                    }
                }else if(popStoresUrl != '') {
                    if(popStoresUrl.indexOf('http') > -1 && popStoresUrl.indexOf('rewards') > -1){
                        var tokenVal = localStorage.getItem('stronglogintoken');
                        if(tokenVal){
                            tokenVal = tokenVal.substring(1, tokenVal.length-1);
                            if(popStoresUrl.indexOf('?') > -1){
                                popStoresUrl = popStoresUrl + "&tokendata=" + tokenVal;
                            }else{
                                popStoresUrl = popStoresUrl + "?tokendata=" + tokenVal;
                            }
                        }
                    }
                    window.open(popStoresUrl,"_system");
                }
            }

		    $scope.getPoppularStoreArray = function(view) {
                try {
				    $scope.popularStoresData =  localStorage.getCacheItem("popularStoresResponse_"+view);
				    if($scope.popularStoresData) {
                        $scope.popularStoresData = JSON.parse($scope.popularStoresData);
                    	$scope.popularStoresArr = $scope.popularStoresData;
                    	$scope.newStoresArray = [];

                    	if(view == 'Carousel View') {
                            if($scope.popularStoresData){
                                $scope.popularrowSize = $scope.popularStoresData['popularStores'];
                                angular.forEach($scope.popularStoresData['popularStores'], function (stores) {
                                    console.log("stores ::"+stores.storeLogo);
                                    $scope.newStoresArray.push(stores);
                                });
                            }
                            var initialCount  = 0;
                            var count = initialCount;
                            if($scope.newStoresArray){
                                $scope.popularrowSize = $scope.newStoresArray.length;
                            }
                            $scope.popularrowSize = Math.ceil($scope.popularrowSize/2);
                            $scope.popularrows = [];
                            var maxRows = $scope.popularrowSize;
                            var maxCols = 2;
                            for( var i =0 ; i < maxRows;i++){
                                $scope.popularrows.push([]);
                                count = initialCount;
                                for( var j =0 ; j < maxCols;j++){
                                    if ($scope.newStoresArray[count]) {
                                        $scope.popularrows[i][j] =  $scope.newStoresArray[count];
                                        $scope.popularrows[i][j].count = count;
                                        count++;
                                    }
                                }
                                initialCount = initialCount + 2;
                            }

                            var initailFinal = 0;
                            var finalCount = initailFinal;
                            if($scope.popularrows) {
                                $scope.popularrowsSize = $scope.popularrows.length;
                            }

                            $scope.finalPopularRowSize = Math.ceil($scope.popularrowsSize/2);
                            $scope.finalPopularRows = [];
                            var finalMaxRows = $scope.finalPopularRowSize;
                            var maxfinalCol = 2;

                            for( var l =0 ; l < finalMaxRows;l++){
                                $scope.finalPopularRows.push([]);
                                finalCount = initailFinal;
                                for( var m =0 ; m < maxfinalCol;m++){
                                    if ($scope.popularrows[finalCount]) {
                                        $scope.finalPopularRows[l][m] =  $scope.popularrows[finalCount];
                                        $scope.finalPopularRows[l][m].finalCount = finalCount;
                                        finalCount++;
                                    }
                                }
                                initailFinal = initailFinal + 2;
                            }
                        }
                        else {
                            if($scope.popularStoresData){
                                $scope.popularrowSize = $scope.popularStoresData['allstores'];
                                angular.forEach($scope.popularStoresData['allstores'], function (stores) {
                                    console.log("stores ::"+stores.storeLogo);
                                    $scope.newStoresArray.push(stores);
                                });
                            }
							var initialCount  = 0;
                            var count = initialCount;
                            if($scope.newStoresArray){
                                $scope.popularrowSize = $scope.newStoresArray.length;
                            }
                            $scope.popularrowSize = Math.ceil($scope.popularrowSize/2);
                            $scope.popularrows = [];
                            var maxRows = $scope.popularrowSize;
                            var maxCols = 2;
                            for( var i =0 ; i < maxRows;i++){
                                $scope.popularrows.push([]);
                                count = initialCount;
                                for( var j =0 ; j < maxCols;j++){
                                    if ($scope.newStoresArray[count]) {
                                        $scope.popularrows[i][j] =  $scope.newStoresArray[count];
                                        $scope.popularrows[i][j].count = count;
                                        count++;
                                    }
                                }
                                initialCount = initialCount + 2;
                            }

                        }
                    }

                }
                catch(getPoppularStoreArrayErr) {
				    console.log("getPoppularStoreArrayErr"+getPoppularStoreArrayErr);
                }
            }

            if(localStorage.getCacheItem("homepageResponse")) {
                localStorage.removeItem("homepageResponse")
            }
            if(!localStorage.getCacheItem("popularStoresResponse_"+$attrs.viewtype)) {
                $scope.popularStoresServerUrl = localStorage.getItem("serverUrl");
                if($attrs.viewtype == 'Carousel View') {
                    $scope.popularStoresRestUrl = "/payback/anon/shoponline.html/homepage.store.json";
                    if($scope.popularStoresServerUrl && $scope.popularStoresServerUrl!="") {
                        $scope.popularStoresRestUrl = $scope.popularStoresServerUrl + $scope.popularStoresRestUrl;
                    }
                    $http.defaults.headers.common['type']= "store";
                }
                else {
                    $scope.popularStoresRestUrl = "/payback/anon/shoponline.html/getAllStores.json";
                    if($scope.popularStoresServerUrl && $scope.popularStoresServerUrl!="") {
                        $scope.popularStoresRestUrl = $scope.popularStoresServerUrl + $scope.popularStoresRestUrl;
                    }
                }

                $scope.methodType = "GET";
                var popularStoreResponse = externalService.callService($scope.popularStoresRestUrl, '', $scope.methodType);

                popularStoreResponse.then(function(response){
                    var error = response["error"];
                    var faultMsg = response["types.FaultMessage"];
                    if(error) {
                        $scope.popularShowNetwork = true;
                    }
                    else if(faultMsg) {
                        $scope.popularShowNetwork = true;
                    }
                    else {
                        if(response["popularStores"] || response["allstores"]) {
                            localStorage.setCacheItem("popularStoresResponse_"+$attrs.viewtype, JSON.stringify(response), { hours: 5 });
                            $scope.getPoppularStoreArray($attrs.viewtype);
                        }
                    }
                });
            }
            else {
                $scope.getPoppularStoreArray($attrs.viewtype);
            }

            $scope.getContentUrl = function() {
				if($attrs.viewtype == 'Carousel View') {
				    if(!localStorage.getItem("serverUrl")) {
                        return "/content/phonegap/payback/content/payback/phonegap/www/templates/popularstorescarousel.html";
                    }
                    else {
                        return '../../../../../../templates/popularstorescarousel.html';
                    }
                }
                else {
                    if(!localStorage.getItem("serverUrl")) {
                        return "/content/phonegap/payback/content/payback/phonegap/www/templates/popularstoresgrid.html";
                    }
                    else {
                        return '../../../../../../templates/popularstoresgrid.html';
                    }
                }
            }
        },
        link: function(scope, element, attrs) {

        }
    }
})






.directive('shoponlinecarousel', function() {
    return {
        template: '<ng-include src="getContentUrl()"/>',
        restrict: 'E',
        scope: {
            dynamicid: '@',
            carouseltype: '@',
            viewtype: '@',
            viewallitems: '@',
            pagetitle: '@',
            shopheadingtitlecolor: '@',
            shopheadingtitlefont: '@',
            shopheadingtitlealignment: '@',
            shopheadingstripcolor: '@',
            appExports :'@'
        },
        controller: function ($scope,$attrs,externalService,$timeout,$rootScope) {
            var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
            if ( app ) {
                $scope.appExports = true;
            }
            else {
                $scope.appExports = false;
            }
            $scope.getShopCarouselArray = function(type,view){
                $scope.hotDealsData = JSON.parse(localStorage.getCacheItem(type+view+"Response"));
                $rootScope.productRedirectionPath = $scope.hotDealsData['productRedirectionPath'];
                if($scope.hotDealsData['carouselType'] == 'topDeals'){
                    $scope.hotDealsArray = $scope.hotDealsData["homeminiCarousels"]["hotproducts"];
                    $scope.hotDealsArray = $scope.hotDealsArray.concat($scope.hotDealsData["homeminiCarousels"]["deals_electronics"]);
                    $scope.hotDealsArray = $scope.hotDealsArray.concat($scope.hotDealsData["homeminiCarousels"]["newarrivals"]);
                    $scope.hotDealsArray = $scope.hotDealsArray.concat($scope.hotDealsData["homeminiCarousels"]["featuredbooks"]);
                }
                else if($scope.hotDealsData['carouselType'] == 'hotDeals'){
                     $scope.hotDealsArray = $scope.hotDealsData["homeminiCarousels"]["deals_electronics"];
                     $scope.hotDealsArray = $scope.hotDealsArray.concat($scope.hotDealsData["homeminiCarousels"]["hotproducts"]);
                     $scope.hotDealsArray = $scope.hotDealsArray.concat($scope.hotDealsData["homeminiCarousels"]["newarrivals"]);
                     $scope.hotDealsArray = $scope.hotDealsArray.concat($scope.hotDealsData["homeminiCarousels"]["featuredbooks"]);
                }
                else if($scope.hotDealsData['carouselType'] == 'featuredProducts'){
                    $scope.hotDealsArray = $scope.hotDealsData["homeminiCarousels"]["newarrivals"];
                    $scope.hotDealsArray = $scope.hotDealsArray.concat($scope.hotDealsData["homeminiCarousels"]["deals_electronics"]);
                    $scope.hotDealsArray = $scope.hotDealsArray.concat($scope.hotDealsData["homeminiCarousels"]["hotproducts"]);
                    $scope.hotDealsArray = $scope.hotDealsArray.concat($scope.hotDealsData["homeminiCarousels"]["featuredbooks"]);
                }
                else if($scope.hotDealsData['carouselType'] == 'featuredBooks'){
                     $scope.hotDealsArray = $scope.hotDealsData["homeminiCarousels"]["featuredbooks"];
                     $scope.hotDealsArray = $scope.hotDealsArray.concat($scope.hotDealsData["homeminiCarousels"]["deals_electronics"]);
                     $scope.hotDealsArray = $scope.hotDealsArray.concat($scope.hotDealsData["homeminiCarousels"]["newarrivals"]);
                     $scope.hotDealsArray = $scope.hotDealsArray.concat($scope.hotDealsData["homeminiCarousels"]["hotproducts"]);
                }
                else if($scope.hotDealsData['carouselType'] == 'popularTelevisions'){
                    $scope.hotDealsArray = $scope.hotDealsData["sidebarData"]['sidebar1']['products'];
                }
                else if($scope.hotDealsData['carouselType'] == 'popularCameras'){
                     $scope.hotDealsArray = $scope.hotDealsData["sidebarData"]['sidebar2']['products'];
                }
                else if($scope.hotDealsData['carouselType'] == 'recentSearchedProducts'){
                    if(localStorage.getItem("productObject")){
                        $scope.hotDealsArray = localStorage.getItem("productObject");
                        $scope.hotDealsArray = JSON.parse($scope.hotDealsArray);
                        $scope.recentSearchArray= $scope.hotDealsArray;
                        $scope.hotDealsArray.reverse();
                    }
                }

                if($scope.hotDealsData['viewType'] == 'Grid view') {
                    var initialCount  = 0;
                    var count = initialCount;
                    if($scope.hotDealsArray){
                        $scope.rowSize = $scope.hotDealsArray.length;
                    }
                    $scope.rowSize = Math.ceil($scope.rowSize/2);
                    $scope.rows = [];
                    var maxRows = $scope.rowSize;
                    var maxCols = 2;
                    for( var i =0 ; i < maxRows;i++){
                        $scope.rows.push([]);
                        count = initialCount;
                        for( var j =0 ; j < maxCols;j++){
                            if ($scope.hotDealsArray[count]) {
                                $scope.rows[i][j] =  $scope.hotDealsArray[count];
                                $scope.rows[i][j].count = count;
                                count++;
                            }
                        }
                        initialCount = initialCount + 2;
                    }
                }
            }

            if(!localStorage.getCacheItem($attrs.carouseltype+$attrs.viewtype+"Response")) {
                $scope.serverUrl = localStorage.getItem("serverUrl");
                if($scope.serverUrl && $scope.serverUrl != ""){
                    $scope.socialCarouselUrl = $scope.serverUrl+$attrs.url;
                }
                else {
                    $scope.socialCarouselUrl = $attrs.url;
                }
                var carouselDataPromise =  externalService.carouselService($scope.socialCarouselUrl,"",'GET',$attrs.resourcepath);

                carouselDataPromise.then(function(hotDealsResponseData) {
                    var faultMsg = hotDealsResponseData["types.FaultMessage"];
                    var errorMessage = hotDealsResponseData["error"];
                    if(errorMessage) {
                        $scope.hotDealsArray = null;
                        $rootScope.showNetwork = true;
                    }
                    else if(faultMsg) {
                        $scope.hotDealsArray = null;
                        if(faultMsg == "Invalid token") {
                            $rootScope.errorMsg = "Token seems invalid, please check again.";
                            localStorage.removeItem('stronglogintoken');
                            localStorage.removeItem('weaklogintoken');
                            localStorage.removeItem('userPoint');
                            $rootScope.showError();
                            $rootScope.go("/content/phonegap/payback/apps/payback/en/home");
                        }
                        else {
                            $rootScope.errorMsg = faultMsg;
                            $rootScope.showError();
                            $rootScope.go("/content/phonegap/payback/apps/payback/en/home");
                        }
                    }
                    else {
                        if(hotDealsResponseData['carouselType'] == 'topDeals'){
                            $scope.hotDealsArray = hotDealsResponseData["homeminiCarousels"]["hotproducts"];
                            $scope.hotDealsArray = $scope.hotDealsArray.concat(hotDealsResponseData["homeminiCarousels"]["deals_electronics"]);
                            $scope.hotDealsArray = $scope.hotDealsArray.concat(hotDealsResponseData["homeminiCarousels"]["newarrivals"]);
                            $scope.hotDealsArray = $scope.hotDealsArray.concat(hotDealsResponseData["homeminiCarousels"]["featuredbooks"]);
                        }
                        else if(hotDealsResponseData['carouselType'] == 'hotDeals'){
                             $scope.hotDealsArray = hotDealsResponseData["homeminiCarousels"]["deals_electronics"];
                             $scope.hotDealsArray = $scope.hotDealsArray.concat(hotDealsResponseData["homeminiCarousels"]["hotproducts"]);
                             $scope.hotDealsArray = $scope.hotDealsArray.concat(hotDealsResponseData["homeminiCarousels"]["newarrivals"]);
                             $scope.hotDealsArray = $scope.hotDealsArray.concat(hotDealsResponseData["homeminiCarousels"]["featuredbooks"]);
                         }
                        else if(hotDealsResponseData['carouselType'] == 'featuredProducts'){
                            $scope.hotDealsArray = hotDealsResponseData["homeminiCarousels"]["newarrivals"];
                            $scope.hotDealsArray = $scope.hotDealsArray.concat(hotDealsResponseData["homeminiCarousels"]["hotproducts"]);
                             $scope.hotDealsArray = $scope.hotDealsArray.concat(hotDealsResponseData["homeminiCarousels"]["deals_electronics"]);
                             $scope.hotDealsArray = $scope.hotDealsArray.concat(hotDealsResponseData["homeminiCarousels"]["featuredbooks"]);
                        }
                        else if(hotDealsResponseData['carouselType'] == 'featuredBooks'){
                              $scope.hotDealsArray = hotDealsResponseData["homeminiCarousels"]["featuredbooks"];
                              $scope.hotDealsArray = $scope.hotDealsArray.concat(hotDealsResponseData["homeminiCarousels"]["hotproducts"]);
                              $scope.hotDealsArray = $scope.hotDealsArray.concat(hotDealsResponseData["homeminiCarousels"]["deals_electronics"]);
                              $scope.hotDealsArray = $scope.hotDealsArray.concat(hotDealsResponseData["homeminiCarousels"]["newarrivals"]);
                        }
                        else if(hotDealsResponseData['carouselType'] == 'popularTelevisions'){
                            $scope.hotDealsArray = hotDealsResponseData["sidebarData"]['sidebar1']['products'];
                        }
                        else if(hotDealsResponseData['carouselType'] == 'popularCameras'){
                             $scope.hotDealsArray = hotDealsResponseData["sidebarData"]['sidebar2']['products'];
                        }
                        else if(hotDealsResponseData['carouselType'] == 'recentSearchedProducts'){
                            if(localStorage.getItem("productObject")){
                                $scope.hotDealsArray = localStorage.getItem("productObject");
                                $scope.hotDealsArray = JSON.parse($scope.hotDealsArray);
                                $scope.recentSearchArray= $scope.hotDealsArray;
                                $scope.hotDealsArray.reverse();
                            }
                        }

                        if($scope.hotDealsArray) {
                            localStorage.setCacheItem(hotDealsResponseData['carouselType']+hotDealsResponseData['viewType']+"Response", JSON.stringify(hotDealsResponseData), { minutes: 30 });
                            $scope.getShopCarouselArray(hotDealsResponseData['carouselType'],hotDealsResponseData['viewType']);
                        }
                    }
                });
            }
            else {
                $scope.getShopCarouselArray($attrs.carouseltype,$attrs.viewtype);
            }

            $scope.redirectFromSearchBox = function() {
                $rootScope.searchResultData = '';
                $scope.go('/content/phonegap/payback/apps/payback/en/home/search');
            }

            $scope.clearRecentSearchedProducts = function(){
                if(localStorage.getItem("productObject")){
                    localStorage.removeItem("productObject");
                    $("#clearProducts").hide();
                    $("#clearButton").hide();
                    $(".displayClearMessage").show();
                }
                else {
                    $rootScope.errorMsg = "Recent Searched Products are already Cleared";
                    $rootScope.showError();
                }
            }

            $scope.goToProductSite = function(id,url,title,imagePath,originalPrice,finalPrice,pointText,numOfSellers){
                if(id) {
                    localStorage.setItem("productId",id);
                    var productJSONArray;
                    if(localStorage.getItem("productObject")) {
                        productJSONArray = localStorage.getItem("productObject");
                        productJSONArray = JSON.parse(productJSONArray);
                        if(productJSONArray.map(function(d) { return d['id']; }).indexOf(id)>=0) {
                            productJSONArray.splice(productJSONArray.map(function(d) { return d['id']; }).indexOf(id),1);
                        }
                    }
                    else {
                        productJSONArray = [];
                    }
                    var productObject = {};
                    var pbStoreDataObject = {};
                    productObject.id=id;
                    title = title.replace(/_/g, ' ');
                    productObject.numTotalStores = numOfSellers;
                    productObject.title=title;
                    productObject.img = imagePath;
                    productObject.origPrice = originalPrice;
                    productObject.finalPrice = finalPrice;
                    pbStoreDataObject.pointText = unescape(pointText);
                    productObject.pbstore_data = pbStoreDataObject;
                    productJSONArray.push(productObject);
                    localStorage.setItem("productObject",JSON.stringify(productJSONArray));
                    $rootScope.go($rootScope.productRedirectionPath);
                }
                else {
                   window.open(url,"_system");
                }
            }
            $scope.discountValue= function(originalPrice,finalPrice){
                if (originalPrice != '' && finalPrice != '') {
                    return Math.ceil(((originalPrice - finalPrice)/originalPrice)*100);
                }
                else {
                    return "";
                }
            }
            $scope.getContentUrl = function() {
                if($attrs.viewtype == 'Carousel View') {
                    if(!localStorage.getItem("serverUrl")) {
                        return "/content/phonegap/payback/content/payback/phonegap/www/templates/hotdealstemplate.html";
                    }
                    else {
                        return '../../../../../../templates/hotdealstemplate.html';
                    }
                }
                else {
                    if(!localStorage.getItem("serverUrl")) {
                        return "/content/phonegap/payback/content/payback/phonegap/www/templates/hotdealsgridtemplate.html";
                    }
                    else {
                        return '../../../../../../templates/hotdealsgridtemplate.html';
                    }
                }
            }
            $scope.redirection = function(path) {
                $rootScope.go(path);
            }
        },
        link: function(scope, element, attrs) {

        }
    }
})

.directive('maincarousel', function() {
    return {
        template: '<ng-include src="getMainTemplateUrl()"/>',
        restrict: 'E',
        scope: {
            mainweakloginrequired : '@',
            autoplaymaincarousel : '@',
            appExports :'@'
        },
        controller: function ($scope,$attrs,externalService,$timeout,$rootScope,$http) {
            var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
            if ( app ) {
                $scope.appExports = true;
            }
            else {
                $scope.appExports = false;
            }
            if(!localStorage.getCacheItem("mainCarouselResponse")) {
                $scope.mainCarouselServerUrl = localStorage.getItem("serverUrl");
                $scope.mainCarouselUrl = "/payback/anon/shoponline.html/homepage.main.json";
                if($scope.mainCarouselServerUrl && $scope.mainCarouselServerUrl!="") {
                    $scope.mainCarouselUrl = $scope.mainCarouselServerUrl + $scope.mainCarouselUrl;
                }
                $http.defaults.headers.common['type']= "main";
                var jsonCarouselResp = externalService.callService($scope.mainCarouselUrl,"","GET");

                jsonCarouselResp.then(function(mainCarouselResponse){
                    var error = mainCarouselResponse["error"];
                    var faultMsg = mainCarouselResponse["types.FaultMessage"];
                    if(!error && !faultMsg && mainCarouselResponse["carouselSlides"]){
                        localStorage.setCacheItem("mainCarouselResponse", JSON.stringify(mainCarouselResponse), { hours: 3 });
                        $scope.carouselRespVar = mainCarouselResponse;
                    }
                });
            }
            else {
                $scope.carouselRespVar = JSON.parse(localStorage.getCacheItem("mainCarouselResponse"));
            }

            $scope.redirectToProductPage = function(productId) {
                localStorage.setItem("productId", productId);
                if($rootScope.redirectProductPath){
                    $rootScope.go($rootScope.redirectProductPath);
                }
            }

            $scope.redirectToWebSites = function(siteUrl,weakLoginRequired){
                var cardNumber = '';
                if(weakLoginRequired && weakLoginRequired=='true'){
                    var weakToken  =  localStorage.getItem('weaklogintoken');
                    var strongToken = localStorage.getItem('stronglogintoken');
                    if(localStorage.getItem("PBCardNum")) {
                        cardNumber = localStorage.getItem("PBCardNum");
                    }
                    if(weakToken == null&&strongToken==null) {
                        $rootScope.redirectedFromCarousel = true;
                        $rootScope.weakLoginRedirection(siteUrl);
                    }
                    else {
                        $rootScope.trackBanner(siteUrl);
                        if(siteUrl.indexOf('http') > -1 && siteUrl.indexOf('rewards') > -1){
                            var tokenVal = localStorage.getItem('stronglogintoken');
                            if(tokenVal){
                            tokenVal = tokenVal.substring(1, tokenVal.length-1);
                                if(siteUrl.indexOf('?') > -1){
                                    siteUrl = siteUrl + "&tokendata=" + tokenVal;
                                }else{
                                    siteUrl = siteUrl + "?tokendata=" + tokenVal;
                                }
                            }
                        }
                        if(siteUrl.indexOf('TEMPXYZ786') > -1) {
                            var timeStamp = Date.now();
                            if(cardNumber.length <= 10) {
                                var tokenValue ="PBA_oooooo"+cardNumber+"_"+timeStamp+"_";
                            }
                            else {
                                var tokenValue ="PBA_"+cardNumber+"_"+timeStamp+"_";
                            }
                            siteUrl = siteUrl.replace('TEMPXYZ786',tokenValue);
                            window.open(siteUrl,"_system");

                        }else if(siteUrl != '') {
                            window.open(siteUrl,"_system");
                        }
                    }
                }else if(siteUrl != '') {
                    if(siteUrl.indexOf('http') > -1 && siteUrl.indexOf('rewards') > -1){
                        var tokenVal = localStorage.getItem('stronglogintoken');
                        if(tokenVal){
                            tokenVal = tokenVal.substring(1, tokenVal.length-1);
                            if(siteUrl.indexOf('?') > -1){
                                siteUrl = siteUrl + "&tokendata=" + tokenVal;
                            }else{
                                siteUrl = siteUrl + "?tokendata=" + tokenVal;
                            }
                        }
                    }
                    window.open(siteUrl,"_system");
                }
            }

            $scope.getMainTemplateUrl = function() {
                if(!localStorage.getItem("serverUrl")) {
                    return "/content/phonegap/payback/content/payback/phonegap/www/templates/maincarouseltemplate.html";
                }
                else {
                    return "../../../../../../templates/maincarouseltemplate.html";
                }
            }
        },

        link: function(scope, element, attrs) {

        }
    }
})


.directive('paybackgridcarousel', function() {
    return {
        template: '<ng-include src="getGridTemplateUrl()"/>',
        restrict: 'E',
        scope: {
            griddynamicid: '@',
            enablenotifsscreen: '@',
            gridcarouselheading: '@',
            gridheadingtitlecolor : '@',
            gridheadingtitlefont : '@',
            gridnumofitems : '@',
            gridheadingtitlealignment : '@',
            gridheadingstripcolor : '@',
            appExports : '@'
        },
        controller: function ($scope,$attrs,externalService,$timeout,$rootScope) {
            var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
            if ( app ) {
                $scope.appExports = true;
            }
            else {
                $scope.appExports = false;
            }
            var gridCarouselDynamicId = $attrs.griddynamicid;
            $scope.gridCarouselServerUrl =localStorage.getItem("serverUrl");
            $scope.notificationMsgsJson = localStorage.getItem("pushNotifications");
            if($scope.notificationMsgsJson){
                $scope.notificationMsgsJson = JSON.parse("["+$scope.notificationMsgsJson+"]");
            }

            //if(!localStorage.getCacheItem($attrs.griddynamicid+"_gridCarousel")) {
                var Url = "";
                if ($scope.gridCarouselServerUrl == null || $scope.gridCarouselServerUrl == '') {
                    Url =$attrs.url;
                } else {
                    Url = $scope.gridCarouselServerUrl+$attrs.url;
                }

				var gridCarouselDataPromise =  externalService.carouselService(Url,"",'GET',$attrs.resourcepath);

                gridCarouselDataPromise.then(function(gridCarouselResult) {

                    if(gridCarouselResult["gridCarousel"]["carouselItemArray"].length > 0) {
                        //localStorage.setCacheItem(gridCarouselResult["gridCarousel"]['dynamicId']+"_gridCarousel", JSON.stringify(gridCarouselResult), { hours: 2 });
						$scope.gridCarouselArray = gridCarouselResult["gridCarousel"]["carouselItemArray"];
                    }
                });
            /*
            }
            else {
                var gridResult= JSON.parse(localStorage.getCacheItem($attrs.griddynamicid+"_gridCarousel"));
                $scope.gridCarouselArray = gridResult['gridCarousel']['carouselItemArray'];
            }
            */
            $scope.isServerUrl = function() {
                if($rootScope.appExports && localStorage.getItem("serverUrl")){
					return true;
                }
                else {
                    return false;
                }
            }

            $scope.showNotificationPopup = function(message, url) {
                if(url) {
                    $rootScope.go(url);
                }
                navigator.notification.alert(message, function(){}, 'Notification');
            }


			$scope.redirection = function(isExternal,path,isWeakLoginRequired) {

				if(isWeakLoginRequired && isWeakLoginRequired=='true'){
                    var weakToken  =  localStorage.getItem('weaklogintoken');
                    var strongToken = localStorage.getItem('stronglogintoken');
                    if(localStorage.getItem("PBCardNum")) {
                        var cardNumber = localStorage.getItem("PBCardNum");
                    }
                    if(weakToken == null&&strongToken==null) {
                        $rootScope.weakLoginRedirection(path);
                        $rootScope.redirectionIsExternal = isExternal;
                        $rootScope.redirectedFromCarousel = true;
                    }
                    else {
                        $rootScope.trackBanner(path);
                        if(path.indexOf('TEMPXYZ786') > -1) {
                            var timeStamp = Date.now();
                            if(cardNumber.length <= 10) {
                                var tokenValue = "PBA_oooooo"+cardNumber+"_"+timeStamp+"_";
                            }
                            else {
                                var tokenValue ="PBA_"+cardNumber+"_"+timeStamp+"_";
                            }
                            path = path.replace('TEMPXYZ786',tokenValue);
                        }

                        if(isExternal == "true"){
                            if(path.indexOf('http') > -1 && path.indexOf('rewards') > -1){
                                var tokenVal = localStorage.getItem('stronglogintoken');
                                if(tokenVal){
                                    tokenVal = tokenVal.substring(1, tokenVal.length-1);
                                    if(path.indexOf('?') > -1){
                                        path = path + "&tokendata=" + tokenVal;
                                    }else{
                                        path = path + "?tokendata=" + tokenVal;
                                    }
                                }
                            }
                            window.open(path,"_blank",'location=no');
                        }
                        else {
                            $rootScope.go(path);
                        }
                    }
                }
                else if(isExternal == "true") {
                    $rootScope.trackBanner(path);
                    if(path.indexOf('http') > -1 && path.indexOf('rewards') > -1){
                        var tokenVal = localStorage.getItem('stronglogintoken');
                        if(tokenVal){
                            tokenVal = tokenVal.substring(1, tokenVal.length-1);
                            if(path.indexOf('?') > -1){
                                path = path + "&tokendata=" + tokenVal;
                            }else{
                                path = path + "?tokendata=" + tokenVal;
                            }
                        }
                    }
                    window.open(path,"_blank",'location=no');
                }
                else {
                    $rootScope.go(path);
                }
			}

			$scope.getGridTemplateUrl = function() {
                if(!localStorage.getItem("serverUrl")) {
                    return "/content/phonegap/payback/content/payback/phonegap/www/templates/gridcarouseltemplate.html";
                }
                else {
                    return "../../../../../../templates/gridcarouseltemplate.html";
                }
            }

        },

        link: function(scope, element, attrs) {
        }
    }
})

  .directive('phone', function() {
    return {
        restrice: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
            angular.element(element).bind('blur', function() {
                var value = this.value;
                if(PHONE_REGEXP.test(value)) {
                    // Valid input

                    angular.element(this).next().next().css('display','none');
                } else {
                    // Invalid input

                    angular.element(this).next().next().css('display','block');
                }
            });
        }
     }
  })

.directive("owlCarousel", function() {
	return {
		restrict: 'E',
		transclude: false,
		link: function (scope) {
			scope.initCarousel = function(element) {
			  // provide any default options you want
				var defaultOptions = {
				};
				var customOptions = scope.$eval($(element).attr('data-options'));
				// combine the two options objects
				for(var key in customOptions) {
					defaultOptions[key] = customOptions[key];
				}
				// init carousel
				$(element).owlCarousel(defaultOptions);
			};
		}
	};
})
.directive('owlCarouselItem', [function() {
	return {
		restrict: 'A',
		transclude: false,
		link: function(scope, element) {
		  // wait for the last item in the ng-repeat then call init
			if(scope.$last) {
				scope.initCarousel(element.parent());
			}
		}
	};
}])

  .filter('rawHtml', ['$sce', function($sce){
  return function(val) {
    return $sce.trustAsHtml(val);
  };
}])

    .factory('branches', function($http) {


    var getBranches = function(latitude,longitude,searchDistance) {

        var serverUrl = localStorage.getItem("serverUrl");
        if (serverUrl == null || serverUrl == '') {
            serverUrl = '/payback/anon/stores.html/branch/list';
        } else {
            serverUrl = serverUrl+'/payback/anon/stores.html/branch/list';
        }
                return $http({
            method:'POST',
			url:serverUrl,
			params: {
					latitude : latitude,
					longitude : longitude,
                	searchDistance : searchDistance
				}
        }).then(function(result){


            return result.data;
        });
    };
     return { getBranches : getBranches };

    var getChangedLocation = function(chnagelocation)
    {

	};
    return  { getChangedLocation : getChangedLocation };

})

.service('externalService', function($http, $q,$rootScope){




  this.callService = function(serverurl,formData,serviceMethod){


      var timeout = 60;

    var timeout = $q.defer(),
                result = $q.defer(),
                timedOut = false,
                httpRequest;

            setTimeout(function () {
                timedOut = true;
                timeout.resolve();
            }, (60000));

            httpRequest = $http({
                method : serviceMethod,
				url: serverurl,
                params: formData,
                cache: false,
                timeout: timeout.promise

            });

            httpRequest.success(function(data, status, headers, config) {
				var responseType = headers()['content-type'];

                         if(responseType)
                                {
                                       if(responseType.indexOf('application/json')== -1){
                                     $rootScope.showNetwork = true;
                                     //$rootScope.errorMsg="We cannot connect to the server either, you might want to visit us again in sometime."
                                     $rootScope.showWeak=false;
                                     $rootScope.showStrong=false;
                                     // $rootScope.showError();
                                     //$rootScope.go("/content/phonegap/payback/apps/payback/en/home/error");
                                      $rootScope.hideLoadingroot();

                                 }
                                 result.resolve(data);

                                }else
                                {
                						 $rootScope.hideLoadingroot();
                                }


            });

			httpRequest.error(function(data, status, headers, config) {
                if(serverurl.indexOf('/payback/secure/userops.html/account/balance')==-1) {
                    if (timedOut) {
                        result.reject({
                            error: 'timeout',
                            message: 'Request took longer than ' + timeout + ' seconds.'
                        });
                        $rootScope.showNetwork = true;
                        $rootScope.errorMsg="We cannot connect to the server either, you might want to visit us again in sometime."
                        $rootScope.showWeak=false;
                        $rootScope.showStrong=false;
                        $rootScope.showError();
                         $rootScope.hideLoadingroot();
                    } else {
                        result.reject(data);
                        $rootScope.showNetwork = true;
                        //$rootScope.errorMsg="We cannot connect to the server either, you might want to visit us again in sometime."
                        $rootScope.showWeak=false;
                        $rootScope.showStrong=false;
                        // $rootScope.showError();
                        if(navigator.onLine == false) {
                          $rootScope.go("/content/phonegap/payback/apps/payback/en/home/error");
                        }
                        else {
                         // ADB.trackAction( 'serviceCallFailed:'+config['url']);
                        }
                        $rootScope.hideLoadingroot();
                    }
                }
            });

            return result.promise;



      return  { callService : callService };


  };

  this.carouselService = function(serverurl,formData,serviceMethod,resourcePath){



      $http.defaults.headers.common['pagePath']= resourcePath;
		 var timeout = 60;

    var timeout = $q.defer(),
                result = $q.defer(),
                timedOut = false,
                httpRequest;

            setTimeout(function () {
                timedOut = true;
                timeout.resolve();
            }, (60000));

            httpRequest = $http({
                method : serviceMethod,
				url: serverurl,
                params: formData,
                cache: false,
                timeout: timeout.promise

            });

            httpRequest.success(function(data, status, headers, config) {
				var responseType = headers()['content-type'];

                         if(responseType)
                                {
                                       if(responseType.indexOf('application/json')== -1){
                                     $rootScope.showNetwork = true;
                                     //$rootScope.errorMsg="We cannot connect to the server either, you might want to visit us again in sometime."
                                     $rootScope.showWeak=false;
                                     $rootScope.showStrong=false;
                                     // $rootScope.showError();
                                     //$rootScope.go("/content/phonegap/payback/apps/payback/en/home/error");
                                      $rootScope.hideLoadingroot();

                                 }
                                 result.resolve(data);

                                }else
                                {
                						 $rootScope.hideLoadingroot();
                                }


            });

			httpRequest.error(function(data, status, headers, config) {
                if(serverurl.indexOf('/payback/secure/userops.html/account/balance')==-1) {
                    if (timedOut) {
                        result.reject({
                            error: 'timeout',
                            message: 'Request took longer than ' + timeout + ' seconds.'
                        });
                        $rootScope.showNetwork = true;
                        $rootScope.errorMsg="We cannot connect to the server either, you might want to visit us again in sometime."
                        $rootScope.showWeak=false;
                        $rootScope.showStrong=false;
                        $rootScope.showError();
                         $rootScope.hideLoadingroot();
                    } else {
                        result.reject(data);
                        $rootScope.showNetwork = true;
                        //$rootScope.errorMsg="We cannot connect to the server either, you might want to visit us again in sometime."
                        $rootScope.showWeak=false;
                        $rootScope.showStrong=false;
                        // $rootScope.showError();
                        if(navigator.onLine == false) {
                          $rootScope.go("/content/phonegap/payback/apps/payback/en/home/error");
                        }
                        else {
                         // ADB.trackAction( 'serviceCallFailed:'+config['url']);
                        }
                        $rootScope.hideLoadingroot();
                    }
                }
            });

            return result.promise;



      return  { carouselService : carouselService };
  };





})

.service('errorhandler', function($http, $q,$rootScope){
  	this.handleError = function(responseData){
  		if(responseData['types.FaultMessage'])
			{
						$rootScope.hideLoadingroot();
						var faultMsg = responseData['types.FaultMessage']['types.Message'];
						if(faultMsg == "Invalid token") {
							$rootScope.errorMsg = "Token seems invalid, please check again.";
							localStorage.removeItem('stronglogintoken');
							localStorage.removeItem('weaklogintoken');
							localStorage.removeItem('userPoint');
							$rootScope.showError();
                           	$rootScope.go("/content/phonegap/payback/apps/payback/en/home");
						}
						else {
							$rootScope.errorMsg = faultMsg;
							$rootScope.showError();
                            $rootScope.go("/content/phonegap/payback/apps/payback/en/home");
						}
				}else
				{
					if(responseData['error'])
						{
						    $rootScope.showNetwork = true;
							$rootScope.hideLoadingroot();
                        	if(responseData['error'] == 'Fatal transport error:Network is unreachable while hitting PAYBACK Service - Authenticate')
                            {
                                $rootScope.go("/content/phonegap/payback/apps/payback/en/home/error");

                            }else
                            {
								$rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
								$rootScope.showError();
                        		$rootScope.go("/content/phonegap/payback/apps/payback/en/home");
                            }

						}
						else if (responseData['message']) {
							$rootScope.hideLoadingroot();
							$rootScope.errorMsg = responseData['message'];
							$rootScope.showError();
							showAllCoupons();
						}
				}

 		};

  })



.factory('branchesWithCoupons', function($http) {


    var getBranchesWithCoupons = function(latitude,longitude,searchDistance,token) {

           var serverUrl = localStorage.getItem("serverUrl");
           if (serverUrl == null || serverUrl == '') {
                serverUrl = '/payback/secure/serviceintegration.html/brancheswithcoupons/list';
           } else {
                serverUrl = serverUrl+'/payback/secure/serviceintegration.html/brancheswithcoupons/list';
           }
		  return $http({
            method:'POST',
			url:serverUrl,
			params: {
					latitude : latitude,
					longitude : longitude,
                	searchDistance : searchDistance,
                    token : token,
                	distributionChannel : '5'

				}
        }).then(function(result){


            return result.data;
        });


		 return  { getBranchesWithCoupons : getBranchesWithCoupons };


    };
    return { getBranchesWithCoupons : getBranchesWithCoupons };
})


.directive('requiredAny', function () {
    // Hash for holding the state of each group
    var groups = {};

    // Helper function: Determines if at least one control
    //                  in the group is non-empty
    function determineIfRequired(groupName) {
        var group = groups[groupName];
        if (!group) return false;

        var keys = Object.keys(group);
        return keys.every(function (key) {
            return (key === 'isRequired') && !group[key];
        });
    }

    return {
        restrict: 'A',
        require: '?ngModel',
        scope: {},   // an isolate scope is used for easier/cleaner
        // $watching and cleanup (on destruction)
        link: function postLink(scope, elem, attrs, modelCtrl) {

            // If there is no `ngModel` or no groupName has been specified,
            // then there is nothing we can do
            if (!modelCtrl || !attrs.requiredAny) return;

            // Get a hold on the group's state object
            // (if it doesn't exist, initialize it first)
            var groupName = attrs.requiredAny;
            if (groups[groupName] === undefined) {
                groups[groupName] = {isRequired: true};
            }
            var group = scope.group = groups[groupName];

            // Clean up when the element is removed
            scope.$on('$destroy', function () {
                delete(group[scope.$id]);
                if (Object.keys(group).length <= 1) {
                    delete(groups[groupName]);
                }
            });

            // Updates the validity state for the 'required' error-key
            // based on the group's status
            function updateValidity() {
                if (group.isRequired) {
                     alert("directive called updateValidity");
                    modelCtrl.$setValidity('required', false);
                } else {
                    modelCtrl.$setValidity('required', true);
                }
            }

            // Updates the group's state and this control's validity
            function validate(value) {
                 alert("directive called validate");
                group[scope.$id] = !modelCtrl.$isEmpty(value);
                group.isRequired = determineIfRequired(groupName);
                updateValidity();
                return group.isRequired ? undefined : value;
            };

            // Make sure re-validation takes place whenever:
            //   either the control's value changes
            //   or the group's `isRequired` property changes
            modelCtrl.$formatters.push(validate);
            modelCtrl.$parsers.unshift(validate);
            scope.$watch('group.isRequired', updateValidity);
        }
    };
})
.directive('uiDate', function() {
return {
require: '?ngModel',
link: function($scope, element, attrs, controller) {
var originalRender, updateModel, usersOnSelectHandler;
if ($scope.uiDate == null) $scope.uiDate = {};
if (controller != null) {
updateModel = function(value, picker) {
return $scope.$apply(function() {
return controller.$setViewValue(element.datepicker("getDate"));
});
};
if ($scope.uiDate.onSelect != null) {
usersOnSelectHandler = $scope.uiDate.onSelect;
$scope.uiDate.onSelect = function(value, picker) {
updateModel(value);
return usersOnSelectHandler(value, picker);
};
} else {
$scope.uiDate.onSelect = updateModel;
}
originalRender = controller.$render;
controller.$render = function() {
originalRender();
return element.datepicker("setDate", controller.$viewValue);
};
}
return element.datepicker($scope.uiDate);
}
};
})

.controller('profilecontroller', function($scope, $ionicPopover, $ionicSideMenuDelegate,$ionicSlideBoxDelegate) {
        /**********************************CHANGE-PIN************************/
        $scope.old = {
            show: false
        };
        $scope.new = {
            show:false
        };

    })

.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                element.next().addClass('headeLogoClass');
                element.removeClass('headeLogoClass').addClass('overlay-show');
            });
        }
    };
})

.directive('focusMe', function($timeout) {
    return {
        link: function(scope, element, attrs) {
            $timeout(function() {
                element[0].focus();
            },1000);

            $timeout(function() {
                if(cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.show();
                }
            },1600);
        }
    };
})


.directive('ddCollapseText', ['$compile', function($compile) {
    return {
        restrict: 'A',
        scope : true,
        replace: true,
        link: function(scope, element, attrs) {
            //console.log(element);
            // start collapsed
            scope.collapsed = false;

            // create the function to toggle the collapse
            scope.toggle = function() {
                scope.collapsed = !scope.collapsed;
            };

            // get the value of the dd-collapse-text attribute
            attrs.$observe('ddCollapseText', function(maxLength) {
                // get the contents of the element
                var text = element.text();

                if (text.length > maxLength) {
                    // split the text in two parts, the first always showing
                    var firstPart = String(text).substring(0, maxLength);
                    var secondPart = String(text).substring(maxLength, text.length);

                    // create some new html elements to hold the separate info
                    var firstSpan = $compile('<span>' + firstPart + '</span>')(scope);
                    var secondSpan = $compile('<span ng-if="collapsed">' + secondPart + '</span>')(scope);
                    var moreIndicatorSpan = $compile('<span ng-if="!collapsed"> ...</span>')(scope);
                    var toggleButton = $compile('<a href="javascript:void(0)" class="collapse-text-toggle" ng-click="toggle()"><i ng-if="!collapsed" class="fa fa-level-down"></i> <i ng-if="collapsed" class="fa fa-level-up"></i> {{collapsed ? "Show Less" : "Show More"}}</a>')(scope);

                    // remove the current contents of the element
                    // and add the new ones we created
                    element.html('');
                    element.append(firstSpan);
                    element.append(secondSpan);
                    element.append(moreIndicatorSpan);
                    element.append(toggleButton);
                }
            });
        }
    };
}])

.filter('escapeTitle', function() {
    return function(input) {
        input = input || '';
        if(input.indexOf("&#39;")!= -1){
            input = input.replace(/&#39;/g,"");
        }
        input = escape(input);
        if(input.indexOf("%u2019")!= -1){
            input = input.replace(/%u2019/g,"");
        }
        input = unescape(input);
        return input.replace(/ /g,"_");
    };
})

.filter('escapePointText', function() {
    return function(input) {
        input = input.trim();
        input = escape(input);
        return input;
    };
})

.filter('pointText', function() {
    return function(input) {
		input = input.trim();
        var firstPointText = input.substring(0, input.indexOf(" "));
        var remainText = input.substring(input.indexOf(" "), input.length);
        return firstPointText+"<span class='blue-text'>"+remainText+"</span>";
    };
})

	.config(['$sceProvider', function($sceProvider) {
        $sceProvider.enabled(false);
    }])

    .config(function($ionicConfigProvider) {
        $ionicConfigProvider.views.maxCache(10);
        $ionicConfigProvider.views.forwardCache(true);
    })

    .config(['$routeProvider',
        function($routeProvider) {
            $routeProvider





                .when('/content/phonegap/payback/apps/payback/en/home/shop', {
                    templateUrl: 'home/shop.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeshop'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/member-enrollment', {
                    templateUrl: 'home/member-enrollment.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomememberenrollment'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/about-payback', {
                    templateUrl: 'home/about-payback.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeaboutpayback'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/privacy-policy', {
                    templateUrl: 'home/privacy-policy.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeprivacypolicy'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/privacy-policy/faq', {
                    templateUrl: 'home/privacy-policy/faq.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeprivacypolicyfaq'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/privacy-policy/terms-and-conditions', {
                    templateUrl: 'home/privacy-policy/terms-and-conditions.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeprivacypolicytermsandconditions'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/store-locator', {
                    templateUrl: 'home/store-locator.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomestorelocator'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/contact-us', {
                    templateUrl: 'home/contact-us.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomecontactus'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/coupons', {
                    templateUrl: 'home/coupons.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomecoupons'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/social-network', {
                    templateUrl: 'home/social-network.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomesocialnetwork'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/partner-brands', {
                    templateUrl: 'home/partner-brands.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomepartnerbrands'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/shop-online', {
                    templateUrl: 'home/shop-online.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeshoponline'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/shop-online/product', {
                    templateUrl: 'home/shop-online/product.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeshoponlineproduct'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/shop-online/coupons-details', {
                    templateUrl: 'home/shop-online/coupons-details.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeshoponlinecouponsdetails'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/shop-online/hot-deals', {
                    templateUrl: 'home/shop-online/hot-deals.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeshoponlinehotdeals'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/shop-online/top-deals', {
                    templateUrl: 'home/shop-online/top-deals.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeshoponlinetopdeals'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/shop-online/featured-products', {
                    templateUrl: 'home/shop-online/featured-products.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeshoponlinefeaturedproducts'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/shop-online/featured-books', {
                    templateUrl: 'home/shop-online/featured-books.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeshoponlinefeaturedbooks'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/shop-online/popular-television', {
                    templateUrl: 'home/shop-online/popular-television.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeshoponlinepopulartelevision'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/shop-online/popular-cameras', {
                    templateUrl: 'home/shop-online/popular-cameras.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeshoponlinepopularcameras'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/shop-online/popular-stores', {
                    templateUrl: 'home/shop-online/popular-stores.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeshoponlinepopularstores'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/search', {
                    templateUrl: 'home/search.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomesearch'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/strong-login', {
                    templateUrl: 'home/strong-login.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomestronglogin'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/weak-login', {
                    templateUrl: 'home/weak-login.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeweaklogin'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/forgot-pin', {
                    templateUrl: 'home/forgot-pin.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeforgotpin'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/my-balance', {
                    templateUrl: 'home/my-balance.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomemybalance'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/my-balance/my-transactions', {
                    templateUrl: 'home/my-balance/my-transactions.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomemybalancemytransactions'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/my-balance/my-profile', {
                    templateUrl: 'home/my-balance/my-profile.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomemybalancemyprofile'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/my-balance/change-pin', {
                    templateUrl: 'home/my-balance/change-pin.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomemybalancechangepin'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/my-balance/linking', {
                    templateUrl: 'home/my-balance/linking.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomemybalancelinking'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/error', {
                    templateUrl: 'home/error.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeerror'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/category-display', {
                    templateUrl: 'home/category-display.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomecategorydisplay'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/EVERYBUY', {
                    templateUrl: 'home/EVERYBUY.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeEVERYBUY'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/moto', {
                    templateUrl: 'home/moto.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomemoto'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/offers', {
                    templateUrl: 'home/offers.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeoffers'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/Burn', {
                    templateUrl: 'home/Burn.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeBurn'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/earn-points', {
                    templateUrl: 'home/earn-points.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeearnpoints'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/Notifications', {
                    templateUrl: 'home/Notifications.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeNotifications'
                })






                .when('/content/phonegap/payback/apps/payback/en/home/Offer', {
                    templateUrl: 'home/Offer.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhomeOffer'
                })






                .when('/content/phonegap/payback/apps/payback/en/home', {
                    templateUrl: 'home.template.html' + cacheKiller,
                    controller: 'contentphonegappaybackappspaybackenhome'
                })

                .otherwise({
                    redirectTo: '/content/phonegap/payback/apps/payback/en/home'
                });
        }
    ]);


}(angular));
