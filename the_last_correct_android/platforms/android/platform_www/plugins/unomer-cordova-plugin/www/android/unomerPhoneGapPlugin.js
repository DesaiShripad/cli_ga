cordova.define("unomer-cordova-plugin.unomerPhoneGapPlugin", function(require, exports, module) {
var LCNvalue = "";

function unomerPhoneGapPlugin() {}

unomerPhoneGapPlugin.prototype.setLCNForUnomer = function(callBackFunction) {
    cordova.exec(callBackFunction, errCallBack, "pluginConnector", "setLCN", [{"LCN" : LCNvalue}]);
};

unomerPhoneGapPlugin.prototype.unomerFetchSurvey = function(callBackFunction) {
    cordova.exec(callBackFunction, errCallBack, "pluginConnector", "fetchUnomerSurvey", [{"location" : "Default"}]);
};

unomerPhoneGapPlugin.prototype.unomerShowSurvey = function(callBackFunction) {
    cordova.exec(callBackFunction, errCallBack, "pluginConnector", "showUnomerSurvey", [{"location" : "Default"}]);
};

function errCallBack(response){
    //alert("error in callback "+ response);
    console.warn("Error response from unomer java class: " + response);
}

(function () {

    unomerPhoneGapPlugin.prototype.callBackFunction = function(response) {

        /*
            List of params that you receive in response :
            response.location                   :   every response/request is marked with a location field ( comes in all )
            response.status                     :   contains various statuses for a response to decode what kind of callback it is ( comes in all )
            response.reward                     :   indicates the value for a reward for example 100. It's configured in your Unomer account.( comes in survey_complete and survey_upload_complete )
            response.rewardCurrency             :   indicates the currency for the reward for example gold coins, points etc. It's configured in your Unomer account. ( comes in survey_complete and survey_upload_complete )
            response.responseCode               :   this is unique identifier that identifies each response on Unomer server ( comes in survey_upload_complete )
            response.isRewarded                 :   informs you whether the completed survey was of rewarded type response or not ( comes in  survey_complete and survey_upload_complete )
            response.numberOfQuestions          :   this field contains the number of questions in the specific survey ( comes in fetch_success )
            response.message                    :   contains any error messages from Unomer ( comes in fetch_failed )
            response.isRewardEnabledOnThisSurvey:   informs you whether the downloaded survey is a rewarded type ( comes in fetch_success )

        */

        if ( response.status == "fetch_started"){
            // Informs you that a new fetch request has been issued
            //document.getElementById("unomerStatus").innerHTML = "Fetching started ... please wait'";
        } else if ( response.status == "fetch_success"){
            // This method is called when a new survey is downloaded successfully. Only when a survey is
            // downloaded, can you attempt to make the show survey call
            //document.getElementById("unomerStatus").innerHTML = "Survey fetched successfully. <br/><br/>Click on Show Survey button to start taking a survey";
            document.getElementById('unomerBox').style.display = 'block';
            localStorage.setItem("unomerResponseFetchStatus", "");
            //changeFetchBtn();

            if(window.ADB){
                ADB.trackAction("unomer_survey_fetch",{"pb_card_number": localStorage.getItem("PBCardNum"),"app_version": "12.2.7", "unomer_response_code": response.responseCode , "unomer_response_status":response.status, "unomer_response_isRewardEnabledOnThisSurvey":response.isRewardEnabledOnThisSurvey});
                console.log("ADB.trackAction is working fine at unomer_survey_fetch");
            }else{
                console.log("ADB.trackAction not working at unomer_survey_fetch");
            }
        } else if ( response.status == "fetch_failed"){
            // This method is called when an attempt to download a new survey fails
            // The response.message gives you the details
            //document.getElementById("unomerStatus").innerHTML = "Fetch Failed :: " + response.message;
            //changeShowBtn();
            document.getElementById('unomerBox').style.display = 'none';
            localStorage.setItem("unomerResponseFetchStatus", "fetch_failed");
        } else if ( response.status == "survey_displayed"){
            // Informs you that the survey is now being displayed on to the screen of the user's device
            if(window.ADB){
                ADB.trackAction("unomer_survey_start",{"pb_card_number": localStorage.getItem("PBCardNum"),"app_version": "12.2.7", "unomer_response_code": response.responseCode , "unomer_response_status":response.status});
                console.log("ADB.trackAction is working fine at unomer_survey_start");
            } else{
                console.log("ADB.trackAction not working at unomer_survey_start");
            }
        } else if ( response.status == "survey_closed"){
            // This method is called whenever the user either clicks on the "cross button" or
            // if the user cicks on back button of device. In both the cases the survey is
            // considered not complete and is not attempted to be uploaded on to the server
            //document.getElementById("unomerStatus").innerHTML = "Survey was cancelled. <br/><br/>Click on Fetch survey button to start downloading a new survey";
            //changeShowBtn();
            fetchUnomer();
        } else if ( response.status == "survey_complete"){
            // This is called when the slast question on the Survey is taken and the
            // survey animates out. Note that this method only tells you that the
            // user has completed a survey - it does not guarantee that the response
            // has also been uploaded on Unomer. If you want to reward user only when
            // the response has been uploaded, then use survey_upload_complete
            //document.getElementById("unomerStatus").innerHTML = "Survey completed. You have been awarded : "+ response.reward + "" + response.rewardCurrency + ".<br/><br/>You can now click on Fetch Button to start downloading a new survey";
            //changeShowBtn();
            fetchUnomer();
        } else if ( response.status == "survey_upload_complete"){
            // This is a confirmation that survey response has been uploaded on server
            // This is called only after a confirmation is received with a "response code"
            // You will get a response code in response.responseCode

            if(window.ADB){
                ADB.trackAction("unomer_survey_success",{"pb_card_number": localStorage.getItem("PBCardNum"),"app_version": "12.2.7", "unomer_response_code": response.responseCode , "unomer_response_status":response.status, "unomer_response_isRewarded":response.isRewarded});
                console.log("ADB.trackAction is working fine at unomer_survey_success");
            }else{
                console.log("ADB.trackAction not working at unomer_survey_success");
            }

            var cardNumber = localStorage.getItem("cardNumber");
            var serverUrl = null;
            serverUrl = localStorage.getItem("serverUrl");
            var createEarnUrl = "";

            if(serverUrl==null&&serverUrl == "" ){
                createEarnUrl =  '/payback/secure/userops.html/earnpoints';
            }
            else {
                createEarnUrl = serverUrl+"/payback/secure/userops.html/earnpoints";
            }
            var points = response.reward;
            $.ajax({
                method:'POST',
                url : createEarnUrl,
                async : false,
                data: {
                    alias : cardNumber,
                    points : response.reward,
                    responseCode : response.responseCode
                }
            }).success(function (data, status, headers, config) {
                if(data['extint.AccountBalanceDetails']){
                    var weakToken = data['weakToken']
                    localStorage.setItem('weaklogintoken', weakToken);

                    if(window.ADB){
                        ADB.trackAction("unomer_createEarn_success",{"pb_card_number": localStorage.getItem("PBCardNum"),"app_version": "12.2.7", "unomer_response_code": response.responseCode , "unomer_response_status":response.status, "unomer_response_isRewarded":response.isRewarded,"points":response.reward, "partnerUid":data["extint.PartnerUId"]});
                        console.log("ADB.trackAction is working fine at unomer_createEarn_success");
                    }else{
                        console.log("ADB.trackAction not working at unomer_createEarn_success");
                    }
                    //$scope.balance = data['extint.AccountBalanceDetails']['types.TotalPointsAmount'];
                    var userPoint = localStorage.getItem('userPoint');
                    userPoint = parseInt(userPoint) + parseInt(points);
                    var pointsHTML = $("#points").find('p')[1];
                    $(pointsHTML).find('strong').html(userPoint + " Points");
                    localStorage.setCacheItem("userPoint", userPoint, { days: 1 });
                    localStorage.setItem("unomerResponse","");
                    navigator.notification.alert('You have earned '+ points +' PAYBACK Points. Thank you for taking the survey.','','Congratulations!','OK');
                } else {
                    if(window.ADB){
                        ADB.trackAction("unomer_createEarn_success_no_details",{"pb_card_number": localStorage.getItem("PBCardNum"),"app_version": "12.2.7", "unomer_response_code": response.responseCode , "unomer_response_status":response.status, "unomer_response_isRewarded":response.isRewarded,"points":response.reward, "partnerUid":data["extint.PartnerUId"]});
                        console.log("ADB.trackAction is working fine at unomer_createEarn_success_no_details");
                    }else{
                        console.log("ADB.trackAction not working at unomer_createEarn_success_no_details");
                    }
                    if(data['types.CreateEarnTransactionFault']){
                        navigator.notification.alert('Points did not get credited! Please contact Support!','','Sorry!','OK');
                        console.log("Points did not get credited! because of data mismatch between frontend and backend.");
                    }else{
                        navigator.notification.alert('You have earned '+ points +' PAYBACK Points. Thank you for taking the survey.','','Congratulations!','OK');
                    }
                }
            }).error(function (data, status, headers, config) {
                    if(window.ADB){
                        ADB.trackAction("unomer_createEarn_failure",{"pb_card_number": localStorage.getItem("PBCardNum"),"app_version": "12.2.7", "unomer_response_code": response.responseCode , "unomer_response_status":response.status, "unomer_response_isRewarded":response.isRewarded,"points":response.reward, "partnerUid":data["extint.PartnerUId"]});
                        console.log("ADB.trackAction is working fine at unomer_createEarn_failure");
                    }else{
                        console.log("ADB.trackAction not working at unomer_createEarn_failure");
                    }
                    throw new Error('Something went wrong... in unomer callback');
            });

            unomerPhoneGapPlugin.prototype.unomerResponseAsJson = response;
            fetchUnomer();


        } else if ( response.status == "survey_confirm_declined"){
            // If the confirmation to take a survey is declined by user then this is called.
            // Note that you can configure to show ( or not show ) the confirmation and if you
            // have configured it to "not show" then this method will never be called
        } else if ( response.status == "survey_upload_failed"){
            // Informs you that survey upload failed on to Unomer server
        } else if ( response.status == "survey_user_disqualified"){
            // This method is called when an attempt to download a new survey fails
            // The response.message gives you the details
            //document.getElementById("unomerStatus").innerHTML = "User Disqualified" + response.message;
            //changeShowBtn();
            navigator.notification.alert('You did not qualify for this survey. Please look out for more surveys in the future to accelerate points.','','Try Next time.','OK');
            document.getElementById('unomerBox').style.display = 'none';
            localStorage.setItem("unomerResponseDisqualified", "true");

            if(window.ADB){
                ADB.trackAction("unomer_survey_disqualified",{"pb_card_number": localStorage.getItem("PBCardNum"),"app_version": "12.2.7", "unomer_response_code": response.responseCode , "unomer_response_status":response.status, "unomer_response_isRewarded":response.isRewarded});
                console.log("ADB.trackAction is working fine at unomer_survey_disqualified");
            }else{
                console.log("ADB.trackAction not working at unomer_survey_disqualified");
            }
        }


    }

    function changeFetchBtn(){
        document.getElementById("unomerBox").innerHTML = '<button id="unomerButton" type="button" value="show">Show survey</button>';
        //buttonListener();
    }

    function changeShowBtn(){
            document.getElementById("unomerBox").innerHTML = '<button id="unomerButton" type="button" value="fetch">Fetch survey</button>';
            //buttonListener();
    }

   function fetchUnomer() {
        unomerPhoneGapPlugin.prototype.unomerFetchSurvey(unomerPhoneGapPlugin.prototype.callBackFunction);
   }

   function showUnomer() {
        unomerPhoneGapPlugin.prototype.unomerShowSurvey(unomerPhoneGapPlugin.prototype.callBackFunction);
   }

   function setLCNviaJS(valOfLCN) {
        LCNvalue = valOfLCN;
        alert("Value of LCN set to : "+LCNvalue);
        unomerPhoneGapPlugin.prototype.setLCNForUnomer(unomerPhoneGapPlugin.prototype.callBackFunction);
   }

   //alert(globalLCN);
   fetchUnomer();

})();
module.exports = new unomerPhoneGapPlugin();
});
