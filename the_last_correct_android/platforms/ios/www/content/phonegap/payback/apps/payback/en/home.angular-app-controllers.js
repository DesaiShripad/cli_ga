




;(function (angular, document, undefined) {

    'use strict';

    // Cache killer is used to ensure we get the very latest content after an app update
    var cacheKiller = '?ck=' + (new Date().getTime());

    /**
     * Controllers
     */
    angular.module('cqAppControllers', ['ngRoute'])







// Controller for page 'shop'
.controller('contentphonegappaybackappspaybackenhomeshop', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/shop.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'shop';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/shop';
		$rootScope.angularPageTitle = $('<div/>').html('BINGO - COMPARE. SHOP. EARN.').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});   
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* staticblock component controller (path: content-par/staticblock) */


    data.then(function(response) {
    var serverUrl = localStorage.getItem("serverUrl");
    $scope.htmlContent = response.data["content-par/staticblock"]["items"][0]["htmlText"];
    $scope.pageImage = response.data["content-par/staticblock"]["items"][0]["pageImage"];
    if(serverUrl != null || serverUrl != ' '){
        $scope.pageImage = serverUrl + $scope.pageImage
    }
    $scope.pageTitle = response.data["content-par/staticblock"]["items"][0]["pageTitle"];

	var div1 = document.getElementById("htmlContent");
	var decoded = $("<div/>").html($scope.htmlContent).text();


	$scope.decodedHtml =decoded;

	$scope.headerHtml ="<div class='policy-title col'>"+
                            "<img src='"+$scope.pageImage+"' alt='store' class='pop-icons'>"+
                            "<p>"+$scope.pageTitle+"</p>"+
                        "</div>";

});
$scope.headerHtml = "";
$scope.headerClass = "row row-center policy-header";
$scope.ionContentClass = "";
$rootScope.redirectedFromProductCoupons =  false;





    /* hotdealscarousel component controller (path: content-par/hotdealscarousel) */

    data.then(function(response) {

    });


    $rootScope.redirectedFromProductCoupons =  false;
    $scope.shopOnline = "shop-online";
    $rootScope.shopOnlineClass = "row coupons-header row-center";
    $scope.shopOnlinePage  =  "true";
    $scope.shopOnlineTitle ="<div class='sl-title col'>"+
                                "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='!appExports'>"+
                                "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='appExports'>"+
                                "<p>{{ angularPageTitle | uppercase}}</p>"+
                           "</div>";





}])









// Controller for page 'member-enrollment'
.controller('contentphonegappaybackappspaybackenhomememberenrollment', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/member-enrollment.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'member-enrollment';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/member-enrollment';
		$rootScope.angularPageTitle = $('<div/>').html('Member Enrollment').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }
        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});   
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* enrollmember component controller (path: content-par/enrollmember) */

$scope.successPath = "";
$scope.enrollDetails = "";
$scope.cardTypeValue = '';
data.then(function(response) {
    $scope.enrollDetails = response['data']['content-par/enrollmember'];
    $scope.successPath = response['data']['content-par/enrollmember']['enrollSuccessPath'];

});

$scope.contentWrapper = "dynamic-header";
$scope.headerHtml ="<div class='policy-title col'>"+
                            "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/user_icon.png' alt='store' id='brand-logo' ng-if='!appExports'>"+
                            "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/user_icon.png' alt='store' id='brand-logo' ng-if='appExports'>"+
                            "<p>{{ angularPageTitle | uppercase}}</p>"+
                        "</div>";
$rootScope.headerClass = "row row-center brand-header";
$rootScope.redirectedFromProductCoupons = false;
$rootScope.linkedMobileNumber = "";

$scope.register = "";
if ( localStorage.getItem("registerUser") && localStorage.getItem("registerUser") ) {
     $scope.getStates();
    localStorage.removeItem("registerUser");
    $scope.register = true;
} else {
    $scope.register = false;
}

$scope.registerForm = false;
$scope.enrollmentForm = false;
$scope.enrollOrRegister = false;
if($scope.register) {
    $scope.registerForm = true;
    $scope.enrollOrRegister = true;
}
else {
    $scope.enrollmentForm = true;

}

$scope.closePop();



$scope.enrollMember = function(){
    var serverUrl = localStorage.getItem("serverUrl");
    if($scope.register) {
        if (serverUrl == null || serverUrl == '') {
            serverUrl = '/payback/secure/userops.html/registration/cardholder';
        } else {
            serverUrl = serverUrl+'/payback/secure/userops.html/registration/cardholder';
        }
    }
    else {
        if (serverUrl == null || serverUrl == '') {
            serverUrl = '/payback/secure/userops.html/registration/newuser';
        } else {
            serverUrl = serverUrl+'/payback/secure/userops.html/registration/newuser';
        }
    }
    var identification=1234;
    var passPin = parseInt(document.getElementById("17").value);
    var title = $("input[name=title]:checked").val();
    var firstName = document.getElementById("firstName").value;
    var lastName = document.getElementById("lastName").value;
    var email = document.getElementById("confirmEmail").value;
    if($scope.register) {
        var phoneNumber = document.getElementById("phoneNum").value;
    }
    else {
        var phoneNumber = document.getElementById("mobileNumber").value;
		var cardType = $scope.enrollDetails['cardType'];

        if(cardType == '1') {
            $scope.cardTypeValue = "PERMANENT";
        }
        else if(cardType == '2') {
            $scope.cardTypeValue = "TEMPORARY";
        }
        else if(cardType == '3') {
            $scope.cardTypeValue = "NOT CARDED";
        }
        else if(cardType == '4') {
            $scope.cardTypeValue = "RETAIL CARD";
        }
        else if(cardType == '5') {
            $scope.cardTypeValue = "VIRTUAL CARD";
        }
    }
    var dob = document.getElementById("dob").value;
    var addressFirst = document.getElementById("addressFirst").value;
    var addressSecond = document.getElementById("addressSecond").value;
    var addressThird = document.getElementById("addressThird").value;
    var state = document.getElementById("state").value;
    var city = document.getElementById("city").value;
    var zipCode = document.getElementById("zipCode").value;

    if($scope.register) {
        var cardNumber = localStorage.getItem("cardNumber");
        var parameters = {
            "salutation" : title,
            "cardNumber" : cardNumber,
            "firstName" : firstName,
            "lastName" : lastName,
            "dateOfBirth" : dob,
            "pin" : passPin,
            "mobileNumber" : phoneNumber,
            "emailId" : email,
            "addressLineone" : addressFirst,
            "addressLinetwo" : addressSecond,
            "addressLineThree" : addressThird,
            "city" : city,
            "state" : state,
            "pincode" : zipCode
        }
    }
    else {
        var parameters = {
            "salutation" : title,
            "firstName" : firstName,
            "lastName" : lastName,
            "dateOfBirth" : dob,
            "pin" : passPin,
            "mobileNumber" : phoneNumber,
            "emailId" : email,
            "addressLineone" : addressFirst,
            "addressLinetwo" : addressSecond,
            "addressLineThree" : addressThird,
            "city" : city,
            "state" : state,
            "pincode" : zipCode,
            "lmid" : $scope.enrollDetails['partnerShortName'],
            "partnerName" : $scope.enrollDetails['title'],
            "enrollmentSource" : $scope.enrollDetails['enrollmentSource'],
            "promotionalId" : $scope.enrollDetails['promotionalId'],
            "memberClassId" : $scope.enrollDetails['memberClassId'],
            "memberCardTypeId" : $scope.enrollDetails['memberCardTypeId'],
            "promoCode" : $scope.enrollDetails['promoCode'],
            "logoCode" : $scope.enrollDetails['logoCode'],
            "logoCodeDesc" : $scope.enrollDetails['logoCodeDesc'],
            "cardTypeId" : $scope.enrollDetails['cardType'],
            "cardTypeValue" : $scope.cardTypeValue
        }
    }

    if($scope.register) {
        $http({
                method:'POST',
                url : serverUrl,
                async : false,
                params: parameters
        }).success(function (data, status, headers, config) {
            $scope.hideLoading();
            if(data['extint.Token']) {
                var strongToken = data['extint.Token'];
                localStorage.setItem('stronglogintoken', strongToken);
                $scope.getPoints(strongToken);
                if(data['extint.LoyCardNumber']) {
                    localStorage.setItem("cardNumber",weakCardNumber);

                }
                localStorage.setItem("enrolRegMobileNo", document.getElementById("phoneNum").value);
                localStorage.setItem("registerMember", true);

                $scope.go($rootScope.mobileLinkPath);
            }
            else if (data["types.FaultMessage"]) {
                $rootScope.errorMsg = "";
                if(data["types.FaultMessage"]['types.Code'] == 'LOY-00075') {
                    $rootScope.errorMsg = "Did you forget we have met before?";
                }
                else if(data["types.FaultMessage"]['types.Code'] == 'IND-00005') {
                    $rootScope.errorMsg = "We know you birthday already and that cannot be your PIN. Choose another unique code"
                }
                else if(data["types.FaultMessage"]['types.Code'] == 'EXTINT-00039') {
                    $rootScope.errorMsg = "The PIN has to be 4 digits, no less and no more so try again!";
                }
                else if(data["types.FaultMessage"]['types.Code'] == 'IND-00007') {
                    $rootScope.errorMsg = "Some crucial information is missing, please check again.";
                }
                else {
                    $rootScope.errorMsg = data["types.FaultMessage"]['types.Message'];
                }
                $rootScope.showError();
            } else {
                if (registerCallBack['error']) {
                    $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
                    $rootScope.showError();
                }

            }

        }).error(function (data, status, headers, config) {
            $scope.hideLoading();
            throw new Error('Something went wrong...');
        });
    }
    else {
        $http({
            method:'POST',
            url : serverUrl,
            async : false,
            params: parameters
        }).success(function (data, status, headers, config) {
            $scope.hideLoading();
            if(data['extint.Token']) {
                var strongToken = data['extint.Token'];
                localStorage.setItem('stronglogintoken', strongToken);
                $scope.getPoints(strongToken);
                if(data['extint.LoyCardNumber']) {
                    localStorage.setItem("cardNumber",data['extint.LoyCardNumber']);
                    // if(window.ADB)
                    // {
                    //     ADB.trackAction("signUp",{"signedUpCardNumber": data['extint.LoyCardNumber']});
                    // }
                }
                localStorage.setItem("enrolRegMobileNo", document.getElementById("mobileNumber").value);
                localStorage.setItem("enrolMember", true);
                $scope.go('/content/phonegap/payback/apps/payback/en/home/my-balance/linking');
            }
            else if (data["types.FaultMessage"]) {
                $rootScope.errorMsg = "";
                if(data["types.FaultMessage"]['types.Code'] == 'LOY-00075') {
                    $rootScope.errorMsg = "Did you forget we have met before?";
                }
                else if(data["types.FaultMessage"]['types.Code'] == 'IND-00007') {
                    $rootScope.errorMsg = "Some crucial information is missing, please check again."
                }
                else if(data["types.FaultMessage"]['types.Code'] == 'IND-00005') {
                    $rootScope.errorMsg = "We know you birthday already and that cannot be your PIN. Choose another unique code."
                }
                else if(data["types.FaultMessage"]['types.Code'] == 'EXTINT-00003') {
                    $rootScope.errorMsg = "Sorry. Try again."
                }
                else {
                    $rootScope.errorMsg = enrollMemberResponse["types.FaultMessage"]['types.Message'];
                }
                $rootScope.showError();
            }
            else {
                if (data['error']) {
                    $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
                    $rootScope.showError();
                }
            }

        }).error(function (data, status, headers, config) {
            $scope.hideLoading();
            throw new Error('Something went wrong...');
        });
    }
}

data.then(function(response) {
    $scope.Title = response.data["content-par/enrollmember"].items;
});

$scope.enrollMemberValidation = function() {
    if ($scope.register) {
        var emailCompareId = "#confirmEmail";
    } else {
        var emailCompareId = "#emailId";
    }

    $.validator.addMethod("chkDate", function(value, element) {
        var selDate = document.getElementById("dob").value;
        var date = new Date(selDate);
        var now = new Date();
        now.setMonth(now.getMonth() - 216);
        if(now > date) {
            return true;
        }
        else {
            return false;
        }
    }, "Age must be above 18 years");

    $.validator.addMethod("emailInput", function(value, element) {
        return this.optional(element) || /^[A-Z0-9._-]+@[A-Z0-9.-]+\.(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|inc)$/i.test(value);
    }, "Letters only please");
    $.validator.addMethod("mobileNumber", function(value, element) {
        var mobileNumberLength=$("#phoneNum").val();
        if(mobileNumberLength.length==10 ||mobileNumberLength.length==12){
         return true;
        }else{
            return false;
        }
    }, "Invalid");
    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z-]+$/i.test(value);
    }, "Letters only please");
    $.validator.addMethod("addressInput", function(value, element) {
        return this.optional(element) || /^[0-9a-z#',-./"&_ ]+$/i.test(value);
    }, "Invalid address");
    var validator = $("#enrollRegForm").validate({
        errorClass: "error",
        errorElement: "div",
        rules : {
            title :{
                required:true
            },
            firstName : {
                required: true,
                maxlength : "20"
            },
            lastName : {
                required: true,
                maxlength : "20"
            },
            confirmEmail : {
                required: true,
                emailInput:true,
                equalTo : emailCompareId
            },
            phone : {
                required: true,
                mobileNumber:true
            },
            dob : {
                required: true,
                chkDate: true
            },
            addressFirst:{
                required: true,
                addressInput:true,
                maxlength : "50",
                minlength : "1"
            },
            addressSecond:{
                required: true,
                maxlength : "50",
                addressInput:true
            },
            addressThird : {
                maxlength : "50",
                addressInput:true
            },
            state : {
                required: true,
                maxlength : "30"
            },
            city : {
                required: true,
                maxlength : "30"
            },
            zipCode : {
                required: true,
                maxlength : "6"
            },
            pin1:{
                required: true
            },
            pin2:{
                required: true
            },
            pin3:{
                required: true
            },
            pin4:{
                required: true
            }

        },
        groups: {
            pinNumbers: "title"
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "title")
            error.insertAfter("#titleThree");
            else
            error.insertAfter(element);
        },
        messages : {
            cardNumber:{
                required: "Please enter Card Number or Mobile Number!",
                minlength: "Card number should be minimum 16 characters!",
                maxlength: "Card number should be minimum 16 characters!"
            },
            title :{
                required: "Please select Title!",
            },
            firstName : {
                required: "Please enter First Name!",
                maxlength: "First name should be maximum 20 characters!"
            },
            lastName : {
                required: "Please enter Last Name!",
                maxlength: "Last Name should be maximum 20 characters!"
            },
            confirmEmail : {
                required: "Please enter Email-Id!",
                emailInput : "Please enter Valid Email-Id!",
                equalTo : "Email ids does not match"
            },
            phone : {
                required: "Please enter Mobile Number!",
                mobileNumber:"Invalid Mobile Number!"
            },
            dob : {
                required: "Please select Date!",
                chkDate: "Age must be above 18 years"
            },
            addressFirst:{
                required: "Flat/House Number is required!",
                maxlength: "Flat/House Number should be maximum 50 characters!",
                addressInput: "Please enter valid input",
                minlength: "At least 1 characters required here."
            },
            addressSecond : {
                required: "Society/Street Name is required!",
                maxlength: "Society/Street Name should be maximum 50 characters!",
                addressInput: "Please enter valid input"
            },
            addressThird : {
                maxlength: "Landmark should be maximum 50 characters!",
                addressInput: "Please enter valid input"
            },
            state : {
                required: "Please select State!",
                maxlength: "State should be maximum 30 characters!"
            },
            city : {
                required: "Please select City!",
                maxlength: "City should be  maximum 30 characters!"
            },
            zipCode : {
                required: "Please enter Pin Code!",
                maxlength: "PIN should be maximum 6 digits!"
            },
            pin1:{
                required: "Please enter PIN!"
            },
            pin2:{
                required: "Please enter PIN!"
            },
            pin3:{
                required: "Please enter PIN!"
            },
            pin4:{
                required: "Please enter PIN!"
            }
        }
    });
    if (validator.form()) {
        $scope.showLoading();
        $scope.enrollMember();
    }
}


$scope.checkEnrollValidation = function() {
    try {
        var validator = $("#enrollmentCheckForm").validate({
            errorClass: "error",
            errorElement: "div",
            rules : {
                mobileNumber : {
                    required : true
                },
                emailId : {
                    required : true
                }
            },
            messages : {
                mobileNumber : {
                    required : "Please enter Mobile Number!"
                },
                emailId : {
                    required : "Please enter Email ID!"
                }
            }
        });
        if (validator.form()) {
            $scope.showLoading();
            $scope.checkNumberValidity();

        }
    }
    catch(err) {
        console.log("** Check Enrollment Validation Exception **"+err);
    }
}

$scope.checkNumberValidity = function() {
    try {
        var mobile = $("#mobileNumber").val();
        var serverUrl = localStorage.getItem("serverUrl");
        if (serverUrl == null || serverUrl == '') {
            serverUrl = '/payback/secure/userops.html/auth/getCardDetailFromNumber';
        } else {
            serverUrl = serverUrl+'/payback/secure/userops.html/auth/getCardDetailFromNumber';
        }
        $http({
            method:'POST',
            url : serverUrl,
            async : false,
            params : {
                mobileNumber : mobile
            }
        }).success(function (data, status, headers, config) {
            console.log("mobile response "+data.message);
            if(undefined != data.message && data.message == "Member not registered") {
                $scope.checkEmailValidity();
            }
            else {
                $scope.hideLoading();
                $rootScope.errorMsg = "You are already a member. Please contact PAYBACK customer care.";
                $rootScope.showError();
            }
        }).error(function (data, status, headers, config) {
            $scope.hideLoading();
            throw new Error('Something went wrong...');
        });
    }
    catch(err) {
        console.log("** Check Mobile Number Exception **"+err);
    }
}

$scope.checkEmailValidity = function() {
    try {
        var emailId = $("#emailId").val();
        var serverUrl = localStorage.getItem("serverUrl");
        if (serverUrl == null || serverUrl == '') {
            serverUrl = '/payback/secure/userops.html/auth/getCardDetailFromEmail';
        } else {
            serverUrl = serverUrl+'/payback/secure/userops.html/auth/getCardDetailFromEmail';
        }
        $http({
            method:'POST',
            url : serverUrl,
            async : false,
            params : {
                email : emailId,
                partnerShortName : $scope.enrollDetails['partnerShortName']
            }
        }).success(function (data, status, headers, config) {
            $scope.hideLoading();
            console.log("email response "+data.message);
            if(data.message == "Member not registered") {
                $(".enollBtn").hide();
                $scope.enrollOrRegister = true;
                $('#mobileNumber').attr('disabled', 'disabled');
                $('#emailId').attr('disabled', 'disabled');
                $scope.getStates();
            } else {
                $rootScope.errorMsg = "You are already a member. Please contact PAYBACK customer care.";
                $rootScope.showError();
            }
        }).error(function (data, status, headers, config) {
            $scope.hideLoading();
            throw new Error('Something went wrong...');
        });
    }
    catch(err) {
        console.log("** Check Email Id Exception **"+err);
    }
}
}])









// Controller for page 'about-payback'
.controller('contentphonegappaybackappspaybackenhomeaboutpayback', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/about-payback.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'about-payback';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/about-payback';
		$rootScope.angularPageTitle = $('<div/>').html('About PAYBACK').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }
        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});   
        }
		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* staticblock component controller (path: content-par/staticblock) */


    data.then(function(response) {
    var serverUrl = localStorage.getItem("serverUrl");
    $scope.htmlContent = response.data["content-par/staticblock"]["items"][0]["htmlText"];
    $scope.pageImage = response.data["content-par/staticblock"]["items"][0]["pageImage"];
    if(serverUrl != null || serverUrl != ' '){
        $scope.pageImage = serverUrl + $scope.pageImage
    }
    $scope.pageTitle = response.data["content-par/staticblock"]["items"][0]["pageTitle"];

	var div1 = document.getElementById("htmlContent");
	var decoded = $("<div/>").html($scope.htmlContent).text();


	$scope.decodedHtml =decoded;

	$scope.headerHtml ="<div class='policy-title col'>"+
                            "<img src='"+$scope.pageImage+"' alt='store' class='pop-icons'>"+
                            "<p>"+$scope.pageTitle+"</p>"+
                        "</div>";

});
$scope.headerHtml = "";
$scope.headerClass = "row row-center policy-header";
$scope.ionContentClass = "";
$rootScope.redirectedFromProductCoupons =  false;
}])









// Controller for page 'privacy-policy'
.controller('contentphonegappaybackappspaybackenhomeprivacypolicy', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/privacy-policy.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'privacy-policy';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/privacy-policy';
		$rootScope.angularPageTitle = $('<div/>').html('Privacy Policy').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }
        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});   
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* staticblock component controller (path: content-par/staticblock) */


    data.then(function(response) {
    var serverUrl = localStorage.getItem("serverUrl");
    $scope.htmlContent = response.data["content-par/staticblock"]["items"][0]["htmlText"];
    $scope.pageImage = response.data["content-par/staticblock"]["items"][0]["pageImage"];
    if(serverUrl != null || serverUrl != ' '){
        $scope.pageImage = serverUrl + $scope.pageImage
    }
    $scope.pageTitle = response.data["content-par/staticblock"]["items"][0]["pageTitle"];

	var div1 = document.getElementById("htmlContent");
	var decoded = $("<div/>").html($scope.htmlContent).text();


	$scope.decodedHtml =decoded;

	$scope.headerHtml ="<div class='policy-title col'>"+
                            "<img src='"+$scope.pageImage+"' alt='store' class='pop-icons'>"+
                            "<p>"+$scope.pageTitle+"</p>"+
                        "</div>";

});
$scope.headerHtml = "";
$scope.headerClass = "row row-center policy-header";
$scope.ionContentClass = "";
$rootScope.redirectedFromProductCoupons =  false;
}])









// Controller for page 'faq'
.controller('contentphonegappaybackappspaybackenhomeprivacypolicyfaq', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/privacy-policy/faq.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'faq';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/privacy-policy/faq';
		$rootScope.angularPageTitle = $('<div/>').html('FAQ').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/privacy-policy';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        //

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});   
        } 

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* staticblock component controller (path: content-par/staticblock) */


    data.then(function(response) {
    var serverUrl = localStorage.getItem("serverUrl");
    $scope.htmlContent = response.data["content-par/staticblock"]["items"][0]["htmlText"];
    $scope.pageImage = response.data["content-par/staticblock"]["items"][0]["pageImage"];
    if(serverUrl != null || serverUrl != ' '){
        $scope.pageImage = serverUrl + $scope.pageImage
    }
    $scope.pageTitle = response.data["content-par/staticblock"]["items"][0]["pageTitle"];

	var div1 = document.getElementById("htmlContent");
	var decoded = $("<div/>").html($scope.htmlContent).text();


	$scope.decodedHtml =decoded;

	$scope.headerHtml ="<div class='policy-title col'>"+
                            "<img src='"+$scope.pageImage+"' alt='store' class='pop-icons'>"+
                            "<p>"+$scope.pageTitle+"</p>"+
                        "</div>";

});
$scope.headerHtml = "";
$scope.headerClass = "row row-center policy-header";
$scope.ionContentClass = "";
$rootScope.redirectedFromProductCoupons =  false;
}])









// Controller for page 'terms-and-conditions'
.controller('contentphonegappaybackappspaybackenhomeprivacypolicytermsandconditions', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/privacy-policy/terms-and-conditions.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'terms-and-conditions';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/privacy-policy/terms-and-conditions';
		$rootScope.angularPageTitle = $('<div/>').html('Terms and conditions').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/privacy-policy';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});   
        }


		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* staticblock component controller (path: content-par/staticblock) */


    data.then(function(response) {
    var serverUrl = localStorage.getItem("serverUrl");
    $scope.htmlContent = response.data["content-par/staticblock"]["items"][0]["htmlText"];
    $scope.pageImage = response.data["content-par/staticblock"]["items"][0]["pageImage"];
    if(serverUrl != null || serverUrl != ' '){
        $scope.pageImage = serverUrl + $scope.pageImage
    }
    $scope.pageTitle = response.data["content-par/staticblock"]["items"][0]["pageTitle"];

	var div1 = document.getElementById("htmlContent");
	var decoded = $("<div/>").html($scope.htmlContent).text();


	$scope.decodedHtml =decoded;

	$scope.headerHtml ="<div class='policy-title col'>"+
                            "<img src='"+$scope.pageImage+"' alt='store' class='pop-icons'>"+
                            "<p>"+$scope.pageTitle+"</p>"+
                        "</div>";

});
$scope.headerHtml = "";
$scope.headerClass = "row row-center policy-header";
$scope.ionContentClass = "";
$rootScope.redirectedFromProductCoupons =  false;
}])









// Controller for page 'store-locator'
.controller('contentphonegappaybackappspaybackenhomestorelocator', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/store-locator.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'store-locator';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/store-locator';
		$rootScope.angularPageTitle = $('<div/>').html('Store Locator').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }
        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});   
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* storelocator component controller (path: content-par/storelocator) */



    data.then(function(response) {

        $scope.Title = response.data["content-par/storelocator"].items;


    });
 $rootScope.ion="false";
  $scope.storeRadious = '5';
$scope.headerHtml = "<div class='distance col'>"+
                               "<p>Radius<br>"+
                               "<span>"+$scope.storeRadious+" </span> Km</p>"+
                    "</div>"+
                    "<div class='sl-title sl-header'>"+
                        "<img ng-src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/store_locator_icon.png' alt='store' width='20' height='20' id='store-logo' ng-if='!appExports'>" +
                        "<img ng-src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/store_locator_icon.png' alt='store' width='20' height='20' id='store-logo' ng-if='appExports'>" +
                        "<p>STORE LOCATOR</p>"+
                    "</div>"+
                    "<div class='col'>"+
                        "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/current_location_icon.png' width='25' height='25' ng-click='openPopoverfilter($event)' class='locator'>"+
                    "</div>";
$rootScope.headerClass = "row store-header row-center";
$scope.storeRedirectionUrl = "";
$rootScope.redirectedFromProductCoupons =  false;

var myOptions = [];
var serverUrl = "";
serverUrl = localStorage.getItem("serverUrl");
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
$scope.mapServerUrl = localStorage.getItem("serverUrl");
$scope.filterCheckClass = "checkbox sl-check-green";
$scope.checkBoxClass = "ion-checkmark-round sl-check-icon";

$scope.entities = [{
      name: 'five',
      checked: true,
    value : '5',
    radious : '5000'
    }, {
      name: 'ten',
      checked: false,
    value : '10',
    radious : '10000'
    },
  {
      name: 'fifteen',
      checked: false,
    value : '15',
    radious : '15000'
    }
  ]
var couponStrongToken = $scope.getloginToken();
  var couponweakToken = $scope.getweakloginToken();

  if(couponStrongToken!=null)
  {
    couponStrongToken = couponStrongToken;
  }else
  {
    couponStrongToken = couponweakToken;
  }




 var iconBase = '/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/store_locator.png';
 var map;



    $ionicPopover.fromTemplateUrl('filter-pop', {
    scope: $scope,
  }).then(function(popoverfilter) {
    $scope.popoverfilter = popoverfilter;
  });
  $scope.openPopoverfilter = function($event) {
    $scope.popoverfilter.show($event);
  };
  $scope.closePopover = function() {
    $scope.popoverfilter.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popoverfilter.remove();
  });
  // Execute action on hide popover
  $scope.$on('popoverfilter.hidden', function() {
    // Execute action
  });
  // Execute action on remove popover
  $scope.$on('popoverfilter.removed', function() {
  });


  var initialRadious = '5000';
    initialize($scope,$http);




var image = '/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/bigbazaar1.png';

var markers = [];



var currentLatitude = null;
var currentLongitude = null;
var myLatlng = null;
var marker = null;


function initialize($scope,$http) {


  $scope.showLoading();

  var locationsFromResponse = "";
  var markers = Array();
  var  infowindows = Array();
  var marker;
  var active_info = null;

  if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(success_mapcallback, error_mapcallback, {timeout:10000});

	}





}

function success_mapcallback(position)
{
		var latitude = position.coords.latitude;
		var longitude = position.coords.longitude;

		constructMap(latitude,longitude);
		currentLatitude = latitude;
		currentLongitude = longitude;


}

function error_mapcallback(PositionError)
{

	var latitude = "21";
	var longitude = "77";
	constructDefaultMap(latitude,longitude);
	currentLatitude = latitude;
	currentLongitude = longitude;
	$rootScope.hideLoadingroot();
}


function constructDefaultMap(latitude,longitude,$rootScope,$scope)
{
	 directionsDisplay = new google.maps.DirectionsRenderer();
	 initialRadious = "5000";
		try
		{
			 var pos = new google.maps.LatLng(latitude,longitude);
             var mapOptions = {
              zoom: 5,
              center:pos,
              mapTypeId: google.maps.MapTypeId.ROADMAP



            };
            map = new google.maps.Map(document.getElementById('geolocation'),
                  mapOptions);
			$rootScope.hideLoadingroot();
		}catch(error)
		{
				//$rootScope.hideLoadingroot();
		}


     directionsDisplay.setMap(map);




}

function constructMap(lat,long)
{
 try
	{
	directionsDisplay = new google.maps.DirectionsRenderer();
  if(lat == null && long == null)
	{
		$rootScope.errorMsg =  "Please enable GPS or change location.";
     	$rootScope.showError();
	}else
{
  var currentLocation  =  null;
  var initialRadious = '5000';

  var pos = new google.maps.LatLng(lat,long);
  var mapOptions = {
      zoom: 12,
     center:pos



    };
  	map = new google.maps.Map(document.getElementById('geolocation'),
          mapOptions);
   	var multiplemarker;
    var infowindow = null;
    var content = "";
	var myOptions = [];
    var branchDisplayName = "";
	if($rootScope.couponsCall)
	{
		initialRadious = "30000";
		$scope.getPartnerBranches(lat,long,initialRadious);
	}else
	{
		$scope.getMapBranches(lat,long,initialRadious);
	}
	myLatlng = new google.maps.LatLng(lat,long);
	marker = new google.maps.Marker({
    position: myLatlng,
    map: map
});

}
//markers.push(marker);
 directionsDisplay.setMap(map);
}catch(error)
{
	//$rootScope.hideLoadingroot();
}

};

$scope.getPartnerBranches = function(lat,long,radious)
{
	console.log("partner short name from coupons"+$rootScope.couponsPartnerShortName);

	$scope.hideLoading();

	var serverUrl = localStorage.getItem("serverUrl");
        if (serverUrl == null || serverUrl == '') {
            serverUrl = '/payback/anon/stores.html/branchesforpartner';
        } else {
            serverUrl = serverUrl+'/payback/anon/stores.html/branchesforpartner';
        }

	if(radious!="")
	{
		var getBranchesData = {'latitude' : lat,'longitude' : long,'searchDistance' : radious, 'partner' : $rootScope.couponsPartnerShortName   };
	}else
	{
		var getBranchesData = {'latitude' : lat,'longitude' : long,'partner' : $rootScope.couponsPartnerShortName };
	}
	 var partnerbranchesDataPromise =  externalService.callService(serverUrl,getBranchesData,'POST');
	 partnerbranchesDataPromise.then(function(partnerResult){
		if(partnerResult['extint.BranchList'])
		 {

            if(partnerResult['extint.BranchList'])
            {
                if(partnerResult['extint.BranchList'].length>0)
                {
                    $scope.singleBranch = false;
                }else
                {
                    $scope.singleBranch = true;
                }

            }else
            {
                $scope.singleBranch = true;
            }
                  $scope.branchList = partnerResult['extint.BranchList'];
                  $scope.hideLoading();
                  $scope.setBranchesMarkers(partnerResult);
                  $scope.constructPartnerBrands(partnerResult);
		 	}else
{
  if(partnerResult['message'])
  {

    if(radious == 15 && partnerResult['message'] == "No Branches are available for the location.")
		{
			$rootScope.errorMsg = "No stores located at this radius.";
		}else
		{
			$rootScope.errorMsg =   result['message'];
		}
    $rootScope.showError();
    $scope.hideLoading();

  }else
    {
      if(partnerResult['error'])
      {
        $scope.hideLoading();
        $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
        $rootScope.showError();
      }
    }
}

	});
};

$scope.getMapBranches =  function(lat,long,radious)
{

    /* get Branches call */


if(couponStrongToken == null)
{

  var serverUrl = localStorage.getItem("serverUrl");
        if (serverUrl == null || serverUrl == '') {
            serverUrl = '/payback/anon/stores.html/branch/list';
        } else {
            serverUrl = serverUrl+'/payback/anon/stores.html/branch/list';
        }
	if(radious!="")
	{
		var getBranchesData = {'latitude' : lat,'longitude' : long,'searchDistance' : radious};
	}else
	{
		var getBranchesData = {'latitude' : lat,'longitude' : long};
	}

    var branchesDataPromise =  externalService.callService(serverUrl,getBranchesData,'POST');
  //var branchesDataPromise =  branches.getBranches(lat,long,radious);
  branchesDataPromise.then(function(result) {
  if(result['extint.BranchList'])
{

if(result['extint.BranchList'])
{
	if(result['extint.BranchList'].length>0)
	{
		$scope.singleBranch = false;
	}else
	{
		$scope.singleBranch = true;
	}

}else
{
	$scope.singleBranch = true;
}
  $scope.branchList = result['extint.BranchList'];
  $scope.hideLoading();
  $scope.setBranchesMarkers(result);
  $scope.constructPartnerBrands(result);
}else
{
  if(result['message'])
  {

    if(radious == 15 && result['message'] == "No Branches are available for the location.")
		{
			$rootScope.errorMsg = "No stores located at this radius.";
		}else
		{
			$rootScope.errorMsg =   result['message'];
		}
    $rootScope.showError();
    $scope.hideLoading();

  }else
    {
      if(result['error'])
      {
        $scope.hideLoading();
        $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
        $rootScope.showError();
      }
    }
}
});


}else
{

  var serverUrl = localStorage.getItem("serverUrl");
           if (serverUrl == null || serverUrl == '') {
                serverUrl = '/payback/secure/serviceintegration.html/brancheswithcoupons/list';
           } else {
                serverUrl = serverUrl+'/payback/secure/serviceintegration.html/brancheswithcoupons/list';
           }

var getBranchesWithCouponsData = {'latitude' : lat,'longitude' : long,'token':couponStrongToken,distributionChannel:'5','searchDistance' : radious};
  var branchesWithCouponsDataPromise =  externalService.callService(serverUrl,getBranchesWithCouponsData,'POST');
//var branchesWithCouponsDataPromise = branchesWithCoupons.getBranchesWithCoupons(lat,long,radious,couponStrongToken);
  branchesWithCouponsDataPromise.then(function(result) {
if(result['extint.BranchList'])
{
	if(result['extint.BranchList'].length >0)
		{
			$scope.singleBranch = false;
		}else
		{
			$scope.singleBranch = true;
		}

}else
{
	$scope.singleBranch = true;
}
  if(result['extint.BranchList'])
  {
    $scope.hideLoading();
    $scope.branchList = result['extint.BranchList'];
    $scope.setBranchesWithCoupons(result);
    $scope.constructPartnerBrands(result);
  }else
  {
    if(result['message'])
    {
	  if(radious == 15 && result['message'] == "No Branches are available for the location.")
		{
			$rootScope.errorMsg = "No stores located at this radius.";
		}else
		{
			$rootScope.errorMsg =   result['message'];
		}
      $scope.hideLoading();

      $rootScope.showError();
    }else
    {
      if(result['error'])
      {
        $scope.hideLoading();
        $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
        $rootScope.showError();
      }
    }
  }



});


}


/* end of get Branches call */

};
$scope.updateSelection = function(checkposition, entities) {
        $scope.closePopover();
        $scope.showLoading();
            angular.forEach(entities, function(subscription, index) {
           if (checkposition != index)
        {
                subscription.checked = false;
        }else{

          $scope.storeRadious = subscription.value;
          $scope.storeRadiousValue = subscription.radious;

		 $scope.storeRadious = $scope.storeRadiousValue;

		$scope.headerHtml = "<div class='distance col'>"+
                               "<p>Radius<br>"+
"<span>"+$scope.storeRadious/1000+" </span> Km</p>"+
                    "</div>"+
                    "<div class='sl-title sl-header'>"+
                        "<img ng-src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/store_locator_icon.png' alt='store' width='20' height='20' id='store-logo' ng-if='!appExports'>" +
                        "<img ng-src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/store_locator_icon.png' alt='store' width='20' height='20' id='store-logo' ng-if='appExports'>" +
                        "<p>STORE LOCATOR</p>"+
                    "</div>"+
                    "<div class='col'>"+
                        "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/current_location_icon.png' width='25' height='25' ng-click='openPopoverfilter($event)' class='locator'>"+
                    "</div>";

			if(currentLatitude != null && currentLongitude != null)
				{
					$scope.getMapBranches(currentLatitude,currentLongitude,$scope.storeRadiousValue);

				}else
				{
					$rootScope.errorMsg =  "Please enable GPS or change location.";
      				$rootScope.showError();
					$scope.hideLoading();
				}




  }



          });

          document.getElementById("view").checked = true;
                         $scope.filterCheckClass = "checkbox sl-check-green";
                         $scope.checkBoxClass = "ion-checkmark-round sl-check-icon";
        }


$scope.visible = false;

  $scope.toggle = function(){

    $scope.visible = !$scope.visible;
	$scope.closePopover();
  }
$scope.changeLocation = function()
        {
		   $scope.toggle();
          $scope.closePopover();
          $scope.showLoading();
          var changeLocation  = $("#searchArea").val();

          var changedLat  = "";
          var changedLong = "";
          var url = "https://maps.googleapis.com/maps/api/geocode/json";
		  var locationdata={'address' : changeLocation };
		  var locationMethod = "GET";
		  var locationResponse =  externalService.callService(url,locationdata,'GET');
			locationResponse.then(function(locationdata) {

						  if(locationdata['status']=='ZERO_RESULTS')
							{
								 $scope.hideLoading();
								 $rootScope.errorMsg =  "No location found.";
      							 $rootScope.showError();
							}else
							{
							deleteMarkers();
							if(marker)
							{
								marker.setMap(null);
							}



                          changedLat = locationdata["results"][0]["geometry"]["location"]["lat"];
                          changedLong = locationdata["results"][0]["geometry"]["location"]["lng"];
						  currentLatitude = changedLat;
						  currentLongitude =  changedLong;
                          marker  = new google.maps.Marker({
                                                position: new google.maps.LatLng(currentLatitude,currentLongitude),
                                                optimized: false,
                                                map: map
                                    });

                        window.setTimeout(function() {
                           map.panTo(marker.getPosition());
                          }, 3000);
							markers.push(marker);
                          if(couponStrongToken == null)
                            {
								$scope.hideLoading();
                                var branchesDataPromise =  branches.getBranches(changedLat,changedLong,'5000');
                                branchesDataPromise.then(function(result) {
								if(result['message'])
							{
								 $rootScope.errorMsg =   result['message'];
      							 $rootScope.showError();
                                 $scope.constructPartnerBrands(result);

							}else
							{
								if(result['error'])
								{
									 $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
      							 	 $rootScope.showError();
									 $scope.constructPartnerBrands(result);
								}else
								{
									 $scope.setBranchesMarkers(result);
									 $scope.constructPartnerBrands(result);

								 if(result['extint.BranchList'])
									{
										if(result['extint.BranchList'].length>0)
										{
												$scope.singleBranch = false;
										}else
										{
												$scope.singleBranch = true;
										}


										$scope.branchList = result['extint.BranchList'];
									}else
									{
										$scope.singleBranch = true;
										$scope.branchList = result['extint.BranchList'];
									}
								}
							}


                          });


                        }else
                        {
						  $scope.hideLoading();
                          var branchesWithCouponsDataPromise = branchesWithCoupons.getBranchesWithCoupons(changedLat,changedLong,'5000',couponStrongToken);
                          branchesWithCouponsDataPromise.then(function(result) {
						  if(result['message'])
							{
								 $rootScope.errorMsg =   result['message'];
      							 $rootScope.showError();
								$scope.constructPartnerBrands(result);
							}else
							{
								if(result['error'])
								{
									 $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
      							 	 $rootScope.showError();
									$scope.constructPartnerBrands(result);
								}else
								{
									$scope.setBranchesWithCoupons(result);
									$scope.constructPartnerBrands(result);
									if(result['extint.BranchList'].length>0)
										{
												$scope.singleBranch = false;
										}else
										{
												$scope.singleBranch = true;
										}
									$scope.branchList = result['extint.BranchList'];

								}
							}


                          });
                        }

							}


			});


               document.getElementById("view").checked = true;
               $scope.filterCheckClass = "checkbox sl-check-green";
               $scope.checkBoxClass = "ion-checkmark-round sl-check-icon";

        }


var getBrachesContent = "";
$scope.setBranchesMarkers =  function(data)
{


  if(data['error'])
{
$rootScope.errorMsg = "Not able to connect to PAYBACK and please try again.";

$rootScope.showError();
$scope.go("/content/phonegap/payback/apps/payback/en/home");
$scope.hideLoading();
}else
{

deleteMarkers();

  myOptions  = [];
  $scope.hideLoading();
// var resposnseLength = data["extint.BranchList"].length;
  if(data["extint.BranchList"])
  {


	if(data["extint.BranchList"].length)
	{
		for(var j=0;j<data["extint.BranchList"].length;j++){

        var latitude  = data["extint.BranchList"][j]["extint.BranchPosition"]["types.Latitude"]; var additionalline = data["extint.BranchList"][j]["extint.BranchInfo"]["types.BranchPostalAddress"]["types.AdditionalAddress1"];
        var longitude  = data["extint.BranchList"][j]["extint.BranchPosition"]["types.Longitude"];
		var partnerShortName = data["extint.BranchList"][j]["extint.BranchInfo"]["types.BranchContext"]["types.PartnerShortName"];
		image = serverUrl + data["extint.BranchList"][j]["extint.BranchInfo"]["types.BranchContext"]["partnerMapLogo"];

    	var multiplemarker  = new google.maps.Marker({
                      id : partnerShortName,
                                            position: new google.maps.LatLng(latitude,longitude),
                                            icon: image,
                                            optimized: false,
                                            map: map
                        });



  			setMarkers(j,multiplemarker);
			marker  = new google.maps.Marker({
                                                position: new google.maps.LatLng(currentLatitude,currentLongitude),
                                                optimized: false,
                                                map: map
                                    });
markers.push(marker);

		}
	}else
	{
	     var latitude  = data["extint.BranchList"]["extint.BranchPosition"]["types.Latitude"];
	var longitude  = data["extint.BranchList"]["extint.BranchPosition"]["types.Longitude"];
	var partnerShortName = data["extint.BranchList"]["extint.BranchInfo"]["types.BranchContext"]["types.PartnerShortName"];
	image = serverUrl + data["extint.BranchList"]["extint.BranchInfo"]["types.BranchContext"]["partnerMapLogo"];


    var multiplemarker  = new google.maps.Marker({
                      id : partnerShortName,
                                            position: new google.maps.LatLng(latitude,longitude),
                                            icon: image,
                                            optimized: false,
                                            map: map
                        });



  		setMarkers(0,multiplemarker);
        marker  = new google.maps.Marker({
                                                position: new google.maps.LatLng(currentLatitude,currentLongitude),
                                                optimized: false,
                                                map: map
                                    });
		markers.push(marker);

	}


  }else
  {


	var latitude  = data["extint.BranchPosition"]["types.Latitude"];
	var longitude  = data["extint.BranchPosition"]["types.Longitude"];
	var partnerShortName = data["extint.BranchInfo"]["types.BranchContext"]["types.PartnerShortName"];
	image = serverUrl + data["extint.BranchInfo"]["types.BranchContext"]["partnerMapLogo"];


    var multiplemarker  = new google.maps.Marker({
                      id : partnerShortName,
                                            position: new google.maps.LatLng(latitude,longitude),
                                            icon: image,
                                            optimized: false,
                                            map: map
                        });



  setMarkers(0,multiplemarker);
  marker  = new google.maps.Marker({
                                                position: new google.maps.LatLng(currentLatitude,currentLongitude),
                                                optimized: false,
                                                map: map
                                    });
markers.push(marker);

  }






  }
}



$scope.setBranchesWithCoupons = function(data)
{
  myOptions  = [];
  var couponsMarkerResponseLength = data["extint.BranchList"].length;

  deleteMarkers();
  if(data["extint.BranchList"].length)
  {
  for(var k=0;k<couponsMarkerResponseLength;k++)
  {
    	var branchesWithCoupons = "";
    	var branchDisplayNameCoupons = data["extint.BranchList"][k]["extint.BranchInfo"]["types.BranchContext"]["types.PartnerDisplayName"];
    	var streetCoupons = data["extint.BranchList"][k]["extint.BranchInfo"]["types.BranchPostalAddress"]["types.Street"];
        var zipCodeCoupons = data["extint.BranchList"][k]["extint.BranchInfo"]["types.BranchPostalAddress"]["types.ZipCode"];
        var cityCoupons =  data["extint.BranchList"][k]["extint.BranchInfo"]["types.BranchPostalAddress"]["types.City"];
        var stateCoupons = data["extint.BranchList"][k]["extint.BranchInfo"]["types.BranchPostalAddress"]["types.Province"];
        var latitudeCoupons  = data["extint.BranchList"][k]["extint.BranchPosition"]["types.Latitude"];
        var additionallineCoupons = data["extint.BranchList"][k]["extint.BranchInfo"]["types.BranchPostalAddress"]["types.AdditionalAddress1"];
        var longitudeCoupons  = data["extint.BranchList"][k]["extint.BranchPosition"]["types.Longitude"];
    	var distanceFromBranchCoupons  = data["extint.BranchList"][k]["extint.BranchDistance"];
    	var partnerShortNameCoupons = data["extint.BranchList"][k]["extint.BranchInfo"]["types.BranchContext"]["types.PartnerShortName"];
		image = serverUrl + data["extint.BranchList"][k]["extint.BranchInfo"]["types.BranchContext"]["partnerMapLogo"];




        var couponsListSize  = "";
    	if(data["extint.BranchList"][k]["couponsList"])
      	{
           couponsListSize  = data["extint.BranchList"][k]["couponsList"].length;
      	}

        if(couponsListSize > 0)
        {
            image = image = serverUrl + data["extint.BranchList"][k]["extint.BranchInfo"]["types.BranchContext"]["partnerMapCouponslogo"];
        }
    var multiplemarker  = new google.maps.Marker({
                      						id : partnerShortNameCoupons,
                                            position: new google.maps.LatLng(latitudeCoupons,longitudeCoupons),
                                            icon: image,
                                            optimized: false,
                                            map: map
                        });

    if(couponsListSize == 0)
    {

      setMarkers(k,multiplemarker);

    }else
    {

       setMarkers(k,multiplemarker);

    }
  }
}else
{
	    var branchesWithCoupons = "";
    	var branchDisplayNameCoupons = data["extint.BranchList"]["extint.BranchInfo"]["types.BranchContext"]["types.PartnerDisplayName"];
    	var streetCoupons = data["extint.BranchList"]["extint.BranchInfo"]["types.BranchPostalAddress"]["types.Street"];
        var zipCodeCoupons = data["extint.BranchList"]["extint.BranchInfo"]["types.BranchPostalAddress"]["types.ZipCode"];
        var cityCoupons =  data["extint.BranchList"]["extint.BranchInfo"]["types.BranchPostalAddress"]["types.City"];
        var stateCoupons = data["extint.BranchList"]["extint.BranchInfo"]["types.BranchPostalAddress"]["types.Province"];
        var latitudeCoupons  = data["extint.BranchList"]["extint.BranchPosition"]["types.Latitude"];
        var additionallineCoupons = data["extint.BranchList"]["extint.BranchInfo"]["types.BranchPostalAddress"]["types.AdditionalAddress1"];
        var longitudeCoupons  = data["extint.BranchList"]["extint.BranchPosition"]["types.Longitude"];
    	var distanceFromBranchCoupons  = data["extint.BranchList"]["extint.BranchDistance"];
    	var partnerShortNameCoupons = data["extint.BranchList"]["extint.BranchInfo"]["types.BranchContext"]["types.PartnerShortName"];
		image = serverUrl + data["extint.BranchList"]["extint.BranchInfo"]["types.BranchContext"]["partnerMapLogo"];




        var couponsListSize  = "";
    	if(data["extint.BranchList"]["couponsList"])
      	{
           couponsListSize  = data["extint.BranchList"]["couponsList"].length;
      	}
      	if(couponsListSize >0)
      	{
      		image = serverUrl + data["extint.BranchList"]["extint.BranchInfo"]["types.BranchContext"]["partnerMapCouponslogo"];
      	}


    var multiplemarker  = new google.maps.Marker({
                      						id : partnerShortNameCoupons,
                                            position: new google.maps.LatLng(latitudeCoupons,longitudeCoupons),
                                            icon: image,
                                            optimized: false,
                                            map: map
                        });

    if(couponsListSize == 0)
    {

      setMarkers(0,multiplemarker);

    }else
    {

       setMarkers(0,multiplemarker);

    }
}

};


$scope.constructPartnerBrands = function(data)
{

		if(data['message'])
							{

	$scope.partnerHtml = "";
}else
{
if(data['error']){
	$scope.partnerHtml = "";
}else
{
	$scope.partnerHtml = data['partnerList'];
}
}



};





function setAllMap(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

try
{
	google.maps.event.addDomListener(window, "resize", function() {

     var center = map.getCenter();
     google.maps.event.trigger(map, "resize");
     map.setCenter(center);
    });
}catch(error)
{
	console.log("error"+error);
}
var selectedmarker = null;
directionsDisplay = new google.maps.DirectionsRenderer();
$scope.getDirection = function(markerb)
{



if(markerb)
{
	selectedmarker = markerb;
}

try{
myLatlng = new google.maps.LatLng(currentLatitude,currentLongitude);
 var request = {
      origin: myLatlng,
      destination: markerb.getPosition(),
      travelMode: google.maps.TravelMode.DRIVING,
	  unitSystem: google.maps.DirectionsUnitSystem.METRIC

  };

directionsService.route(request, function(result, status) {
    if (status == google.maps.DirectionsStatus.OK) {
      directionsDisplay.setDirections(result);
    }
  });
directionsDisplay.setMap(map);
directionsDisplay.setOptions({
			suppressMarkers : true
		});
//var directionRestUrl = "https://www.google.es/maps/dir/'"+myLatlng.lat()+","+myLatlng.lng()+"'/'"+markerb.getPosition().lat()+","+markerb.getPosition().lng()+"'";
var directionRestUrl = "https://www.google.es/maps/dir/"+myLatlng.lat()+","+myLatlng.lng()+"/"+markerb.getPosition().lat()+","+markerb.getPosition().lng()+"";
console.log("directionRestUrl ::"+directionRestUrl);
$scope.storeRedirectionUrl = directionRestUrl;


}catch(err)
{
	console.log("err in get direction"+err);
}
};

var infowindow = null;

var action = 'all';
$scope.showallnone = function($event, id) {

        var checkbox = $event.target;
        action = (checkbox.checked ? 'all' : 'none');
    if(action == 'none')
      {
		directionsDisplay.setMap();

        clearMarkers();
        $scope.filterCheckClass = "checkbox sl-check-red";
        $scope.checkBoxClass = "ion-close-round sl-check-icon";
      }else

      {
        setAllMap(map);
        $scope.filterCheckClass = "checkbox sl-check-green";
        $scope.checkBoxClass = "ion-checkmark-round sl-check-icon";
      }


}

function clearMarkers() {
  setAllMap(null);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {



  clearMarkers();
  markers = [];
}

function setMarkers(count,multiplemarker)
{

  myOptions[count] = {

        disableAutoPan: true,
        maxWidth: 0,
        alignBottom: true,
        pixelOffset: new google.maps.Size(-16, -11),
        zIndex: null,
        boxClass: "info-windows",
        closeBoxURL: "",
        pane: "floatPane",
        enableEventPropagation: false,
        infoBoxClearance: "20px"
      };
	var previousid= "";
	google.maps.event.addListener(multiplemarker, 'click', (function(multiplemarker, count) {
            return function() {

              this.map.panTo(multiplemarker.getPosition());
              var markeid = "#branchDetail"+count;
			  var lastClicked =	localStorage.getItem("preiviousClicked");
			  if(lastClicked)
				{
					$(lastClicked).removeClass("col store-details ng-show").addClass("col store-details ng-hide");
					localStorage.setItem("preiviousClicked",markeid);
				}
			  else
				{
					localStorage.setItem("preiviousClicked",markeid);
				}
              $(markeid).removeClass("col store-details ng-hide").addClass("col store-details ng-show");


                     if(infowindow){
                    infowindow.close();
                    }
				$scope.getDirection(multiplemarker);

               }

          })(multiplemarker, count));

   markers.push(multiplemarker);
}



$scope.removeMarker = function(markerId,filterCheckClassid,checkBoxClassid)
{

	var checkedCount = null;

  var filterCheckClass = document.getElementById(filterCheckClassid).className;
  var checkBoxClass = document.getElementById(checkBoxClassid).className;

  if(filterCheckClass == 'checkbox sl-check-green activated' || filterCheckClass == 'checkbox sl-check-green')
  {

        $("#"+filterCheckClassid).removeClass("checkbox sl-check-green").addClass("checkbox sl-check-red");
        $("#"+checkBoxClassid).removeClass("ion-checkmark-round sl-check-icon").addClass("ion-close-round sl-check-icon");

        var markerForRemove = null;
  for(var i=0;i<markers.length;i++)
  {

    var markerIdFrom = markers[i].id;
    if(markerIdFrom == markerId)
    {
      markerForRemove = markers[i];
      markerForRemove.setMap(null);

	  if(directionsDisplay != null) {
  	 		directionsDisplay.setMap(null);

		}

    }
  }


	checkedCount = $('.sl-check-green').length;
if(checkedCount == 0)
	{
		$('#view').prop('checked', false);
	}else
	{
		 $('#view').prop('checked', true);
	}


  }else
  {


$("#"+filterCheckClassid).removeClass("checkbox sl-check-red").addClass("checkbox sl-check-green");
$("#"+checkBoxClassid).removeClass("ion-close-round sl-check-icon").addClass("ion-checkmark-round sl-check-ico");

        for(var i=0;i<markers.length;i++)
  {

    var markerIdFrom = markers[i].id;
    if(markerIdFrom == markerId)
    {
      markerForRemove = markers[i];

      markerForRemove.setMap(map);
    }
  }
checkedCount = $('.sl-check-green').length;
if(checkedCount == 0)
	{
		$('#view').prop('checked', false);
	}else
	{
		 $('#view').prop('checked', true);
	}

  }


}



$scope.closeMapDetail = function(storeDetails)
{

storeDetails = "#"+storeDetails;

$(storeDetails).removeClass("col store-details ng-show").addClass("col store-details ng-hide");

}



$scope.activateCoupons = function(couponid)

{

  var couponId = couponid["extint.Coupon"]["types.CouponID"];

  var finalUrl = serverUrl;
  if (finalUrl == null || finalUrl == '') {
    finalUrl = '/payback/wksecure/offers.html/coupon/activate';
  } else {
    finalUrl = serverUrl + '/payback/wksecure/offers.html/coupon/activate';
  }
  $scope.showLoading();
  $http({
                                                method:'POST',
                                                url:finalUrl,
                                               params: {
                                                                       token : couponStrongToken,
                                     couponId : couponId


                                                      }


                                              }).success(function (data, status, headers, config) {
                                                if(data["types.FaultMessage"])
                                                {
                                                    $scope.hideLoading();
                                                    var errorMsg = data["types.FaultMessage"]["types.Message"];
                                                    $rootScope.successMsg = errorMsg;
                                                    $scope.showSuccess();
                                                    $scope.closePop();
                                                    $scope.closeMapDetail();
                                                }else
                                                {
                                                    $scope.hideLoading();
                                                    $('#'+couponId).html('ACTIVATED');
                                                    $('#'+couponId).parent().removeClass( "inactive-coupon" ).addClass( "active-coupon" );
                                                    $('#'+couponId).attr('disabled','disabled');
                                                    $rootScope.successMsg = "Coupon successfully activated!";
                                                    $scope.showSuccess();
                                                    $scope.closePop();
                                                    $scope.closeMapDetail();
													// if(window.ADB)
        					// 						{
             //   											ADB.trackAction("storeCouponsActivation");
        					// 						}
                                                }
                                              }).error(function (data, status, headers, config) {
                                                  $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
                          						  $rootScope.showError();
                                                  throw new Error('Something went wrong...');
                                              });
}


$scope.shopNow = function(item)
{
var redirectionurl = "";
if(item['extint.Coupon']['DisplayTexts']['onlineShopUrl']!=undefined)
{
  redirectionurl = item['extint.Coupon']['DisplayTexts']['onlineShopUrl'];
  //$window.location.href = redirectionurl;

  window.open(redirectionurl,"_system");
}

}
$scope.goToMap = function()
{
window.open($scope.storeRedirectionUrl,"_sytem");
};


}])









// Controller for page 'contact-us'
.controller('contentphonegappaybackappspaybackenhomecontactus', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/contact-us.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'contact-us';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/contact-us';
		$rootScope.angularPageTitle = $('<div/>').html('Contact Us').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});   
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* staticblock component controller (path: content-par/staticblock) */


    data.then(function(response) {
    var serverUrl = localStorage.getItem("serverUrl");
    $scope.htmlContent = response.data["content-par/staticblock"]["items"][0]["htmlText"];
    $scope.pageImage = response.data["content-par/staticblock"]["items"][0]["pageImage"];
    if(serverUrl != null || serverUrl != ' '){
        $scope.pageImage = serverUrl + $scope.pageImage
    }
    $scope.pageTitle = response.data["content-par/staticblock"]["items"][0]["pageTitle"];

	var div1 = document.getElementById("htmlContent");
	var decoded = $("<div/>").html($scope.htmlContent).text();


	$scope.decodedHtml =decoded;

	$scope.headerHtml ="<div class='policy-title col'>"+
                            "<img src='"+$scope.pageImage+"' alt='store' class='pop-icons'>"+
                            "<p>"+$scope.pageTitle+"</p>"+
                        "</div>";

});
$scope.headerHtml = "";
$scope.headerClass = "row row-center policy-header";
$scope.ionContentClass = "";
$rootScope.redirectedFromProductCoupons =  false;
}])









// Controller for page 'coupons'
.controller('contentphonegappaybackappspaybackenhomecoupons', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/coupons.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'coupons';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/coupons';
		$rootScope.angularPageTitle = $('<div/>').html('Coupons').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});   
        }
		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	
	





    /* carousel_0 component controller (path: content-par/carousel_0) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;


	





    /* gridcarousel component controller (path: content-par/gridcarousel) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/coupons/jcr:content/content-par/gridcarousel";
    var gridCarouselDynamicPath = "content_par_gridcarousel";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}
	





    /* gridcarousel_0 component controller (path: content-par/gridcarousel_0) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/coupons/jcr:content/content-par/gridcarousel_0";
    var gridCarouselDynamicPath = "content_par_gridcarousel_0";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}

}])









// Controller for page 'social-network'
.controller('contentphonegappaybackappspaybackenhomesocialnetwork', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/social-network.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'social-network';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/social-network';
		$rootScope.angularPageTitle = $('<div/>').html('Social Network').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* socialblock component controller (path: content-par/socialblock) */
data.then(function(response) {
    $scope.componentResponse = response.data["content-par/socialblock"];

});
$scope.headerHtml = "<div class='social-title col'>"+
                        "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/social_icon.png' alt='store' id='brand-logo' ng-if='!appExports'>"+
                        "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/social_icon.png' alt='store' id='brand-logo' ng-if='appExports'>"+
                        "<p>SOCIAL NETWORKS</p>"+
                     "</div>";
$rootScope.headerClass = "row row-center social-header";

 $scope.facebookGo = function()
{
    var scheme;
    if(device) {
        if(device.platform == 'Android') {
            scheme = 'com.facebook.katana';
        }
        else if(device.platform == 'iOS') {
            scheme = 'fb://';
        }
        appAvailability.check(
            scheme,       // URI Scheme or Package Name
            function() {  // Success callback
                window.open('fb://page/164070713638584','_system');
            },
            function() {  // Error callback
                window.open('https://m.facebook.com/PaybackIndia','_system');
            }
        );
    }
    else {
        window.open('https://m.facebook.com/PaybackIndia','_system');
    }
}


	
 $scope.shareAnywhere = function() {
        if(device.platform == 'Android') {
            window.plugins.socialsharing.share('Try out PAYBACK app - Hey, have you tried PAYBACK new mobile app yet ? I think you would like it. market://details?id=com.adobe.cq.mobile.payback ');
        }
        else if(device.platform == 'iOS') {
            window.plugins.socialsharing.share('Try out PAYBACK app - Hey, have you tried PAYBACK new mobile app yet ? I think you would like it. https://itunes.apple.com/in/app/payback-india/id961049647?mt=8 ');
        }
    }
    
   $scope.ratePayback = function() {
        if(device.platform == 'Android') {
   		    window.open("market://details?id=com.adobe.cq.mobile.payback","_system");
   		}
   		else if(device.platform == 'iOS') {
   		    window.open("https://itunes.apple.com/in/app/payback-india/id961049647?mt=8","_system");
   		}
   }
   $rootScope.redirectedFromProductCoupons =  false;
}])









// Controller for page 'partner-brands'
.controller('contentphonegappaybackappspaybackenhomepartnerbrands', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/partner-brands.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'partner-brands';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/partner-brands';
		$rootScope.angularPageTitle = $('<div/>').html('Partner Brands').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* partnerbrands_0 component controller (path: content-par/partnerbrands_0) */


data.then(function(response) {



});
$scope.headerHtml = "<div class='brand-title col'>"+
                     "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/brand_icon_new.png' alt='store' width='20' height='20' id='brand-logo' ng-if='!appExports'>"+
                     "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/brand_icon_new.png' alt='store' width='20' height='20' id='brand-logo' ng-if='appExports'>"+
                     "<p>PARTNER BRANDS</p></div>";
$rootScope.headerClass = "row row-center brand-header";

$scope.constructPartners = function(brandDataResponse) {
    try {
        $scope.componentResponse = brandDataResponse["items"];
        if($scope.componentResponse) {
            $scope.contentWrapper = "brand-content-header";
            $scope.brandCarouselContent = "<div class='card brand-info-wrapper' id='brandCard'>";
            if($scope.componentResponse.length>0) {
                for(var i=0;i<$scope.componentResponse.length;i++) {
                    if($scope.componentResponse[i]['buttonText']&&$scope.componentResponse[i]['brandUrl']) {
                        if($scope.appExports) {
                            var noImagePath = "../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/no_image1.png";
                            if(serverUrl){
                                $scope.brandCarouselContent =  $scope.brandCarouselContent+"<div><div class='row brand-info-header'><img ng-src='"+serverUrl+$scope.componentResponse[i]['imagePath']+"' onError=this.onerror=null;this.src='"+noImagePath+"'/><span class='ion-close-circled brand-close popupclose' ng-click='closeBrand()'></span></div><div class='brand-info'><p data-dd-collapse-text='150'>"+$scope.componentResponse[i]['brandsdescription']+"</p></div><div class='col brandpop-button'>"+$scope.componentResponse[i]['pointsdescription']+"</div> <div class='row coupons-btn-wrapper'><div class='brand-site-button coupons-button' ng-click=openBrandPage('"+$scope.componentResponse[i]['brandUrl']+"')>"+$scope.componentResponse[i]['buttonText']+"</div></div></div>";
                            }
                            else{
                                $scope.brandCarouselContent =  $scope.brandCarouselContent+"<div><div class='row brand-info-header'><img ng-src='https://mobi.payback.in"+$scope.componentResponse[i]['imagePath']+"' onError=this.onerror=null;this.src='"+noImagePath+"'/><span class='ion-close-circled brand-close popupclose' ng-click='closeBrand()'></span></div><div class='brand-info'><p data-dd-collapse-text='150'>"+$scope.componentResponse[i]['brandsdescription']+"</p></div><div class='col brandpop-button'>"+$scope.componentResponse[i]['pointsdescription']+"</div> <div class='row coupons-btn-wrapper'><div class='brand-site-button coupons-button' ng-click=openBrandPage('"+$scope.componentResponse[i]['brandUrl']+"')>"+$scope.componentResponse[i]['buttonText']+"</div></div></div>";
                            }
                        }
                        else {
                            var noImagePath = "/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/no_image1.png";
                            $scope.brandCarouselContent =  $scope.brandCarouselContent+"<div><div class='row brand-info-header'><img ng-src='"+$scope.componentResponse[i]['imagePath']+"' onError=this.onerror=null;this.src='"+noImagePath+"'/><span class='ion-close-circled brand-close popupclose' ng-click='closeBrand()'></span></div><div class='brand-info'><p data-dd-collapse-text='150'>"+$scope.componentResponse[i]['brandsdescription']+"</p></div><div class='col brandpop-button'>"+$scope.componentResponse[i]['pointsdescription']+"</div> <div class='row coupons-btn-wrapper'><div class='brand-site-button coupons-button' ng-click=openBrandPage('"+$scope.componentResponse[i]['brandUrl']+"')>"+$scope.componentResponse[i]['buttonText']+"</div></div></div>";
                        }
                    }
                    else {
                        if($scope.appExports) {
                            var noImagePath = "../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/no_image1.png";
                            if(serverUrl){
                                $scope.brandCarouselContent =  $scope.brandCarouselContent+"<div><div class='row brand-info-header'><img ng-src='"+serverUrl+$scope.componentResponse[i]['imagePath']+"' onError=this.onerror=null;this.src='"+noImagePath+"'/><span class='ion-close-circled brand-close popupclose' ng-click='closeBrand()'></span></div><div class='brand-info'><p data-dd-collapse-text='150'>"+$scope.componentResponse[i]['brandsdescription']+"</p></div><div class='col brandpop-button'>"+$scope.componentResponse[i]['pointsdescription']+"</div></div>";
                            }
                            else{
                                $scope.brandCarouselContent =  $scope.brandCarouselContent+"<div><div class='row brand-info-header'><img ng-src='https://mobi.payback.in"+$scope.componentResponse[i]['imagePath']+"' onError=this.onerror=null;this.src='"+noImagePath+"'/><span class='ion-close-circled brand-close popupclose' ng-click='closeBrand()'></span></div><div class='brand-info'><p data-dd-collapse-text='150'>"+$scope.componentResponse[i]['brandsdescription']+"</p></div><div class='col brandpop-button'>"+$scope.componentResponse[i]['pointsdescription']+"</div></div>";
                            }
                        }
                        else {
                            var noImagePath = "/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/no_image1.png";
                            $scope.brandCarouselContent =  $scope.brandCarouselContent+"<div><div class='row brand-info-header'><img ng-src='"+$scope.componentResponse[i]['imagePath']+"' onError=this.onerror=null;this.src='"+noImagePath+"'/><span class='ion-close-circled brand-close popupclose' ng-click='closeBrand()'></span></div><div class='brand-info'><p data-dd-collapse-text='150'>"+$scope.componentResponse[i]['brandsdescription']+"</p></div><div class='col brandpop-button'>"+$scope.componentResponse[i]['pointsdescription']+"</div></div>";
                        }
                    }
                }
                $scope.brandCarouselContent = $scope.brandCarouselContent + "</div><div class='row'><ul class='slider-dots-wrapper'><li class='slider-dots'></li><li class='slider-dots'></li><li class='slider-dots'></li></ul></div>";
                $scope.brandCarouselContentt = $('<div/>').html($scope.brandCarouselContent);
                $scope.brandCarouselContent = $filter('rawHtml')($scope.brandCarouselContent)
                $scope.brandCarouselContent =  $compile($scope.brandCarouselContent)($scope);
                $("#brandCarosel").html($scope.brandCarouselContent);
                $("#brandCard").slick({
                    arrows:false
                });
            }
        }

        $scope.serverBrandUrl =  localStorage.getItem("serverUrl");
        $scope.rowSize = $scope.componentResponse.length;
        $scope.rowSize = Math.ceil($scope.rowSize/2);
        var content = "";
        var initialCount  = 0;
        var count = initialCount;
        var limit = 2;
        $scope.rows = [];
        var maxRows = $scope.rowSize;
        var maxCols = 2;
        for( var i =0 ; i < maxRows;i++){
            $scope.rows.push([]);
            count = 	initialCount;
            for( var j =0 ; j < maxCols;j++){
                if ($scope.componentResponse[count]) {
                    $scope.rows[i][j] =  $scope.componentResponse[count];
                    $scope.rows[i][j].count = count;
                    count++;
                }
            }
            initialCount = initialCount + 2;
        }
    }
    catch(constructPartnersError) {
        console.log("Error in constructPartners ::"+ constructPartnersError)
    }
}


$scope.brandPagePath = "/content/payback/home/partner-brands";
console.log("the  brand page path ::"+$scope.brandPagePath);
try {
    if($scope.brandPagePath) {
        if(!localStorage.getCacheItem("partnerBrands")) {
            var serverUrl = localStorage.getItem("serverUrl");
            var Url = "";
            if (serverUrl == null || serverUrl == '') {
                Url = '/payback/anon/utils.html/brands';
            } else {
                Url = serverUrl+'/payback/anon/utils.html/brands';
            }
            var brandsInputData = { 'brandPagePath': $scope.brandPagePath};
            var brandsResponse =  externalService.callService(Url,brandsInputData,"GET");

            brandsResponse.then(function(brandDataResponse) {
                if(brandDataResponse) {
                    localStorage.setCacheItem("partnerBrands", JSON.stringify(brandDataResponse), { hours: 3 });
                    $scope.constructPartners(brandDataResponse);
                }
            });
        }
        else {
            $scope.brandsData = localStorage.getCacheItem("partnerBrands");
            if($scope.brandsData) {
                $scope.constructPartners(JSON.parse($scope.brandsData));
            }
        }
    }
}
catch(brandserr) {
	console.log("brandserr ::"+brandserr);
}

$rootScope.redirectedFromProductCoupons =  false;
var myPopup =null;

$scope.showBrand = function(index) {
    $('.backdrop').addClass('visible active');
    $('#brandCarosel').show();
    $('.slick-dots').addClass("partner-dots");
    $('.slick-dots').css("bottom","0px");
    console.log("brand carousel index"+index);
    $('#brandCard').slick('slickGoTo',parseInt(index),true);
    $("body").scroll(function(e){ e.preventDefault()});
}

$scope.closeBrand = function() {
	$('#brandCarosel').hide();
	$('.backdrop').removeClass('visible active');
    $("body").unbind('scroll');
}

$scope.openBrandPage = function(path) {
    window.open(path,'_system');
}



}])









// Controller for page 'shop-online'
.controller('contentphonegappaybackappspaybackenhomeshoponline', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/shop-online.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'shop-online';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online';
		$rootScope.angularPageTitle = $('<div/>').html('Shop Online').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }
     if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	







$rootScope.redirectProductPath = '';
data.then(function(response){
    $scope.compoName = response.data["content-par/maincarousel"]["compName"];
    $rootScope.redirectProductPath = response.data["content-par/maincarousel"]["redirectProductPath"];

});






    /* popularstores component controller (path: content-par/popularstores) */

$scope.componentTitle = '';
$rootScope.storeType = '';
$scope.viewAllLink = '';
data.then(function(response){

});

    $scope.componentTitle = "{&#034;componentTitle&#034;:&#034;POPULAR STORE&#034;,&#034;storeType&#034;:&#034;Carousel View&#034;,&#034;viewAllLink&#034;:&#034;/content/phonegap/payback/apps/payback/en/home/shop-online/popular-stores&#034;,&#034;popStoresWeakLoginRequired&#034;:&#034;true&#034;}";
    $rootScope.storeType = "Carousel View"
    $scope.viewAllLink = "/content/phonegap/payback/apps/payback/en/home/shop-online/popular-stores";

    if($rootScope.storeType == 'Grid View') {
        $rootScope.headerClass = "row coupons-header row-center bar";
        $scope.contentWrapper = "dynamic-header";

        $scope.headerHtml="<div class='policy-title col'>"+
            "<p>{{angularPageTitle | uppercase}}</p>"+
                         "</div>";
    }
	





    /* gridcarousel component controller (path: content-par/gridcarousel) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/shop-online/jcr:content/content-par/gridcarousel";
    var gridCarouselDynamicPath = "content_par_gridcarousel";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}






    /* hotdealscarousel_0 component controller (path: content-par/hotdealscarousel_0) */

    data.then(function(response) {

    });


    $rootScope.redirectedFromProductCoupons =  false;
    $scope.shopOnline = "shop-online";
    $rootScope.shopOnlineClass = "row coupons-header row-center";
    $scope.shopOnlinePage  =  "true";
    $scope.shopOnlineTitle ="<div class='sl-title col'>"+
                                "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='!appExports'>"+
                                "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='appExports'>"+
                                "<p>{{ angularPageTitle | uppercase}}</p>"+
                           "</div>";










    /* hotdealscarousel component controller (path: content-par/hotdealscarousel) */

    data.then(function(response) {

    });


    $rootScope.redirectedFromProductCoupons =  false;
    $scope.shopOnline = "shop-online";
    $rootScope.shopOnlineClass = "row coupons-header row-center";
    $scope.shopOnlinePage  =  "true";
    $scope.shopOnlineTitle ="<div class='sl-title col'>"+
                                "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='!appExports'>"+
                                "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='appExports'>"+
                                "<p>{{ angularPageTitle | uppercase}}</p>"+
                           "</div>";




	





    /* gridcarousel_0 component controller (path: content-par/gridcarousel_0) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/shop-online/jcr:content/content-par/gridcarousel_0";
    var gridCarouselDynamicPath = "content_par_gridcarousel_0";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}

}])









// Controller for page 'product'
.controller('contentphonegappaybackappspaybackenhomeshoponlineproduct', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/shop-online/product.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'product';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online/product';
		$rootScope.angularPageTitle = $('<div/>').html('Product').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* homesearchbox component controller (path: content-par/homesearchbox) */
		$scope.editMode = 'DISABLED';
		
		$rootScope.redirectedFromProductCoupons =  false;
		$scope.redirectFromSearchBox = function() {
        	$rootScope.searchResultData = '';
        	$scope.go('/content/phonegap/payback/apps/payback/en/home/search');
        }

        $scope.unomerbuttonListener = function(){
            //window.unomerPhoneGapPlugin.unomerFetchSurvey(window.unomerPhoneGapPlugin.callBackFunction);
            $rootScope.weakTokenUnomer = localStorage.getItem('weaklogintoken');
            if($rootScope.weakTokenUnomer){
                window.unomerPhoneGapPlugin.unomerShowSurvey(window.unomerPhoneGapPlugin.callBackFunction);
            }else{
                $rootScope.weakLoginRedirection('/content/phonegap/payback/apps/payback/en/home');
                $scope.timeStampNum = 0;
                $rootScope.unosurveyStatus = true;
                $scope.unoSurvey = setInterval(function(){
                    $scope.timeStampNum += 1;
                    if($scope.timeStampNum > 3 && !$rootScope.unosurveyStatus){
                        clearInterval($scope.unoSurvey);
                    }
                    else{
                        if(localStorage.getItem('weaklogintoken') && $rootScope.unosurveyStatus){
                            $rootScope.unosurveyStatus = false;
                            console.log("Unomer Survey Fetch got started post login");
                            window.unomerPhoneGapPlugin.unomerShowSurvey(window.unomerPhoneGapPlugin.callBackFunction);
                        }
                    }
                }, 7000);
            }
        }





    /* productdetails component controller (path: content-par/productdetails) */

       data.then(function(response) {
		$rootScope.couponsRedirectionPath = response.data["content-par/productdetails"]['couponsRedirectionPath'];
       });
       $scope.headerHtml="";
       $rootScope.redirectedFromProductCoupons =  false;
       
       /* Product Details function */
       $scope.fetchProductDetails = function()
       {
       		try{
       				$scope.showLoading();
		       		$scope.productDetailsServerUrl = localStorage.getItem("serverUrl");
		       		$rootScope.productId = localStorage.getItem("productId");
					$scope.methodType = "GET";
					
					$scope.productDetailsRestUrl = "/payback/anon/shoponline.html/products/"+$rootScope.productId+".json";
					$scope.productDetailsfinalUrl = null;
					$scope.productData = null;
					if($scope.productDetailsServerUrl && $scope.productDetailsServerUrl!="")
					 {
						$scope.productDetailsfinalUrl = $scope.productDetailsServerUrl + $scope.productDetailsRestUrl;
						
					 }else
					 {
						$scope.productDetailsfinalUrl =  $scope.productDetailsRestUrl;
					 }
					var productDetailsResponse =  externalService.callService($scope.productDetailsfinalUrl,"",$scope.methodType);
					productDetailsResponse.then(function(productResponseDetails) {
					$scope.hideLoading();
					if(productResponseDetails["results"])
			 		{
						$scope.productData = productResponseDetails;
						$scope.productTrack = $scope.productData["metaInfo"]["id"] + ' - ' + $scope.productData["metaInfo"]["title"];
						// if(window.ADB)
      //                   {
      //                      ADB.trackAction("viewProduct",{"product": $scope.productTrack ,"cardnumber": localStorage.getItem("PBCardNum")});
      //                   }
						if(productResponseDetails['specsAvailable']&&productResponseDetails['specsAvailable'] == '1')
						{
							$rootScope.specAvail = true;
							$("#specificationTab").show()
						}else
						{
							$rootScope.specAvail = false;
							$("#specificationTab").hide()
						}
						
			 		}else
			 		{
						
						errorhandler.handleError(productResponseDetails);	
			 		}	

			 		});
				
       		
       		
       		}catch(err)
       		{
       			console.log("Expection in fetchProductDetails fucntion product details controller :: "+err);
       		}
       	
       };
       
       /* On load product details call */
       $scope.fetchProductDetails();



	     /* Chceks coupon status */
	   	$scope.couponStatus = function(product) {
		var storeName = product['store'];	
		var couponObj = $scope.productData['couponsinfo']['coupons'][storeName];
		
		if(couponObj)
		{

			return true;
		}else
		{

			return false;
		}



       	};


		 /* Redirect to coupons redirect*/
		 
		 $scope.redirectToCouponsDetails  = function(coupon)
		 {

			var strongToken = localStorage.getItem("weaklogintoken");
			var productId = localStorage.getItem("productId");
			var storeName = coupon['store'];	
			localStorage.setItem("storeName",storeName);
			var couponObj = $scope.productData['couponsinfo']['coupons'][storeName];
			var cardNumber = localStorage.getItem("PBCardNum");
			if(strongToken)
			{
                if(couponObj)
                {

                    $scope.productNameCoupons =  $scope.productData['metaInfo']['title'];
                    $scope.productImageCoupons = $scope.productData['metaInfo']['img'];
                    $scope.productPriceCoupons = $scope.productData['metaInfo']['finalPrice'];
                    $scope.productPartnerShortName = coupon['pbstore_data']['PartnerShortName'];
                    $scope.productPartnerDetails = JSON.stringify(coupon['pbstore_data']);
                    $scope.productCouponsInfo = JSON.stringify($scope.productData['couponsinfo']);
                    $scope.productStoreLogo = coupon['store_logo'];
                    localStorage.setItem("productNameCoupons",$scope.productNameCoupons);
                    localStorage.setItem("productImageCoupons",$scope.productImageCoupons);
                    localStorage.setItem("productPriceCoupons",$scope.productPriceCoupons);
                    localStorage.setItem("productPartnerShortName",$scope.productPartnerShortName);
                    localStorage.setItem("productPartnerDetails",$scope.productPartnerDetails);
                    localStorage.setItem("productCouponsInfo",$scope.productCouponsInfo);
                    localStorage.setItem("productStoreLogo",$scope.productStoreLogo);
                    $scope.go($rootScope.couponsRedirectionPath);

                }else
                {

                   var redirectionurl = coupon['url'];
				   var timeStamp = Date.now();
				   var shoppingTokenValue = "PB_"+cardNumber+"_00000000_"+timeStamp+"_"+storeName+"_"+productId;

					redirectionurl = redirectionurl.replace("TEMPXYZ786", shoppingTokenValue);
					console.log(redirectionurl);	
				  	window.open(redirectionurl,"_system");
                }
			}else
			{	

				if(couponObj)
                {
                    $scope.productNameCoupons =  $scope.productData['metaInfo']['title'];
                    $scope.productImageCoupons = $scope.productData['metaInfo']['img'];
                    $scope.productPriceCoupons = $scope.productData['metaInfo']['finalPrice'];
                    $scope.productPartnerShortName = coupon['pbstore_data']['PartnerShortName'];
                    $scope.productPartnerDetails = JSON.stringify(coupon['pbstore_data']);
                    $scope.productCouponsInfo = JSON.stringify($scope.productData['couponsinfo']);
                    $scope.productStoreLogo = coupon['store_logo'];
                    localStorage.setItem("productNameCoupons",$scope.productNameCoupons);
                    localStorage.setItem("productImageCoupons",$scope.productImageCoupons);
                    localStorage.setItem("productPriceCoupons",$scope.productPriceCoupons);
                    localStorage.setItem("productPartnerShortName",$scope.productPartnerShortName);
                    localStorage.setItem("productPartnerDetails",$scope.productPartnerDetails);
                    localStorage.setItem("productCouponsInfo",$scope.productCouponsInfo);
                    localStorage.setItem("productStoreLogo",$scope.productStoreLogo);
					$rootScope.redirectedFromProductCoupons = true;
 			        $rootScope.weakLoginRedirection($rootScope.couponsRedirectionPath);


                }else
                {
    			  $rootScope.redirectedFromProductCoupons = true;

                  	var 	redirectionurl = coupon['url'];
					$rootScope.couponsRedirectionPath = redirectionurl;
				   $rootScope.weakLoginRedirection($rootScope.couponsRedirectionPath);	
					//window.open(redirectionurl,"_system");
                }


			}
		 };

		/* Gets Product Details  */
		 
		 $scope.getProductDetails = function()
		 {
			try
			{
		 	if($rootScope.specAvail)
		 	 {
		 	 	$scope.showLoading();
			 	$scope.methodType = "GET";
			 	$scope.productSpecificationRestUrl = "/payback/anon/shoponline.html/productspec/"+$rootScope.productId+".json";
				$scope.productSpecificationUrl = null;
				$scope.productSpecificationData = null;
				$scope.productDetailsServerUrl = localStorage.getItem("serverUrl");
				if($scope.productDetailsServerUrl && $scope.productDetailsServerUrl!="")
					{
						$scope.productSpecificationfinalUrl = $scope.productDetailsServerUrl + $scope.productSpecificationRestUrl;
							
					}else
					{
						$scope.productSpecificationfinalUrl =  $scope.productSpecificationRestUrl;
					}
				var productSpecificationResponse =  externalService.callService($scope.productSpecificationfinalUrl,"",$scope.methodType);
				productSpecificationResponse.then(function(productSpecificationResponse) {
				$scope.hideLoading();
				if(productSpecificationResponse["featureAttribute"])
				  {
							$scope.productSpecificationData = productSpecificationResponse["featureAttribute"]; 
							//console.log("No of product specificatoin :: "+$scope.productSpecificationData.length);
							//$scope.constructProductHtml($scope.productSpecificationData);

				 }else
				 {
							
							errorhandler.handleError(productSpecificationResponse);	
			 	 }
			 	 });
			 }
			}catch(error)
			{
				console.log("error in product details :"+error);
			}
		 };
}])









// Controller for page 'coupons-details'
.controller('contentphonegappaybackappspaybackenhomeshoponlinecouponsdetails', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/shop-online/coupons-details.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'coupons-details';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online/coupons-details';
		$rootScope.angularPageTitle = $('<div/>').html('Coupons Details').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* homesearchbox component controller (path: content-par/homesearchbox) */
		$scope.editMode = 'DISABLED';
		
		$rootScope.redirectedFromProductCoupons =  false;
		$scope.redirectFromSearchBox = function() {
        	$rootScope.searchResultData = '';
        	$scope.go('/content/phonegap/payback/apps/payback/en/home/search');
        }

        $scope.unomerbuttonListener = function(){
            //window.unomerPhoneGapPlugin.unomerFetchSurvey(window.unomerPhoneGapPlugin.callBackFunction);
            $rootScope.weakTokenUnomer = localStorage.getItem('weaklogintoken');
            if($rootScope.weakTokenUnomer){
                window.unomerPhoneGapPlugin.unomerShowSurvey(window.unomerPhoneGapPlugin.callBackFunction);
            }else{
                $rootScope.weakLoginRedirection('/content/phonegap/payback/apps/payback/en/home');
                $scope.timeStampNum = 0;
                $rootScope.unosurveyStatus = true;
                $scope.unoSurvey = setInterval(function(){
                    $scope.timeStampNum += 1;
                    if($scope.timeStampNum > 3 && !$rootScope.unosurveyStatus){
                        clearInterval($scope.unoSurvey);
                    }
                    else{
                        if(localStorage.getItem('weaklogintoken') && $rootScope.unosurveyStatus){
                            $rootScope.unosurveyStatus = false;
                            console.log("Unomer Survey Fetch got started post login");
                            window.unomerPhoneGapPlugin.unomerShowSurvey(window.unomerPhoneGapPlugin.callBackFunction);
                        }
                    }
                }, 7000);
            }
        }





    /* productcouponsdetail_0 component controller (path: content-par/productcouponsdetail_0) */

       data.then(function(response) {
      	 $scope.resourcePath  = response.data["content-par/productcouponsdetail_0"]["resourcePath"];
       });

		$rootScope.redirectedFromProductCoupons =  true;
		var referenceDatejs =  $filter('datefilter')( new Date() );
		var serverUrl = localStorage.getItem("serverUrl");
		var partnerShortNameFilter = localStorage.getItem("storeName");
		$scope.partnerShortNameFilter = localStorage.getItem("storeName");
		$scope.partnerShortNameFilter = $scope.partnerShortNameFilter.toLowerCase();
		$scope.constructPaybackCouponsHtml = function(data) {
		if(data['extint.CouponListItem'] && data['extint.CouponListItem'].length>0) {

		var couponsFilter = localStorage.getItem("storeName");
		var couponslength = data['extint.CouponListItem'].length;
		var couponsPoParray = new  Array();
		for(var k=0;k<couponslength;k++)
		{
			if(data["extint.CouponListItem"][k])
			{
                var partnerShortName = data["extint.CouponListItem"][k]["extint.Coupon"]["types.Partner"]["types.PartnerDisplayName"];
                partnerShortName =partnerShortName.toLowerCase();
    
                if(couponsFilter !=  partnerShortName )
                {
					data["extint.CouponListItem"].splice( k, 1 );
					couponsPoParray.push(k);
					if(couponslength!=0)
					{
						couponslength = couponslength-1;
						k=k-1;
					}
                }
			}
		}



		$scope.singleCoupons = false;
		var content = "";
		$scope.rowSize = data["extint.CouponListItem"].length;
		$scope.rowSize = Math.ceil($scope.rowSize/2);
		var initialCount  = 0;
		var count = initialCount;
		var limit = 2;
		$scope.couponPaybackRows = [];
		var maxRows = $scope.rowSize;
		var maxCols = 2;
		for( var i =0 ; i < maxRows;i++){
			$scope.couponPaybackRows.push([]);
			count = initialCount;
			for( var j =0 ; j< maxCols;j++){
				if(data["extint.CouponListItem"][count]) {
					console.log("partner short name"+data["extint.CouponListItem"][count]["extint.Coupon"]["types.Partner"]["types.PartnerDisplayName"]);
					var partnerShortName = data["extint.CouponListItem"][count]["extint.Coupon"]["types.Partner"]["types.PartnerDisplayName"];
					partnerShortName =partnerShortName.toLowerCase();

					$scope.couponPaybackRows[i][j] =  data["extint.CouponListItem"][count];

					if(data["extint.CouponListItem"][count]['buttonText'])
					{

						$scope.couponPaybackRows[i][j].hasButtonText = true;
					}
					else {
						$scope.couponPaybackRows[i][j].hasButtonText = false;
					}
					$scope.couponPaybackRows[i][j].count = count;
					count++;

				}

			}
			initialCount = initialCount + 2;
		}
		} else {
            if(data['extint.CouponListItem'])
            {
                $scope.couponPaybackRows = [];
                $scope.singleCoupons = true;
                $scope.CouponResponse = data["extint.CouponListItem"];
            }
        }
        $scope.hideLoading();
	};


		$scope.constructCouponsHtml = function(data) {
		if(data['secondList'] && data['secondList'].length>0) {
		$scope.singleCoupons = false;
		var content = "";
		$scope.rowSize = data["secondList"].length;
		$scope.rowSize = Math.ceil($scope.rowSize/2);
		var initialCount  = 0;
		var count = initialCount;
		var limit = 2;
		$scope.couponRows = [];
		var maxRows = $scope.rowSize;
		var maxCols = 2;
		for( var i =0 ; i < maxRows;i++){
			$scope.couponRows.push([]);
			count = initialCount;
			for( var j =0 ; j < maxCols;j++){
				if(data["secondList"][count]) {
					$scope.couponRows[i][j] =  data["secondList"][count];

					$scope.couponRows[i][j].count = count;
					count++;
				}

			}
			initialCount = initialCount + 2;
			}
		  } else {
		if(data['secondList'])
		{
			$scope.couponRows = [];
			$scope.singleCoupons = true;
			$scope.CouponResponse = data["secondList"];
			}
		}


	}

	/* Fetch PAYBACK Coupons */

	$rootScope.fetchPaybackCoupons = function()
	{
		try
		{
			$scope.showPaybackCoupons = true;
			//console.log("$scope.productPartnerShortName ="+$scope.productPartnerShortName);
			if (navigator.geolocation) {
				navigator.geolocation.getCurrentPosition(couponssuccess_callback, couponserror_callback, {timeout:10000});
			}

		}catch(exception)
		{
			console.log("error in fetch payback coupons"+exception);
		}




	};

	function couponssuccess_callback(position)
	{

		$scope.showLoading();
		$scope.latitude = position.coords.latitude;
		$scope.longitude = position.coords.longitude;
		$scope.displayPaybackCoupons();
	}
	function couponserror_callback()
	{
		$scope.showLoading();
		$scope.latitude = "0";
		$scope.longitude = "0";
		$scope.displayPaybackCoupons();

	}

	/* Fetch PAYBACK Coupons IMPL */

	$scope.displayPaybackCoupons = function()
	{
		var couponToken = null;
   		var couponStrongToken = $scope.getloginToken();
		var couponweakToken = $scope.getweakloginToken();

		if(couponStrongToken!=null)
		{
			couponToken = couponStrongToken;
		}else
		{
			couponToken = couponweakToken;

		}
		if(couponToken!=null) {
		var finalUrl = serverUrl;
		if (finalUrl == null || finalUrl == '') {
			finalUrl = '/payback/wksecure/offers.html/coupon/list';
		} else {
			finalUrl = serverUrl + '/payback/wksecure/offers.html/coupon/list';
		}
			var showAllCouponsRequestData={'token' : couponToken,'latitude' : $scope.latitude,'longitude' : $scope.longitude, 'distributionChannel' : '5', 'referenceDate' : referenceDatejs, 'periodQuery' : '4', 'pagePath' : $scope.resourcePath };
			var showAllCouponsResponse =  externalService.callService(finalUrl,showAllCouponsRequestData,'POST');
			showAllCouponsResponse.then(function(showAllCouponsResponseData) {
				$scope.hideLoading();

				if(showAllCouponsResponseData['types.FaultMessage'])
				{
					$scope.hideLoading();
					var faultMsg = showAllCouponsResponseData['types.FaultMessage']['types.Message'];
					if(faultMsg == "Invalid token") {
						$rootScope.errorMsg = "Token seems invalid, please check again.";
						localStorage.removeItem('stronglogintoken');
						localStorage.removeItem('weaklogintoken');
						localStorage.removeItem('userPoint');
						$rootScope.showError();
						$scope.go("/content/phonegap/payback/apps/payback/en/home");
					}
					else {
						$rootScope.errorMsg = faultMsg;
						$scope.paybackCouponError = true;
						$scope.fetchPartnerCoupons();
						$scope.hideLoading();


					}
				}
				else {
					if(showAllCouponsResponseData['error'])
					{   $scope.hideLoading();
						$rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
						$rootScope.showError();
						$scope.go("/content/phonegap/payback/apps/payback/en/home");
					}
					else if (showAllCouponsResponseData['message']) {
						$scope.hideLoading();
						$scope.paybackCouponError = true;

					}
					else
					{
						$scope.paybackCouponError = false;
						$scope.hideLoading();
						$scope.constructPaybackCouponsHtml(showAllCouponsResponseData);

					}
				}
			});
		}


	};

$scope.shopNow = function(item)
{
	var redirectionurl = "";
	if(item['extint.Coupon']['DisplayTexts']['onlineShopUrl']!=undefined)
	{
		// if(window.ADB)
  //       	{
  //              	ADB.trackAction("partnerRedirection");
  //       	}

		redirectionurl = item['extint.Coupon']['DisplayTexts']['onlineShopUrl'];
		//$window.location.href = redirectionurl;
		window.open(redirectionurl,"_system");
	}else
	{
		$scope.go("/content/phonegap/payback/apps/payback/en/home/store-locator");
		$rootScope.couponsPartnerShortName = item['extint.Coupon']['types.Partner']['types.PartnerShortName'];
		$rootScope.couponsCall = true;
	}
}

$scope.activateCoupons = function(couponid) {
    var couponToken = null;
    var couponStrongToken = $scope.getloginToken();
    var couponweakToken = $scope.getweakloginToken();

    if(couponStrongToken!=null)
    {
        couponToken = couponStrongToken;
    }else
    {
        couponToken = couponweakToken;

    }
	var couponId = couponid["extint.Coupon"]["types.CouponID"];
	var finalUrl = serverUrl;
	if (finalUrl == null || finalUrl == '') {
		finalUrl = '/payback/wksecure/offers.html/coupon/activate';
	} else {
		finalUrl = serverUrl + '/payback/wksecure/offers.html/coupon/activate';
	}
	$scope.showLoading();
	var activateCouponsData={'token':couponToken, 'couponId':couponId};
	var activateCouponsResponse =  externalService.callService(finalUrl,activateCouponsData,"POST");
	activateCouponsResponse.then(function(activateCouponsResponseData){
		$scope.hideLoading();
		if(activateCouponsResponseData['error'])
		{
			$scope.hideLoading();
			$rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
			$rootScope.showError();
			$scope.go("/content/phonegap/payback/apps/payback/en/home");
		}
		else if(activateCouponsResponseData['types.FaultMessage']) {
			$scope.hideLoading();
			var faultMsg = activateCouponsResponseData['types.FaultMessage']['types.Message'];
			if(faultMsg == "Invalid token") {
				$rootScope.errorMsg = "Token seems invalid, please check again.";
				$rootScope.showError();
				$scope.go("/content/phonegap/payback/apps/payback/en/home");
			}
			else if(faultMsg == "Authentication level not sufficient for this operation") {
                $rootScope.errorMsg = "We need a few more details to verify your login.";
                $rootScope.showError();
            }
			else {
				$rootScope.errorMsg = faultMsg;
				$rootScope.showError();
				$scope.go("/content/phonegap/payback/apps/payback/en/home");
			}
		}
		else {


				   // if(window.ADB)
       //  			{
       //         				 ADB.trackAction("couponActivation");
       //  			}
        	localStorage.removeItem('allCouponsResponse');
            localStorage.removeItem('nonActivatedCouponsResponse');
            localStorage.removeItem('activatedCouponsResponse');
			$rootScope.successMsg = "Coupon successfully activated!";
			couponid['extint.Coupon']['types.CouponStatus'] = "2";
			$scope.showCouponSuccess();
			$scope.closePop();
			$scope.displayPaybackCoupons();
		}
	});
}
var successPopUp = null;
$scope.showCouponSuccess = function() {
    successPopUp = $ionicPopup.show({
        templateUrl : 'successPopUp',
        scope: $scope,
        onTap: function(e) {
           e.preventDefault();
        }
    });
    successPopUp.then(function(res) {

    });
    $timeout(function() {
     successPopUp.close(); //close the popup after 3 seconds for some reason
  }, 3000);
};
$scope.successClosePopup = function(){
    successPopUp.close();
};

	/*Fetch partnerCoupons */

	$scope.fetchPartnerCoupons = function()
	{
			try{
					$scope.showLoading();
					$scope.showPaybackCoupons = false;
					$rootScope.redirectFromProductCoupons = true;
					//console.log("$scope.productCouponsInfo"+$scope.productCouponsInfo['coupons']);
							var couponToken = null;
   					var couponStrongToken = $scope.getloginToken();
					var couponweakToken = $scope.getweakloginToken();

                    if(couponStrongToken!=null)
                    {
                    	couponToken = couponStrongToken;
                    }else
                    {
                    	couponToken = couponweakToken;

                    }

					for (var key in $scope.productCouponsInfo['coupons']) {
		       		if ($scope.productCouponsInfo['coupons'].hasOwnProperty(key)) {
							//console.log($scope.productCouponsInfo['coupons'][key].length);
							if( $scope.productCouponsInfo['coupons'][key] && $scope.productCouponsInfo['coupons'][key].length >0 )
							 {
								for(var i=0;i<$scope.productCouponsInfo['coupons'][key].length;i++)
								{
									//console.log("$scope.productCouponsInfo['coupons'][key]"+$scope.productCouponsInfo['coupons'][key][i]['storeName']);
									var storeCouponImage = $scope.productCouponsInfo['coupons'][key][i]['storeName'];
									var storeCouponTile =  $scope.productCouponsInfo['coupons'][key][i]['couponTitle'];
									var storeCouponCode = $scope.productCouponsInfo['coupons'][key][i]['couponCode'];
									var storeCouponExpiry = $scope.productCouponsInfo['coupons'][key][i]['expiredAt'];
									storeCouponExpiry = new Date(storeCouponExpiry);
 									storeCouponExpiry = $filter('date')(storeCouponExpiry, 'dd-MMM-yyyy');
									var landingUrl = $scope.productCouponsInfo['coupons'][key][i]['landing_url'];

									if(couponToken == null)
									{
                                            $scope.showPaybackCoupons = false;
                                            if(i==0)
                                             {
                                               $scope.singleCouponData = '{ "singleCoupon": { "storeImgUrl": "'+storeCouponImage+'", "storeCouponTitle": "'+storeCouponTile+'", "storeCouponscode": "'+storeCouponCode+'", "storeCouponExpiry": "'+storeCouponExpiry+'","landingUrl" : "'+landingUrl+'" } }';
                                               //console.log("$scope.singleCouponData"+$scope.singleCouponData)
                                               $scope.singleCouponData = JSON.parse($scope.singleCouponData);
                                               console.log("$scope.singleCouponData"+ $scope.singleCouponData);
                                             }else
                                             {
                                                if(i==1)
                                                    {
                                                         $scope.secondList = '{ "secondList": [{ "storeImgUrl": "'+storeCouponImage+'", "storeCouponTitle": "'+storeCouponTile+'", "storeCouponscode": "'+storeCouponCode+'", "storeCouponExpiry": "'+storeCouponExpiry+'","landingUrl" : "'+landingUrl+'" } ';
                                                    }else
                                                    {
                                                         $scope.secondList = $scope.secondList  + ',{ "storeImgUrl": "'+storeCouponImage+'", "storeCouponTitle": "'+storeCouponTile+'", "storeCouponscode": "'+storeCouponCode+'", "storeCouponExpiry": "'+storeCouponExpiry+'","landingUrl" : "'+landingUrl+'"  } ';
                                                    }
                                             }

									}else
									{
										$scope.showPaybackCoupons = true;
										if(i==0)
									 		{
									 			 $scope.secondList = '{ "secondList": [{ "storeImgUrl": "'+storeCouponImage+'", "storeCouponTitle": "'+storeCouponTile+'", "storeCouponscode": "'+storeCouponCode+'", "storeCouponExpiry": "'+storeCouponExpiry+'","landingUrl" : "'+landingUrl+'" } ';
									 		}else
									 		{
												 $scope.secondList = $scope.secondList  + ',{ "storeImgUrl": "'+storeCouponImage+'", "storeCouponTitle": "'+storeCouponTile+'", "storeCouponscode": "'+storeCouponCode+'", "storeCouponExpiry": "'+storeCouponExpiry+'","landingUrl" : "'+landingUrl+'"  } ';
									 		}
									}
								}
								$scope.secondList = $scope.secondList + "]}";
								console.log("the second list is :: "+ $scope.secondList);
								$scope.secondList = JSON.parse($scope.secondList);
								$scope.constructCouponsHtml($scope.secondList)
							 }

		       			}

   			 }
			$rootScope.fetchPaybackCoupons();
				$scope.hideLoading();

   			}catch(error)
   			{
   				console.log("error ::"+error);
				$scope.hideLoading();
   			}



	};

	$scope.partnerDisplayName = "";

	/* Picking product details */
	try{

  		$scope.productNameCoupons =  localStorage.getItem("productNameCoupons");
		$scope.productImageCoupons = localStorage.getItem("productImageCoupons");
		$scope.productPriceCoupons = localStorage.getItem("productPriceCoupons");
		$scope.productPartnerShortName = localStorage.getItem("productPartnerShortName");
		$scope.productPartnerDetails = localStorage.getItem("productPartnerDetails");
		$scope.productCouponsInfo = JSON.parse(localStorage.getItem("productCouponsInfo"));
		$scope.productStoreLogo = localStorage.getItem("productStoreLogo");
		//$scope.partnerDisplayName =productPartnerDetails["PartnerDisplayName"];
		$scope.couponweakToken = $scope.getweakloginToken();

		if($scope.couponweakToken)
		{
			//$rootScope.fetchPaybackCoupons();
			$scope.fetchPartnerCoupons();

		}else
		{
			$scope.fetchPartnerCoupons();
		}
		}catch(err)
		{
			console.log("err"+err);
		}
		/* Picking product details */


		$scope.copyCode = function(redirectionPath,couponCode)
		{
try{
if(couponCode)
		  {
		  	console.log("item['couponCode']::"+couponCode);

			if(cordova)
			{
				if(cordova.plugins.clipboard)
				{
					cordova.plugins.clipboard.copy(couponCode);
					var strongToken = localStorage.getItem("stronglogintoken");
		  			var productId = localStorage.getItem("productId");
		 			 var storeName = $scope.partnerDisplayName;
		  			var cardNumber = localStorage.getItem("PBCardNum");
		  				var timeStamp = Date.now();
                      var shoppingTokenValue = "PBA_oooooo"+cardNumber+"_"+timeStamp+"_"+storeName+"_"+productId;
                      redirectionPath = redirectionPath.replace("TEMPXYZ786", shoppingTokenValue);
                      window.open(redirectionPath,"_system");
				}

			}



		  }

}catch(copyCodeerr)
{
var strongToken = localStorage.getItem("stronglogintoken");
		  var productId = localStorage.getItem("productId");
		  var storeName = $scope.partnerDisplayName;
		  var cardNumber = localStorage.getItem("PBCardNum");
		  var timeStamp = Date.now();
		  var shoppingTokenValue = "PBA_oooooo"+cardNumber+"_"+timeStamp+"_"+storeName+"_"+productId;
		  redirectionPath = redirectionPath.replace("TEMPXYZ786", shoppingTokenValue);
		  window.open(redirectionPath,"_system");
console.log("copyCodeerr ::"+copyCodeerr);
}

		};




}])









// Controller for page 'hot-deals'
.controller('contentphonegappaybackappspaybackenhomeshoponlinehotdeals', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/shop-online/hot-deals.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'hot-deals';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online/hot-deals';
		$rootScope.angularPageTitle = $('<div/>').html('Hot Deals').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* hotdealscarousel component controller (path: content-par/hotdealscarousel) */

    data.then(function(response) {

    });


    $rootScope.redirectedFromProductCoupons =  false;
    $scope.shopOnline = "shop-online";
    $rootScope.shopOnlineClass = "row coupons-header row-center";
    $scope.shopOnlinePage  =  "true";
    $scope.shopOnlineTitle ="<div class='sl-title col'>"+
                                "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='!appExports'>"+
                                "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='appExports'>"+
                                "<p>{{ angularPageTitle | uppercase}}</p>"+
                           "</div>";





}])









// Controller for page 'top-deals'
.controller('contentphonegappaybackappspaybackenhomeshoponlinetopdeals', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/shop-online/top-deals.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'top-deals';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online/top-deals';
		$rootScope.angularPageTitle = $('<div/>').html('Top Deals').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* hotdealscarousel component controller (path: content-par/hotdealscarousel) */

    data.then(function(response) {

    });


    $rootScope.redirectedFromProductCoupons =  false;
    $scope.shopOnline = "shop-online";
    $rootScope.shopOnlineClass = "row coupons-header row-center";
    $scope.shopOnlinePage  =  "true";
    $scope.shopOnlineTitle ="<div class='sl-title col'>"+
                                "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='!appExports'>"+
                                "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='appExports'>"+
                                "<p>{{ angularPageTitle | uppercase}}</p>"+
                           "</div>";





}])









// Controller for page 'featured-products'
.controller('contentphonegappaybackappspaybackenhomeshoponlinefeaturedproducts', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/shop-online/featured-products.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'featured-products';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online/featured-products';
		$rootScope.angularPageTitle = $('<div/>').html('Featured Products').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* hotdealscarousel component controller (path: content-par/hotdealscarousel) */

    data.then(function(response) {

    });


    $rootScope.redirectedFromProductCoupons =  false;
    $scope.shopOnline = "shop-online";
    $rootScope.shopOnlineClass = "row coupons-header row-center";
    $scope.shopOnlinePage  =  "true";
    $scope.shopOnlineTitle ="<div class='sl-title col'>"+
                                "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='!appExports'>"+
                                "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='appExports'>"+
                                "<p>{{ angularPageTitle | uppercase}}</p>"+
                           "</div>";





}])









// Controller for page 'featured-books'
.controller('contentphonegappaybackappspaybackenhomeshoponlinefeaturedbooks', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/shop-online/featured-books.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'featured-books';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online/featured-books';
		$rootScope.angularPageTitle = $('<div/>').html('Featured Books').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* hotdealscarousel component controller (path: content-par/hotdealscarousel) */

    data.then(function(response) {

    });


    $rootScope.redirectedFromProductCoupons =  false;
    $scope.shopOnline = "shop-online";
    $rootScope.shopOnlineClass = "row coupons-header row-center";
    $scope.shopOnlinePage  =  "true";
    $scope.shopOnlineTitle ="<div class='sl-title col'>"+
                                "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='!appExports'>"+
                                "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='appExports'>"+
                                "<p>{{ angularPageTitle | uppercase}}</p>"+
                           "</div>";





}])









// Controller for page 'popular-television'
.controller('contentphonegappaybackappspaybackenhomeshoponlinepopulartelevision', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/shop-online/popular-television.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'popular-television';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online/popular-television';
		$rootScope.angularPageTitle = $('<div/>').html('Popular Television').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* hotdealscarousel component controller (path: content-par/hotdealscarousel) */

    data.then(function(response) {

    });


    $rootScope.redirectedFromProductCoupons =  false;
    $scope.shopOnline = "shop-online";
    $rootScope.shopOnlineClass = "row coupons-header row-center";
    $scope.shopOnlinePage  =  "true";
    $scope.shopOnlineTitle ="<div class='sl-title col'>"+
                                "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='!appExports'>"+
                                "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='appExports'>"+
                                "<p>{{ angularPageTitle | uppercase}}</p>"+
                           "</div>";





}])









// Controller for page 'popular-cameras'
.controller('contentphonegappaybackappspaybackenhomeshoponlinepopularcameras', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/shop-online/popular-cameras.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'popular-cameras';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online/popular-cameras';
		$rootScope.angularPageTitle = $('<div/>').html('Popular Cameras').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* hotdealscarousel component controller (path: content-par/hotdealscarousel) */

    data.then(function(response) {

    });


    $rootScope.redirectedFromProductCoupons =  false;
    $scope.shopOnline = "shop-online";
    $rootScope.shopOnlineClass = "row coupons-header row-center";
    $scope.shopOnlinePage  =  "true";
    $scope.shopOnlineTitle ="<div class='sl-title col'>"+
                                "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='!appExports'>"+
                                "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/shoponline_icon1.png' alt='store' id='brand-logo' ng-if='appExports'>"+
                                "<p>{{ angularPageTitle | uppercase}}</p>"+
                           "</div>";





}])









// Controller for page 'popular-stores'
.controller('contentphonegappaybackappspaybackenhomeshoponlinepopularstores', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/shop-online/popular-stores.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'popular-stores';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online/popular-stores';
		$rootScope.angularPageTitle = $('<div/>').html('Popular Stores').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/shop-online';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* popularstores component controller (path: content-par/popularstores) */

$scope.componentTitle = '';
$rootScope.storeType = '';
$scope.viewAllLink = '';
data.then(function(response){

});

    $scope.componentTitle = "{&#034;componentTitle&#034;:&#034;&#034;,&#034;storeType&#034;:&#034;Grid View&#034;,&#034;viewAllLink&#034;:&#034;&#034;,&#034;popStoresWeakLoginRequired&#034;:&#034;true&#034;}";
    $rootScope.storeType = "Grid View"
    $scope.viewAllLink = "";

    if($rootScope.storeType == 'Grid View') {
        $rootScope.headerClass = "row coupons-header row-center bar";
        $scope.contentWrapper = "dynamic-header";

        $scope.headerHtml="<div class='policy-title col'>"+
            "<p>{{angularPageTitle | uppercase}}</p>"+
                         "</div>";
    }

}])









// Controller for page 'search'
.controller('contentphonegappaybackappspaybackenhomesearch', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/search.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'search';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/search';
		$rootScope.angularPageTitle = $('<div/>').html('Search').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* search component controller (path: content-par/search) */
		$scope.editMode = 'DISABLED';


    data.then(function(response) {
		$rootScope.productRedirectionPath = response.data['content-par/search']['productRedirection'];

    });

$scope.headerHtml = "";
localStorage.removeItem("filterParam");
$scope.noResults = false;
$rootScope.redirectedFromProductCoupons =  false;

$ionicModal.fromTemplateUrl('my-modal.html', {
        scope: $scope
        , animation: 'slide-in-up'
    })
    .then(function (modal) {
        $scope.modal = modal;
    });
$scope.openModal = function () {
    $scope.modal.show();
};
$scope.closeModal = function () {
    $scope.modal.hide();
};
$scope.$on('$destroy', function () {
    $scope.modal.remove();
});
$scope.$on('modal.hidden', function () {});
$scope.$on('modal.removed', function () {});



$ionicPopover.fromTemplateUrl('filter-pop', {
    scope: $scope,
  }).then(function(popoverfilter) {
    $scope.popoverfilter = popoverfilter;
  });
  $scope.openPopoverfilter = function($event) {
    $scope.popoverfilter.show($event);
  };
  $scope.closePopoverfilter = function() {
    $scope.popoverfilter.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popoverfilter.remove();
  });
  // Execute action on hide popover
  $scope.$on('popoverfilter.hidden', function() {
    // Execute action
  });
  // Execute action on remove popover
  $scope.$on('popoverfilter.removed', function() {
  });

 $ionicPopover.fromTemplateUrl('product-filter', {
    scope: $scope,
  }).then(function(popoverfilterProduct) {
    $scope.popoverfilterProduct = popoverfilterProduct;
  });
  $scope.openPopoverfilterProduct = function($event) {
    $scope.popoverfilterProduct.show($event);
  };
  $scope.closePopoverProduct = function() {
    $scope.popoverfilterProduct.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.popoverfilterProduct.remove();
  });
  // Execute action on hide popover
  $scope.$on('popoverfilterProduct.hidden', function() {
    // Execute action
  });
  // Execute action on remove popover
  $scope.$on('popoverfilterProduct.removed', function() {
  });






$scope.searchServerUrl = localStorage.getItem("serverUrl");
if ($scope.searchServerUrl == null || $scope.searchServerUrl == '') {
    $scope.searchServerUrl = '';
}

$scope.getRecentSearches = function(){
    if(localStorage.getItem("recentSearches")) {
        var searchArray = [];
        var resultArray = [];
        searchArray = localStorage.getItem("recentSearches").split(",");
        if((searchArray.length) <= 10){
            $scope.recentSearches = searchArray;
        }
        else{
            for(var num=0;num<=10;num++){
                resultArray[num] = searchArray[num];
            }
            $scope.recentSearches = resultArray;
        }
    }
    else{
        $scope.recentSearches = null;
    }
}


$scope.clearRecentSearches = function() {
    if(localStorage.getItem("recentSearches")) {
        localStorage.removeItem("recentSearches");
        $scope.getRecentSearches();
    }
    else{
        $rootScope.errorMsg = "Recent Searches already Cleared";
        $rootScope.showError();
    }
    $scope.closeModal();
}

$scope.getRecentSearches();


$scope.populateInSearch = function(recentSearch){
    document.getElementById("searchBox_value").value = recentSearch;
    $rootScope.productSearch();
    $scope.closeModal();
}


$rootScope.productSearch = function(speakText) {
    if(window.cordova && cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.close();
    }
    var serverUrl = localStorage.getItem("serverUrl");
    if(speakText){
        var searchText = speakText;
        $('#searchBox_value').val(searchText);
    }
    else{
        var searchText = document.getElementById("searchBox_value").value;
    }
    if(searchText) {
        if($scope.editMode == 'DISABLED') {
            $scope.showLoading();
        }
        if (serverUrl == null || serverUrl == '') {
            serverUrl = '/payback/anon/shoponline.html/search';
        } else {
            serverUrl = serverUrl+'/payback/anon/shoponline.html/search';
        }


         var uniqueSearches = new Array();
         var searchedString = searchText.trim();
         var recentSearches = localStorage.getItem("recentSearches");
         if(recentSearches) {
            recentSearches = recentSearches.split(",");

            for(var i=0;i<recentSearches.length;i++){
                if(searchedString.toUpperCase() != recentSearches[i].toUpperCase()){
                    uniqueSearches.push(recentSearches[i]);
                }
            }
            if(uniqueSearches.indexOf(searchedString) < 0 ){
                uniqueSearches.unshift(searchedString);
            }
            else{
                var index = uniqueSearches.indexOf(searchedString);
                uniqueSearches.splice(index, 1);
                uniqueSearches.unshift(searchedString);
            }
         }
         else{
            uniqueSearches.push(searchedString);
         }
         localStorage.setItem("recentSearches",uniqueSearches);



        $scope.getRecentSearches();




        var searchInputData={'searchTerm':searchText};
		$scope.getSearchResult(searchInputData);

    }
}
$scope.filterCategory = function(categoryToFilter) {
    try {

        var filterParams = {
            "params": [{
                "key": "category",
                "value": categoryToFilter
            }]
        };

		localStorage.setItem("selectedCategory",categoryToFilter);

        if (window.cordova && cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.close();
        }

        var searchInputData = {
            'searchTerm': $rootScope.querySuggestions,
            'filterParams': filterParams
        };

		$scope.getSearchResult(searchInputData);

    } catch (filterCategoryExceptionErr) {
        console.log("filterCategoryExceptionErr ::" + filterCategoryExceptionErr);
    }

};

$scope.stateChanged = function (filterValue,filterKey,filterId) {
	var filterCheck = $('#'+filterId).is(':checked');
	var inputFilterObj = null;
	var categorySelected  =  localStorage.getItem("selectedCategory");

	if(categorySelected==null)
	{
		categorySelected = " ";
	}


   if(filterValue && filterCheck){


	if(categorySelected)
		{
			var filtersApplied = localStorage.getItem("filterParam");
			if(filtersApplied)
			{
				var filterAppliedJson = JSON.parse(filtersApplied);
				if(filterAppliedJson)
					{

							filterKey = filterKey.replace(/ /g,"_");
							filterAppliedJson["filterParam"].push({"key":filterKey,"value":filterValue});
							var filterAppliedJsonString = JSON.stringify(filterAppliedJson);
							localStorage.setItem("filterParam",filterAppliedJsonString);

					}

			}else
			{
				filterKey = filterKey.replace(/ /g,"_");
				var filterJson = {"filterParam":[{"key":filterKey,"value":filterValue}]};
				var filterJsonString = JSON.stringify(filterJson);
				localStorage.setItem("filterParam",filterJsonString);


			}

		}


   }else
	{
		var filtersApplied = localStorage.getItem("filterParam");
		if(filtersApplied)
			{
				var filterAppliedJson = JSON.parse(filtersApplied);
				for(var i=0;i<filterAppliedJson["filterParam"].length;i++)
					{
						var filterArrayValue  = filterAppliedJson["filterParam"][i]['value'];
						if(filterArrayValue == filterValue)
							{
								filterAppliedJson["filterParam"].splice(i, 1);
							}
					}
				var filterModifiedJson = JSON.stringify(filterAppliedJson);
				localStorage.setItem("filterParam",filterModifiedJson);

			}
	}

	var finalFilterObject = localStorage.getItem("filterParam");

	if(finalFilterObject)
	{
		var parsedFinalJson = JSON.parse(finalFilterObject);
		inputFilterObj = {
            "params": [{
                "key": "category",
                "value": categorySelected
            }]
        };

		for(var j=0;j<parsedFinalJson["filterParam"].length;j++)
			{
				inputFilterObj["params"].push(parsedFinalJson["filterParam"][j]);

			}

		 var searchInputData = {
            'searchTerm': $rootScope.querySuggestions,
            'filterParams': inputFilterObj
        };

		$scope.getSearchResult(searchInputData);

	}


};

$scope.getSearchResult = function(searchInputData)
{
try{

var serverUrl = localStorage.getItem("serverUrl");

        if ($scope.editMode == 'DISABLED') {
            $scope.showLoading();
        }
        if (serverUrl == null || serverUrl == '') {
            serverUrl = '/payback/anon/shoponline.html/search';
        } else {
            serverUrl = serverUrl + '/payback/anon/shoponline.html/search';
        }

var searchResponse = externalService.callService(serverUrl, searchInputData, "GET");
        searchResponse.then(function(searchResponseData) {
			$scope.closePopoverfilter();
			$scope.closePopoverProduct();
            $scope.hideLoading();
            $scope.errorMsg = searchResponseData["error"];
            $scope.faultMsg = searchResponseData["types.FaultMessage"];
            if ($scope.errorMsg) {
                $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
                $rootScope.showError();
            } else if ($scope.faultMsg) {
                $rootScope.errorMsg = $scope.faultMsg;
                $rootScope.showError();
            } else {
                $rootScope.searchResultData = searchResponseData['search'];
                $rootScope.categoryData = searchResponseData['categories'];
                $rootScope.filterData = searchResponseData['filters'];
                $rootScope.querySuggestions = searchResponseData['querySuggestions'];

                if ($rootScope.searchResultData == "No result found.") {
                    $scope.noResults = true;
                } else {
                    $scope.noResults = false;
					$scope.constructSearchHtml($rootScope.searchResultData);
                }

                if (Object.keys($rootScope.categoryData).length>0) {
                    $scope.noCategory = true;
                } else {
                    $scope.noCategory = false;

                }


                if ($rootScope.filterData.length >0) {
                    $scope.noFilter = true;
                } else {
                    $scope.noFilter = false;

                }

            }
        });



}catch(getSearchResultErr)
{
console.log("getSearchResultErr ::"+getSearchResultErr);
}
};

$scope.constructSearchHtml = function(data)
{
try{

$scope.searchResultCount = 0;

for(var k=0;k<data.length;k++)
{

	if(data[k]['finalPrice']!=0&& data[k]['origPrice']!=0)
		{
			$scope.searchResultCount = $scope.searchResultCount+1;
		}

}



var content = "";
        $scope.rowSize = $scope.searchResultCount;
        $scope.rowSize = Math.ceil($scope.rowSize/2);
        var initialCount  = 0;
        var count = initialCount;
        var limit = 2;
        $scope.couponRows = [];
        var maxRows = $scope.rowSize;
        var maxCols = 2;
        for( var i =0 ; i < maxRows;i++){
            $scope.couponRows.push([]);
            count = initialCount;
            for( var j =0 ; j < maxCols;j++){

                    $scope.couponRows[i][j] =  data[count];

                    $scope.couponRows[i][j].count = count;
                    count++;


            }
            initialCount = initialCount + 2;


        }



}catch(constructSearchHtmlErr)
{
console.log("constructSearchHtmlErr ::"+constructSearchHtmlErr);
}
};

$scope.clearSearchText = function() {
    document.getElementById("searchBox_value").value = '';
}

$scope.recognizeSpeech= function() {

    if(device) {
        if(device.platform == 'Android') {
            var maxMatches = 5;
            var promptString = "Speak now"; // optional
            var language = "en-US";                     // optional
            window.plugins.speechrecognizer.startRecognize(function(result){
                $rootScope.productSearch(result[0]);
            }, function(errorMessage){
                console.log("Error message: " + errorMessage);
            }, maxMatches, promptString, language);
        }
        else {
         $rootScope.iosSpeech();
        }
    }
}

}])









// Controller for page 'strong-login'
.controller('contentphonegappaybackappspaybackenhomestronglogin', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/strong-login.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'strong-login';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/strong-login';
		$rootScope.angularPageTitle = $('<div/>').html('Strong Login').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* login component controller (path: content-par/login) */
		$scope.editMode = 'DISABLED';

    data.then(function(response) {
        $scope.loginResponse = response.data["content-par/login"];
        $rootScope.forgotPinSuccessHtml = $scope.loginResponse['forgotPinSuccessMsg'];
        $rootScope.removeSignUp = $scope.loginResponse['removeSignUp'];
        $rootScope.removeLogin = $scope.loginResponse['removeLogin'];
    })

$scope.contentWrapper = "dynamic-header";
$scope.headerHtml = "";
$rootScope.cardNumber = localStorage.getItem("cardNumber");


$scope.getNumber = function() {
	$rootScope.forgotNumber=$("#strongcardNumber").val();
}
}])









// Controller for page 'weak-login'
.controller('contentphonegappaybackappspaybackenhomeweaklogin', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/weak-login.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'weak-login';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/weak-login';
		$rootScope.angularPageTitle = $('<div/>').html('Weak Login').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* login component controller (path: content-par/login) */
		$scope.editMode = 'DISABLED';

    data.then(function(response) {
        $scope.loginResponse = response.data["content-par/login"];
        $rootScope.forgotPinSuccessHtml = $scope.loginResponse['forgotPinSuccessMsg'];
        $rootScope.removeSignUp = $scope.loginResponse['removeSignUp'];
        $rootScope.removeLogin = $scope.loginResponse['removeLogin'];
    })

$scope.contentWrapper = "dynamic-header";
$scope.headerHtml = "";
$rootScope.cardNumber = localStorage.getItem("cardNumber");


$scope.getNumber = function() {
	$rootScope.forgotNumber=$("#strongcardNumber").val();
}
}])









// Controller for page 'forgot-pin'
.controller('contentphonegappaybackappspaybackenhomeforgotpin', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/forgot-pin.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'forgot-pin';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/forgot-pin';
		$rootScope.angularPageTitle = $('<div/>').html('Forgot PIN').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* login component controller (path: content-par/login) */
		$scope.editMode = 'DISABLED';

    data.then(function(response) {
        $scope.loginResponse = response.data["content-par/login"];
        $rootScope.forgotPinSuccessHtml = $scope.loginResponse['forgotPinSuccessMsg'];
        $rootScope.removeSignUp = $scope.loginResponse['removeSignUp'];
        $rootScope.removeLogin = $scope.loginResponse['removeLogin'];
    })

$scope.contentWrapper = "dynamic-header";
$scope.headerHtml = "";
$rootScope.cardNumber = localStorage.getItem("cardNumber");


$scope.getNumber = function() {
	$rootScope.forgotNumber=$("#strongcardNumber").val();
}
}])









// Controller for page 'my-balance'
.controller('contentphonegappaybackappspaybackenhomemybalance', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/my-balance.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'my-balance';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/my-balance';
		$rootScope.angularPageTitle = $('<div/>').html('My Balance').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* userblock component controller (path: content-par/userblock) */
		$scope.editMode = 'DISABLED';
		$scope.isMember = false;
		$scope.isBalance = false;
		$rootScope.redirectedFromProductCoupons =  false;
        var strongToken = $scope.getloginToken();
        
         var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
		$rootScope.userblockExports = false;
            if ( app ) {
            	
            	$rootScope.userblockExports = true;
            	
            }else
            	{
            		$rootScope.userblockExports = false;
            	}
        

     $scope.options = [
                { label: 'Mr', value: 1 },
                { label: 'Mrs', value: 2 },
                 { label: 'Ms', value: 3 }
              ];


if($rootScope.isDummyUser)
{
        $("#enroll").css("display","block");
}else
{
    $("#getMemberBlock").css("display","block");
}

if($scope.editMode == 'DISABLED')
{
    $scope.showLoading();

}

$scope.userBlockServices = function()
{
	if(strongToken == null)
	{
		if($scope.editMode == 'DISABLED')
		{
			$rootScope.strongLoginRedirection("/content/phonegap/payback/apps/payback/en/home/my-balance");
		}
	}
	else {
		var serverUrl = localStorage.getItem("serverUrl");
		var Url = "";
		if (serverUrl == null || serverUrl == '') {
			Url = '/payback/secure/userops.html/account/balance';
		} else {
			Url = serverUrl+'/payback/secure/userops.html/account/balance';
		}
		if ($scope.isBalance==false) {
		    if($scope.editMode == 'DISABLED')
            {
                $scope.showLoading();
            }
			/* service start*/
			var memberBalanceInputData = { 'token': strongToken};
			var memberBalanceResponse =  externalService.callService(Url,memberBalanceInputData,"GET");
			memberBalanceResponse.then(function(memberBalanceResponseData){
				$scope.hideLoading();
				$scope.faultMessg = memberBalanceResponseData["types.FaultMessage"];
				if(memberBalanceResponseData["error"] == "Invalid Input" || memberBalanceResponseData["error"])
				{
				    $scope.hideLoading();
                	$rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
					$rootScope.showError();
					$scope.go("/content/phonegap/payback/apps/payback/en/home");
				}
				else if ($scope.faultMessg) {
				    $scope.hideLoading();
				    $scope.faultMessg=memberBalanceResponseData["types.FaultMessage"]["types.Message"];
                    if ($scope.faultMessg == "Invalid token") {
                        $rootScope.errorMsg = "Token seems invalid, please check again.";
                        localStorage.removeItem('stronglogintoken');
                        localStorage.removeItem('weaklogintoken');
                        localStorage.removeItem('userPoint');
                        $rootScope.showError();
                        $scope.go("/content/phonegap/payback/apps/payback/en/home");
                    }
                    else {
                        $rootScope.strongLoginRedirection("/content/phonegap/payback/apps/payback/en/home/my-balance");
					}
				}
				else
				{
					$scope.isBalance = true;
					$scope.accountBalance = memberBalanceResponseData;
					$scope.hideLoading();
				}
			});
			/* service end*/
		}
	}
}

data.then(function(response) {
	$scope.Title = response.data["content-par/userblock"].items;
});

$scope.contentWrapper = "dynamic-header";
$scope.headerHtml ="<div class='myprofile-title col'>"+
                    "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/user_icon.png' width='20' height='20' id='brand-logo' ng-if='!userblockExports'>"+
                    "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/user_icon.png' width='20' height='20' id='brand-logo' ng-if='userblockExports'>"+
                    "<p>{{ angularPageTitle | uppercase}}</p>"+
                   "</div>";
$rootScope.headerClass = "row row-center brand-header";


if($scope.editMode == 'DISABLED') {
    $scope.userBlockServices();
    $rootScope.getMember();
}

}])









// Controller for page 'my-transactions'
.controller('contentphonegappaybackappspaybackenhomemybalancemytransactions', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/my-balance/my-transactions.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'my-transactions';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/my-balance/my-transactions';
		$rootScope.angularPageTitle = $('<div/>').html('My Transactions').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/my-balance';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* transactionlist component controller (path: content-par/transactionlist) */
		$scope.transactionresponse = [];
		$scope.moredata =0;
		$scope.noMoreTransactions=false;
		$scope.noTransactions=false;
		$scope.loadMore=false;
		$scope.editMode = 'DISABLED';
		$scope.isMember = false;
		$scope.isBalance = false;
		 var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
		$rootScope.transactionappExports = false;
            if ( app ) {

            	$rootScope.transactionappExports = true;

            }else
            	{
            		$rootScope.transactionappExports = false;
            	}

        var strongToken = $scope.getloginToken();
$rootScope.redirectedFromProductCoupons =  false;
     $scope.options = [
                { label: 'Mr', value: 1 },
                { label: 'Mrs', value: 2 },
                 { label: 'Ms', value: 3 }
              ];


if($rootScope.isDummyUser)
{
        $("#enroll").css("display","block");
}else
{
    $("#getMemberBlock").css("display","block");
}

if($scope.editMode == 'DISABLED')
{
    $scope.showLoading();

}

$scope.toggleTransaction = function(group) {
    if ($scope.isTransactionShown(group)) {
        $scope.shownGroup = null;
    } else {
        $scope.shownGroup = group;
    }
}

$scope.isTransactionShown = function(group) {
    return $scope.shownGroup === group;
}

if($scope.editMode == 'DISABLED') {
    $scope.transactionDetails = function(){
        if(!$scope.loadMore) {
            var loadmore=0;

            loadmore = $scope.moredata + 10;

            if($scope.editMode == 'DISABLED')
            {
                $scope.showLoading();
            }
            var serverUrl = localStorage.getItem("serverUrl");
            var finalUrl = "";
            if (serverUrl == null || serverUrl == '') {
                finalUrl = '/payback/secure/userops.html/account/transactions';
            } else {
                finalUrl = serverUrl+'/payback/secure/userops.html/account/transactions';
            }
            var transactionDetailsRequestData={'token':strongToken,'numberOfRecords':'10','pageSize':'10','offSet':$scope.moredata};
            var transactionDetailsResponse=externalService.callService(finalUrl,transactionDetailsRequestData,"POST");
            transactionDetailsResponse.then(function(transactionDetailsResponseData){
                $scope.serverTranscationUrl = "";
                    var transacrtionError = transactionDetailsResponseData["error"];
                    $scope.faultMessage = transactionDetailsResponseData["types.FaultMessage"];
                    $scope.transactionStatus  = false;
                    if(transacrtionError)
                    {
                        $scope.hideLoading();
                        $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
                        $rootScope.showError();
                        $scope.go("/content/phonegap/payback/apps/payback/en/home");
                    }
                    else if ($scope.faultMessage) {
                        $scope.hideLoading();
                        $scope.faultMessage=transactionDetailsResponseData["types.FaultMessage"]["types.Message"];
                        if ($scope.faultMessage == "Invalid token") {
                            $rootScope.errorMsg = "Token seems invalid, please check again.";
                            localStorage.removeItem('stronglogintoken');
                            localStorage.removeItem('weaklogintoken');
                            localStorage.removeItem('userPoint');
                            $rootScope.showError();
                            $scope.go("/content/phonegap/payback/apps/payback/en/home");
                        }
                        else {
                            $rootScope.strongLoginRedirection("/content/phonegap/payback/apps/payback/en/home/my-balance");
                        }
                    }
                    else if(transactionDetailsResponseData['message']) {
                        $scope.hideLoading();
                        $scope.noTransactions=true;
                        $scope.loadMore=true;
                    }
                    else if(transactionDetailsResponseData["extint.AccountTransactionEvent"] && transactionDetailsResponseData["extint.AccountTransactionEvent"].length == 0){
                        $scope.hideLoading();
                        $scope.noTransactions=true;
                        $scope.loadMore=true;
                    }
                    else
                    {
                        $scope.moredata = $scope.moredata + 10;
                        if(transactionDetailsResponseData["extint.AccountTransactionEvent"]) {
                            if(transactionDetailsResponseData["extint.AccountTransactionEvent"].length ==undefined && $scope.moredata==10)
                            {
                                $scope.singleTransaction  = true;
                            }
                            else {
                                $scope.singleTransaction  = false;
                            }

                            if(!$scope.singleTransaction) {
                                $scope.transactionresponse.push(transactionDetailsResponseData);
                            }
                            else {
                                $scope.transaction = transactionDetailsResponseData;
                            }
                            $scope.transactionStatus  = true;
                            $scope.serverTranscationUrl = localStorage.getItem("serverUrl");
                            $scope.hideLoading();
                        }
                        if(transactionDetailsResponseData["extint.TotalResultSize"] <= $scope.moredata) {
                            $scope.noMoreTransactions=true;
                            $scope.loadMore=true;
                        }
                        $scope.hideLoading();
                    }
                $scope.$broadcast('scroll.infiniteScrollComplete');
            });
        }
    }
}

data.then(function(response) {
	$scope.Title = response.data["content-par/transactionlist"].items;
});

$scope.contentWrapper = 'transaction-content-wrap';
$scope.isTransaction = "true";
$scope.transactionStrip= "<div class='col col-center'><p>DATE</p></div><div class='col col-center'><p>PARTNER</p></div><div class='col col-center'><p>POINTS</p></div>";

$scope.headerHtml= "<div class='myprofile-title col'>"+
        "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/user_icon.png' width='20' height='20' id='brand-logo'  ng-if='!transactionappExports'>"+
        "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/user_icon.png' width='20' height='20' id='brand-logo'  ng-if='transactionappExports'>"+
        "<p>{{ angularPageTitle | uppercase}}</p>"+
    "</div>";
$rootScope.headerClass = "row row-center brand-header";
}])









// Controller for page 'my-profile'
.controller('contentphonegappaybackappspaybackenhomemybalancemyprofile', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/my-balance/my-profile.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'my-profile';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/my-balance/my-profile';
		$rootScope.angularPageTitle = $('<div/>').html('My Profile').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/my-balance';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* getmember component controller (path: content-par/getmember) */
		$scope.editMode = 'DISABLED';
		$scope.isMember = false;

        var strongToken = $scope.getloginToken();
	$rootScope.redirectedFromProductCoupons =  false;
     $scope.options = [
                { label: 'Mr', value: 1 },
                { label: 'Mrs', value: 2 },
                 { label: 'Ms', value: 3 }
              ];


if($rootScope.isDummyUser)
{
        $("#enroll").css("display","block");
}else
{
    $("#getMemberBlock").css("display","block");
}

if($scope.editMode == 'DISABLED')
{
    $scope.showLoading();

}

$rootScope.errorMsg =  null;
$scope.getMemeberUpdate = function(){

		var addressone = document.getElementById("addressone").value;
		var addresstwo = document.getElementById("addresstwo").value;
		var addressLine3 = document.getElementById("addressthree").value;
		var pin = document.getElementById("pincode").value;
		var state = document.getElementById("state").value;
		var city = document.getElementById("city").value;
		var serverUrl = localStorage.getItem("serverUrl");
		var email = document.getElementById("email").value;
		var selectedRadio = $("#updateRadioValue input[type='radio']:checked");
		var salutation = selectedRadio.val();
		if (serverUrl == null || serverUrl == '') {
			serverUrl = '/payback/secure/userops.html/profile/update';
		} else {
			serverUrl = serverUrl+'/payback/secure/userops.html/profile/update';
		}
		if(strongToken!=null)
		{
			var updateMemberRequestData = { 'token': strongToken,'salutation' : salutation, 'addressLine1' : addressone, 'addressLine2' : addresstwo, 'addressLine3' : addressLine3, 'city':city, 'region': state, 'emailId': email, 'firstName' : $scope.firstName, 'lastName' : $scope.lastName, 'dateOfBirth' : $scope.dob, 'zip' : pin };
			var updateMemberResponse = externalService.callService(serverUrl,updateMemberRequestData,"POST");
            if($scope.editMode == 'DISABLED')
            {
                $scope.showLoading();
            }
			updateMemberResponse.then(function(updateMemberResponseData){
                $scope.hideLoading();
				$scope.faultMsg = updateMemberResponseData["types.FaultMessage"];
				$scope.errorMsg=updateMemberResponseData["error"];
				if($scope.errorMsg) {
					$rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
					$rootScope.showError();

				}
				else if($scope.faultMsg) {
				    $scope.faultMsg=updateMemberResponseData["types.FaultMessage"]["types.Message"];
                    if ($scope.faultMsg == "Invalid token") {
                        $rootScope.errorMsg = "Token seems invalid, please check again.";
                        localStorage.removeItem('stronglogintoken');
                        localStorage.removeItem('weaklogintoken');
                        localStorage.removeItem('userPoint');
                        $rootScope.showError();
                        $scope.go("/content/phonegap/payback/apps/payback/en/home");
                    }
                    else {
					    $rootScope.warningMsg = $scope.faultMsg;
                        $scope.showWarning();
					}
				}
				else{
					$scope.isMember = false;
					$rootScope.successMsg = updateMemberResponseData["message"];
					$scope.showSuccess();
					$scope.getMember();
				}
			});
		}
}

data.then(function(response) {
	$scope.Title = response.data["content-par/getmember"].items;
});

$scope.contentWrapper = "dynamic-header";
$scope.headerHtml ="<div class='myprofile-title col'>"+
                   "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/user_icon.png' width='20' height='20' id='brand-logo' ng-if='!appExports'>"+
                   "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/user_icon.png' width='20' height='20' id='brand-logo' ng-if='appExports'>"+
                   "<p>{{ angularPageTitle | uppercase}}</p>"+
                   "</div>";
$rootScope.headerClass = "row row-center brand-header";

$scope.shownewpin = false;
  $scope.togglePin = function(){
    $scope.shownewpin = !$scope.shownewpin;
  }

$scope.changePin = function()
{
    var oldPin  =  parseInt(document.getElementById("5").value + document.getElementById("6").value + document.getElementById("7").value + document.getElementById("8").value);
    var newPin  =  parseInt(document.getElementById("9").value + document.getElementById("10").value + document.getElementById("11").value+ document.getElementById("12").value);
    var pinVerification  =  parseInt(document.getElementById("13").value + document.getElementById("14").value + document.getElementById("15").value + document.getElementById("16").value);


    if(newPin!=pinVerification) {
        $("matching").css("display","block");
    }
    else {
        var serverUrl = localStorage.getItem("serverUrl");
        if (serverUrl == null || serverUrl == '') {
            serverUrl = '/payback/secure/userops.html/account/password';
        } else {
            serverUrl = serverUrl+'/payback/secure/userops.html/account/password';
        }
        var changePinRequestData={ 'currentPin' : oldPin, 'newPin' : newPin, 'token' : strongToken }
		var changePinResponse = externalService.callService(serverUrl,changePinRequestData,"POST");
		if($scope.editMode == 'DISABLED')
		{
            	$scope.showLoading();
		}
		changePinResponse.then(function(changePinResponseData){
			$scope.hideLoading();
			var faultMsg = changePinResponseData["types.FaultMessage"];
            var errorMessage = changePinResponseData["error"];
            if(faultMsg) {
            	faultMsg=changePinResponseData["types.FaultMessage"]["types.Message"];

                if (faultMsg == "Invalid token") {
                    $rootScope.errorMsg = "Token seems invalid, please check again.";
                    localStorage.removeItem('stronglogintoken');
                    localStorage.removeItem('weaklogintoken');
                    localStorage.removeItem('userPoint');
                    $rootScope.showError();
                    $scope.go("/content/phonegap/payback/apps/payback/en/home");
                }
                else if(faultMsg == "The new PIN cannot be equal to the old PIN") {
                    $rootScope.errorMsg = "We do not accept old stuff hence the PIN needs to be updated!";
                    $rootScope.showError();
                }
                else if(faultMsg == "PIN should not be same as DOB(DDMM)") {
                    $rootScope.errorMsg = "We know you birthday already and that cannot be your PIN. Choose another unique code.";
                    $rootScope.showError();
                }
                else if(faultMsg == "The specified PIN number has to be a 4 digit number") {
                    $rootScope.errorMsg = "The PIN has to be 4 digits, no less and no more so try again!";
                    $rootScope.showError();
                }
                else {
                    $rootScope.errorMsg = faultMsg;
                    $rootScope.showError();
                    document.getElementById("5").value = "";
                    document.getElementById("6").value = "";
                    document.getElementById("7").value = "";
                    document.getElementById("8").value = "";
                    document.getElementById("9").value = "";
                    document.getElementById("10").value = "";
                    document.getElementById("11").value = "";
                    document.getElementById("12").value = "";
                    document.getElementById("13").value = "";
                    document.getElementById("14").value = "";
                    document.getElementById("15").value = "";
                    document.getElementById("16").value = "";
                }
            }
            else if (errorMessage) {
                $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
                $rootScope.showError();
                document.getElementById("5").value = "";
                document.getElementById("6").value = "";
                document.getElementById("7").value = "";
                document.getElementById("8").value = "";
                document.getElementById("9").value = "";
                document.getElementById("10").value = "";
                document.getElementById("11").value = "";
                document.getElementById("12").value = "";
                document.getElementById("13").value = "";
                document.getElementById("14").value = "";
                document.getElementById("15").value = "";
                document.getElementById("16").value = "";
            }
            else {
                $scope.isMember = false;
                $rootScope.successMsg = "PIN changed successfully!";
                $scope.showSuccess();
                document.getElementById("5").value = "";
                document.getElementById("6").value = "";
                document.getElementById("7").value = "";
                document.getElementById("8").value = "";
                document.getElementById("9").value = "";
                document.getElementById("10").value = "";
                document.getElementById("11").value = "";
                document.getElementById("12").value = "";
                document.getElementById("13").value = "";
                document.getElementById("14").value = "";
                document.getElementById("15").value = "";
                document.getElementById("16").value = "";
            }
		});
    }
}

$scope.getMemberValidation = function() {
    $.validator.addMethod("mobileNumber", function(value, element) {
        var mobileNumberLength=$("#mobileNumber").val();
        if(mobileNumberLength.length==10 ||mobileNumberLength.length==12){
         return true;
        }else{
            return false;
        }
     }, "Invalid");
    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z-. ]+$/i.test(value);
    }, "Letters only please");
    $.validator.addMethod("addressInput", function(value, element) {
        return this.optional(element) || /^[0-9a-z#',-./"&_ ]+$/i.test(value);
    }, "Invalid address");
    $.validator.addMethod("emailInput", function(value, element) {
        return this.optional(element) || /^[A-Z0-9._-]+@[A-Z0-9.-]+\.(?:[A-Z]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum|inc)$/i.test(value);
    }, "Letters only please");

    var validator = $("#getMemberForm").validate({
        errorClass: "error",
        errorElement: "div",
        rules : {
            title :{
                required:true
            },
            addressone:{
                required: true,
                addressInput:true,
                maxlength : "50",
                minlength : "1"
            },
            addresstwo:{
                required: true,
                maxlength : "50",
                addressInput:true
            },
            addressthree : {
                maxlength : "50",
                addressInput:true
            },
            email : {
                required: {
                    depends:function(){
                        $(this).val($.trim($(this).val()));
                        return true;
                    }
                },
                emailInput:true
            },
            mobileNumber : {
                required: true,
                mobileNumber:true
            },
            state : {
                required: true,
                lettersonly:true,
                maxlength : "30"
            },
            city : {
                required: true,
                lettersonly:true,
                maxlength : "30"
            },
            pincode : {
                required: true,
                maxlength : "6",
                minlength : "6"
            }
        },
        groups: {
            pinNumbers: "title"
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "title")
            error.insertAfter("#updateTitle");
            else
            error.insertAfter(element);
        },
        messages : {
            title :{
                required: "Please select Title!",
            },
            addressone:{
                required: "Hi, we would like to know where you stay.",
                minlength: "At least one character required here.",
                maxlength: "Character limits is only 50, please proceed to next line.",
                addressInput: "Please enter valid input"
            },
            addresstwo : {
                required: "Please enter Society/Street Name.",
                maxlength: "Character limits exhausted, proceed to the next line.",
                addressInput: "Please enter valid input"
            },
            addressthree : {
                maxlength: "Address line3 should be maximum 50 characters!",
                addressInput: "Please enter valid input"
            },
            state : {
                required: "Please select State!",
                lettersonly: "Please enter valid input",
                maxlength: "State should be maximum 30 characters!"
            },
            city : {
                required: "Please select City!",
                lettersonly: "Please enter valid input",
                maxlength: "City should be maximum 30 characters!"
            },
            pincode : {
                required: "Area PIN code required.",
                maxlength: "Area PIN Codes are 6 digits always.",
                minlength : "Area PIN Codes are 6 digits always."
            },
            email : {
                required: "Please enter Email-Id!",
                emailInput : "Please enter a valid Email ID. E.g.: name@email.com"
            },
            mobileNumber : {
                required: "Please enter your 10 digit mobile number without the country code.",
                mobileNumber:"Invalid Mobile Number!"
            }
        }
    });
    if (validator.form()) {
        $scope.getMemeberUpdate();
    }

}
$scope.changePinValidation = function() {
    $.validator.addMethod("oldPinNewPin", function(value, element) {
        var oldPin1 = $("#9").val();
        var oldPin2 = $("#10").val();
        var oldPin3 = $("#11").val();
        var oldPin4 = $("#12").val();
        var newPin1 = $("#13").val();
        var newPin2 = $("#14").val();
        var newPin3 = $("#15").val();
        var newPin4 = $("#16").val();
        if(oldPin1 == newPin1 && oldPin2 == newPin2 && oldPin3 == newPin3 && oldPin4 == newPin4 ){
            return true;
        } else{
            return false;
        }
    }, "Invalid");
    var validator = $("#changePinForm").validate({
        errorClass: "error",
        errorElement: "div",
        rules : {
            pin5:{
                required: true
            },
            pin6:{
                required: true
            },
            pin7:{
                required: true
            },
            pin8:{
                required: true
            },
            pin9:{
                required: true
            },
            pin10:{
                required: true
            },
            pin11:{
                required: true
            },
            pin12:{
                required: true
            },
            pin13:{
                required: true
            },
            pin14:{
                required: true
            },
            pin15:{
                required: true
            },
            pin16:{
                required: true,
                oldPinNewPin:true
            }
        },
        groups: {
            pinNumber: "pin5 pin6 pin7 pin8",
            pinNumber1: "pin9 pin10 pin11 pin12",
            pinNumber2: "pin13 pin14 pin15 pin16"
        },
        errorPlacement: function(error, element) {
            if (element.attr("name") == "pin5" || element.attr("name") == "pin6" || element.attr("name") == "pin7" || element.attr("name") == "pin8")
                error.insertAfter("#8");
            else if (element.attr("name") == "pin9" || element.attr("name") == "pin10" || element.attr("name") == "pin11" || element.attr("name") == "pin12")
                error.insertAfter("#12");
            else if (element.attr("name") == "pin13" || element.attr("name") == "pin14" || element.attr("name") == "pin15" || element.attr("name") == "pin16")
                error.insertAfter("#16");
            else
                error.insertAfter(element);
        },
        messages : {
            pin5:{
                required: "Please enter Old PIN!",
                minlength:"PIN has to be a 4 digit number",
                number:"Please enter a four digit number"
            },
            pin6:{
                required: "Please enter Old PIN!"
            },
            pin7:{
                required: "Please enter Old PIN!"
            },
            pin8:{
                required: "Please enter Old PIN!"
            },
            pin9:{
                required: "Please enter New PIN!"
            },
            pin10:{
                required: "Please enter New PIN!"
            },
            pin11:{
                required: "Please enter New PIN!"
            },
            pin12:{
                required: "Please enter New PIN!"
            },
            pin13:{
                required: "Please enter Confirm PIN!"
            },
            pin14:{
                required: "Please enter Confirm PIN!"
            },
            pin15:{
                required: "Please enter Confirm PIN!"
            },
            pin16:{
                required: "Please enter Confirm PIN!",
                oldPinNewPin : "New & Confirm PIN does not match!"
            }
        }
    });
    if (validator.form()) {
        $scope.changePin();
    }
}
$scope.scrollTop = function() {
    $ionicScrollDelegate.$getByHandle('modalContent').scrollTop(true);
}

if($scope.editMode == 'DISABLED') {
    $rootScope.getMember();
}


}])









// Controller for page 'change-pin'
.controller('contentphonegappaybackappspaybackenhomemybalancechangepin', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/my-balance/change-pin.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'change-pin';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/my-balance/change-pin';
		$rootScope.angularPageTitle = $('<div/>').html('Change Your 4 Digit PIN').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/my-balance';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }


		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* changepin component controller (path: content-par/changepin) */
		$scope.editMode = 'DISABLED';
		$scope.isMember = false;

		 var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
		$rootScope.changepinappExports = false;
            if ( app ) {

            	$rootScope.changepinappExports = true;

            }else
            	{
            		$rootScope.changepinappExports = false;
            	}

        var strongToken = $scope.getloginToken();
$rootScope.redirectedFromProductCoupons =  false;

if($rootScope.isDummyUser)
{
        $("#enroll").css("display","block");
}else
{
    $("#getMemberBlock").css("display","block");
}


 $rootScope.errorMsg = null;
data.then(function(response) {
	$scope.Title = response.data["content-par/changepin"].items;
});

$scope.contentWrapper = "dynamic-header";
$scope.headerHtml ="<div class='myprofile-title col'>"+
                    "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/user_icon.png' width='20' height='20' id='brand-logo' ng-if='!changepinappExports'>"+
                    "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/user_icon.png' width='20' height='20' id='brand-logo' ng-if='changepinappExports'>"+
                    "<p>{{ angularPageTitle | uppercase}}</p>"+
                   "</div>";
$rootScope.headerClass = "row row-center brand-header";




$scope.changePin = function()
{
    var oldPin  =  parseInt(document.getElementById("5").value );
    var newPin  =  parseInt(document.getElementById("9").value );
    var pinVerification  =  parseInt(document.getElementById("13").value );


    if(newPin!=pinVerification) {
        $("matching").css("display","block");
    }
    else {
        var serverUrl = localStorage.getItem("serverUrl");
        if (serverUrl == null || serverUrl == '') {
            serverUrl = '/payback/secure/userops.html/account/password';
        } else {
            serverUrl = serverUrl+'/payback/secure/userops.html/account/password';
        }
        var changePinRequestData={ 'currentPin' : oldPin, 'newPin' : newPin, 'token' : strongToken }
		var changePinResponse = externalService.callService(serverUrl,changePinRequestData,"POST");
		if($scope.editMode == 'DISABLED')
		{
            	$scope.showLoading();
		}
		changePinResponse.then(function(changePinResponseData){
			$scope.hideLoading();
			var faultMsg = changePinResponseData["types.FaultMessage"];
            var errorMessage = changePinResponseData["error"];
            if(faultMsg) {
            	faultMsg=changePinResponseData["types.FaultMessage"]["types.Message"];

                if (faultMsg == "Invalid token") {
                    $rootScope.changePinerrorMsg = "Token seems invalid, please check again.";
                    localStorage.removeItem('stronglogintoken');
                    localStorage.removeItem('weaklogintoken');
                    localStorage.removeItem('userPoin`t');
                    $scope.changePinshowError();
                    $scope.go("/content/phonegap/payback/apps/payback/en/home");
                }
                else if(faultMsg == "The new PIN cannot be equal to the old PIN") {
                    $rootScope.errorMsg = "We do not accept old stuff hence the PIN needs to be updated!";
                    $rootScope.showError();
                }
                else if(faultMsg == "PIN should not be same as DOB(DDMM)") {
                    $rootScope.errorMsg = "We know you birthday already and that cannot be your PIN. Choose another unique code.";
                    $rootScope.showError();
                }
                else if(faultMsg == "The specified PIN number has to be a 4 digit number") {
                    $rootScope.errorMsg = "The PIN has to be 4 digits, no less and no more so try again!";
                    $rootScope.showError();
                }
                else {
                    $rootScope.changePinerrorMsg = faultMsg;
                    $scope.changePinshowError();
                    document.getElementById("5").value = "";
                    document.getElementById("9").value = "";
                    document.getElementById("13").value = "";
                }
            }
            else if (errorMessage) {
                $rootScope.changePinerrorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
                $scope.changePinshowError();
                document.getElementById("5").value = "";
                document.getElementById("9").value = "";
                document.getElementById("13").value = "";
            }
            else {
                $scope.isMember = false;
                $rootScope.successMsg = "PIN changed successfully!";
                $scope.showSuccess();
                document.getElementById("5").value = "";
                document.getElementById("9").value = "";
                document.getElementById("13").value = "";
            }
		});
    }
}

$scope.changePinValidation = function() {
    $.validator.addMethod("oldPinNewPin", function(value, element) {
        var oldPin1 = $("#9").val();
        var newPin1 = $("#13").val();
        if(oldPin1 == newPin1){
            return true;
        } else{
            return false;
        }
    }, "Invalid");
    var validator = $("#changePinForm").validate({
        errorClass: "error",
        errorElement: "div",
        rules : {
            pin5:{
                required: true,
                minlength : "4"
            },
            pin9:{
                required: true ,
                minlength:"4"
            },
            pin13:{
                required: true,
                minlength:"4",
                oldPinNewPin :true
            }
        },

        messages : {
            pin5:{
                required: "Please enter your old PIN.",
                minlength:"PIN is the 4 digit secret code you chose while account creation."
            },
            pin9:{
                required: "Please enter your new 4 digit PIN.",
                minlength:"PIN has to be 4 digits, please check."
            },
            pin13:{
                required: "Please re-enter your new PIN.",
                minlength:"PIN has to be 4 digits, please check.",
                oldPinNewPin : "Both the PINs have to be same, please enter PIN again."
            }
        }
    });
    if (validator.form()) {
        $scope.changePin();
    }
}
var errorPopUp = null;
$scope.changePinshowError = function()
  {

  // An elaborate, custom popup
    errorPopUp = $ionicPopup.show({
    templateUrl : 'changePinerrorPopUp',
    scope: $scope,
    onTap: function(e) {
       e.preventDefault();
    }

  });
  errorPopUp.then(function(res) {

  });

  };
  $scope.errorclosePop = function(){
    errorPopUp.close();
  };


}])









// Controller for page 'linking'
.controller('contentphonegappaybackappspaybackenhomemybalancelinking', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/my-balance/linking.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'linking';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/my-balance/linking';
		$rootScope.angularPageTitle = $('<div/>').html('Update Mobile Number').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home/my-balance';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* mobilelinking component controller (path: content-par/mobilelinking) */
		$scope.editMode = 'DISABLED';
$scope.showOtp = false;
$rootScope.redirectedFromProductCoupons =  false;

var strongToken = $scope.getloginToken();
$scope.enrolRegMember = false;
if(localStorage.getItem('enrolMember')) {
    var cardNumber = localStorage.getItem('cardNumber');
    $rootScope.successMsg = "</p>YOUR ENROLLMENT HAS BEEN SUCCESSFUL</p>" +
                            "<p>Your PAYBACK CARD NUMBER is <strong>"+cardNumber+"</strong>.</p>" +
                            "<p>Thank you for using the PAYBACK Program. Your PAYBACK card will reach you in 14 working days\n</p>";
    $scope.showSuccess();
    $scope.enrolRegMember = true;
    localStorage.removeItem('enrolMember');
    $('#mobileNum').val(localStorage.getItem("enrolRegMobileNo"));
    localStorage.removeItem("enrolRegMobileNo");
}

if(localStorage.getItem('registerMember')) {
    var cardNumber = localStorage.getItem('cardNumber');
    $rootScope.successMsg = "<p>CONGRATULATIONS!!!</p>" +
                            "<p>YOU HAVE SUCCESSFULLY REGISTERED WITH PAYBACK Your PAYBACK CARD NUMBER is <strong> "+cardNumber+"</strong>.</p>" +
                            "<p>Thank you for registering with PAYBACK Program\n</p>";
    $scope.showSuccess();
    $scope.enrolRegMember = true;
    localStorage.removeItem('registerMember');
    $('#mobileNum').val(localStorage.getItem("enrolRegMobileNo"));
    localStorage.removeItem("enrolRegMobileNo");
}

 var app = document.URL.indexOf( 'http://' ) === -1 && document.URL.indexOf( 'https://' ) === -1;
		$rootScope.mobilelinkExports = false;
            if ( app ) {

            	$rootScope.mobilelinkExports = true;

            }else
            	{
            		$rootScope.mobilelinkExports = false;
            	}
$scope.contentWrapper = "dynamic-header";
$scope.headerHtml ="<div class='myprofile-title col'>"+
                   "<img src='/etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/user_icon.png' width='20' height='20' id='brand-logo' ng-if='!mobilelinkExports'>"+
                   "<img src='../../../../../../etc/designs/phonegap/payback/ng-paybackapp/ng-clientlibsall/img/user_icon.png' width='20' height='20' id='brand-logo' ng-if='mobilelinkExports'>"+
                   "<p>{{ angularPageTitle | uppercase}}</p>"+
                   "</div>";
$rootScope.headerClass = "row row-center brand-header";
$scope.linkMobile = function() {

    var serverUrl = localStorage.getItem("serverUrl");
    var mobileNumber = document.getElementById("mobileNum").value;
    var otpValue = document.getElementById("otp").value;
    if($scope.editMode == 'DISABLED') {
        $scope.showLoading();
    }
    if (serverUrl == null || serverUrl == '') {
        serverUrl = '/payback/secure/userops.html/account/mobile/link';
    } else {
        serverUrl = serverUrl+'/payback/secure/userops.html/account/mobile/link';
    }
    var linkMobileData={'token': strongToken, 'mobileNumber':mobileNumber, 'otp':otpValue};
    var linkMobileResponse=externalService.callService(serverUrl,linkMobileData,"POST");
    linkMobileResponse.then(function(linkMobileResponseData){
        $scope.hideLoading();
        $scope.errorMsg=linkMobileResponseData["error"];
        $scope.faultMsg = linkMobileResponseData["types.FaultMessage"];
        if($scope.errorMsg) {
            $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
            $rootScope.showError();
            $scope.showOtp = true;
            document.getElementById("otp").value = '';
        }
        else if ($scope.faultMsg) {
            $rootScope.errorMsg = $scope.faultMsg["types.Message"];
            if($rootScope.errorMsg == "OTP Expired") {
                $rootScope.errorMsg = "The OTP has a very short life and it just expired. Generate a new one again.";
            }
            $rootScope.showError();
            $scope.showOtp = true;
            document.getElementById("otp").value = '';
        }
        else {

        	// if(window.ADB){
         //    	ADB.trackAction('linking');
        	// }
            $rootScope.successMsg = linkMobileResponseData["message"];
            $scope.showSuccess();
            $rootScope.mobileLinked = true;
            $scope.showOtp = false;
            document.getElementById("otp").value = '';
            $rootScope.linkedMobileNumber = mobileNumber;
        }
    });
}

$scope.sendOtp = function() {
    if($scope.editMode == 'DISABLED') {
        $scope.showLoading();
    }
    var serverUrl = localStorage.getItem("serverUrl");
    var mobileNumber = document.getElementById("mobileNum").value;
    if (serverUrl == null || serverUrl == '') {
        serverUrl = '/payback/secure/userops.html/account/otp';
    } else {
        serverUrl = serverUrl+'/payback/secure/userops.html/account/otp';
    }
    var otpInputData={'token': strongToken, 'mobileNumber':mobileNumber};
    var otpResponse=externalService.callService(serverUrl,otpInputData,"POST");
    otpResponse.then(function(otpResponseData){
        $scope.hideLoading();
        $scope.errorMsg=otpResponseData["error"];
        $scope.faultMsg = otpResponseData["types.FaultMessage"];
        if($scope.errorMsg) {
            $rootScope.errorMsg = "We cannot connect to the server either, you might want to visit us again in sometime.";
            $rootScope.showError();
        }
        else if ($scope.faultMsg) {
            $rootScope.errorMsg = $scope.faultMsg["types.Message"];
            if($rootScope.errorMsg == "OTP generation failed") {
                $rootScope.errorMsg = "OTP could not be generated, try again.";
            }
            $rootScope.showError();
        }
        else {
            $scope.showOtp = true;
            $rootScope.successMsg = otpResponseData["message"];
            if($rootScope.successMsg == "OTP generation successful"){
                $rootScope.successMsg = "OTP generated, use it quickly!";
            }
            $scope.showSuccess();
        }
    });
}

$scope.mobileNumberValidation = function() {
    var mobileNum = document.getElementById("mobileNum").value;
    if(mobileNum != "" && mobileNum == $rootScope.linkedMobileNumber){
        $('#mobileNumLinkedError').show();
    }
    else if(mobileNum == '' || mobileNum.length < 10) {
        $("#mobileNumError").show();
        $('#mobileNumLinkedError').hide();
    }
    else {
        $("#mobileNumError").hide();
        $('#mobileNumLinkedError').hide();
        $scope.sendOtp();
    }

}

$scope.checkLen = function(val) {
    if(val.length == 10) {
        $("#mobileNumError").hide();
        if(val != $rootScope.linkedMobileNumber) {
            $('#mobileNumLinkedError').hide();
        }
    }
}

$scope.mobileLinkingValidation = function() {
    $.validator.addMethod("otpLength", function(value, element) {
        var mobileNumberLength=$("#otp").val();
        if(mobileNumberLength.length==4 ||mobileNumberLength.length==6){
            return true;
        }else{
            return false;
        }
    }, "Invalid");
    var validator = $("#mobileLinkingForm").validate({
        errorClass: "error",
        errorElement: "div",
        rules : {
            mobileNum:{
                required: true,
                minlength : "10"
            },
            otp:{
                required : true,
                otpLength :true
            }
        },
        messages : {
            mobileNum:{
                required: "Please enter Mobile Number!",
                minlength:"Invalid Mobile Number!"
            },
            otp:{
                required: "Please enter OTP!",
                otpLength:"Invalid OTP!"
            }
        }
    });
    if (validator.form()) {
        $scope.linkMobile();
    }
}
}])









// Controller for page 'error'
.controller('contentphonegappaybackappspaybackenhomeerror', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/error.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'error';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/error';
		$rootScope.angularPageTitle = $('<div/>').html('Error').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* staticblock component controller (path: content-par/staticblock) */


    data.then(function(response) {
    var serverUrl = localStorage.getItem("serverUrl");
    $scope.htmlContent = response.data["content-par/staticblock"]["items"][0]["htmlText"];
    $scope.pageImage = response.data["content-par/staticblock"]["items"][0]["pageImage"];
    if(serverUrl != null || serverUrl != ' '){
        $scope.pageImage = serverUrl + $scope.pageImage
    }
    $scope.pageTitle = response.data["content-par/staticblock"]["items"][0]["pageTitle"];

	var div1 = document.getElementById("htmlContent");
	var decoded = $("<div/>").html($scope.htmlContent).text();


	$scope.decodedHtml =decoded;

	$scope.headerHtml ="<div class='policy-title col'>"+
                            "<img src='"+$scope.pageImage+"' alt='store' class='pop-icons'>"+
                            "<p>"+$scope.pageTitle+"</p>"+
                        "</div>";

});
$scope.headerHtml = "";
$scope.headerClass = "row row-center policy-header";
$scope.ionContentClass = "";
$rootScope.redirectedFromProductCoupons =  false;
}])









// Controller for page 'category-display'
.controller('contentphonegappaybackappspaybackenhomecategorydisplay', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/category-display.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'category-display';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/category-display';
		$rootScope.angularPageTitle = $('<div/>').html('Category Display').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }


		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	







/* categorydisplay component controller (path: content-par/categorydisplay) */


$rootScope.isValChk = {};
$rootScope.filterParameters;
$rootScope.catNoMore;
$rootScope.totalPages;
$rootScope.pageCount;
$scope.noProducts;
$rootScope.displayCount;

$scope.categoryDispServerUrl = localStorage.getItem("serverUrl");
$scope.methodType = "GET";
$scope.categoryDispRestUrl = "/payback/anon/shoponline.html/getCategoryData.json";
if($scope.categoryDispServerUrl && $scope.categoryDispServerUrl!="") {
    $scope.categoryDispRestUrl = $scope.categoryDispServerUrl + $scope.categoryDispRestUrl;
}

$scope.contentWrapper = "category-header";
$scope.headerHtml="";
$scope.displayCategories = function(filterParams, removeFilters, removeData) {
    try {

        $scope.showLoading();
        if((localStorage.getItem('categoryFilters') || localStorage.getItem('categoryBrand')) && removeFilters) {
            localStorage.removeItem('categoryFilters');
            localStorage.removeItem('categoryBrand');
        }
        var requestParams = {
            'filterParams' : filterParams
        }

        $rootScope.filterParameters = filterParams;

        var onLoadCategoryDisplay = externalService.callService($scope.categoryDispRestUrl, requestParams, $scope.methodType);
        onLoadCategoryDisplay.then(function(response) {
            console.log(response);
            if($rootScope.category == 'Authors' || $rootScope.category == 'Categories') {
                if(removeData) {
                    $rootScope.categoryProducts = response['prods']['booklist'];
                }
                else {
                    jQuery.each(response['prods']['booklist'], function(objKey, objVal) {
                        $rootScope.categoryProducts.push(objVal);
                    });
                }
                $rootScope.categoryFilterData = [];
                $rootScope.productCount = response['total_counts']['booklist'];
                if($rootScope.category == 'Categories') {
                    $scope.authorList = response['authorList'];
                }
            }
            else {
                $rootScope.categoryFilterData = response['filters']['specFilters'];
                $rootScope.categoryBrandsData = response['filters']['brands'];
                $rootScope.productCount = response['total_counts']['products'];
                if(removeData) {
                    $rootScope.categoryProducts = response['prods']['products'];
                }
                else {
                    jQuery.each(response['prods']['products'], function(objKey, objVal) {
                        $rootScope.categoryProducts.push(objVal);
                    });

                }
            }
            $scope.constructCategoryResponse($rootScope.categoryProducts);
            $rootScope.totalPages = Math.ceil($rootScope.productCount/32);
            $scope.$broadcast('scroll.infiniteScrollComplete');
            $scope.hideLoading();
        });
    }
    catch(err) {
        $scope.hideLoading();
        console.log("** ERROR in displayCategories method ** "+err);
    }
}

$scope.constructCategoryResponse = function(products) {
    try {
        $rootScope.categoryArr = [];
        $scope.categoryObj = [];
        if(products != undefined && products.length > 0) {
            $rootScope.displayCount = products.length;
            jQuery.each(products, function(objKey, objVal) {

                if($scope.categoryObj.length == 2) {
                    $rootScope.categoryArr.push($scope.categoryObj);
                    $scope.categoryObj = [];
                    $scope.categoryObj.push(objVal);

                }
                else {
                    $scope.categoryObj.push(objVal);
                }
            });

            if($scope.categoryObj != null && $scope.categoryObj.length > 0) {
                $rootScope.categoryArr.push($scope.categoryObj);
            }
        }
        else {
            $scope.noProducts = true;
            $rootScope.displayCount = 0;
            $rootScope.productCount = 0;
        }

    }
    catch(err) {
        console.log("** ERROR in Category Display Component constructCategoryResponse method **"+err);
    }
}

$scope.categoryBrandChange = function(brand) {
    try {
        console.log(brand);
        $scope.closePopoverBrandsFilter();
        $rootScope.isValChk = $scope.unCheckFilter($rootScope.isValChk);
        $rootScope.pageCount = 1;
        $rootScope
        $rootScope.catNoMore = false;
        var selCategory = localStorage.getItem("selCategory");
        var productNoun = localStorage.getItem("productNoun");
        var filterJson = {
            "params": [{
                "key": "category",
                "value": selCategory
            },
            {
                "key": "no_products",
                "value": 32
            },
            {
                "key": "page",
                "value": 1
            },
            {
                "key": "brand",
                "value": brand
            }]
        }
        if(productNoun) {
            filterJson["params"].push({"key": "noun", "value": productNoun})
        }
        if(localStorage.getItem('categoryFilters')) {
            localStorage.removeItem('categoryFilters');
        }
        localStorage.setItem("categoryBrand", brand);
        $scope.displayCategories(filterJson, '', true);
    }
    catch(err) {
        console.log("** ERROR in Category Display Component categoryBrandChange method **")
    }
}

$scope.categoryFilterChange = function(displayName, filterValue, filterId) {
    try {
        var filterChk = $('#'+filterId).is(':checked');
        console.log(displayName+" "+filterValue);
        $scope.closePopoverCategoryFilter();
        $rootScope.pageCount = 1;
        $rootScope.catNoMore = false;
        var selCategory = localStorage.getItem('selCategory');
        var productNoun = localStorage.getItem('productNoun');
        var categoryBrand = localStorage.getItem('categoryBrand');
        var filters = [];
        var filterJson = {
            "params": [{
                "key": "no_products",
                "value": 32
            },
            {
                "key": "page",
                "value": 1
            }]
        }
        if(selCategory && selCategory == 'Authors') {
            filterJson['params'].push({"key": "category", "value": "books"});
        }
        else {
            filterJson['params'].push({"key": "category", "value": selCategory});
        }
        if(productNoun && selCategory == 'Authors') {
            filterJson['params'].push({"key": "authid", "value": productNoun});
        }
        else {
            filterJson['params'].push({"key": "noun", "value": productNoun});
        }
        if(categoryBrand && selCategory != 'Authors') {
            filterJson['params'].push({"key": "brand", "value": categoryBrand});
        }

        if(filterValue != undefined && filterChk) {

            $rootScope.isValChk[filterValue] = true;

            if(localStorage.getItem('categoryFilters')) {
                filters = JSON.parse(localStorage.getItem('categoryFilters'));
                filters = $scope.getCategoryFilters(filters, displayName, filterValue);
                localStorage.setItem('categoryFilters', JSON.stringify(filters));
                jQuery.each(filters, function(objKey, objVal) {
                    filterJson['params'].push(objVal);
                });
            }
            else {
                var filter = {"key": displayName, "value": filterValue};
                filters.push(filter);
                localStorage.setItem('categoryFilters', JSON.stringify(filters));
                filterJson['params'].push(filter);
            }

        }
        else {
            $rootScope.isValChk[filterValue] = false;
            if(localStorage.getItem('categoryFilters')) {
                filters = JSON.parse(localStorage.getItem('categoryFilters'));
                filters = $scope.getCategoryRemoveFilter(filters, displayName, filterValue);
                localStorage.setItem('categoryFilters', JSON.stringify(filters));
                jQuery.each(filters, function(objKey, objVal) {
                    filterJson['params'].push(objVal);
                });
            }

        }
        console.log(filterJson);

        $scope.displayCategories(filterJson, '', true);
    }
    catch(err) {
        console.log("** ERROR in Category Display Component categoryFilterChange method **");
    }
}

$scope.categoryAuthorChange = function(authId) {
    try {
        console.log(authId);
        $scope.closePopoverAuthorsFilter();
        $rootScope.pageCount = 1;
        $rootScope.catNoMore = false;
        $rootScope.authId = authId;
        var selCategory = localStorage.getItem("selCategory");
        var productNoun = localStorage.getItem("productNoun");
        var filterJson = {
            "params": [{
                "key": "no_products",
                "value": 32
            },
            {
                "key": "page",
                "value": 1
            },
            {
                "key": "category",
                "value": "books"
            },
            {
                "key": "book_catid",
                "value": productNoun
            },
            {
                "key": "authid",
                "value": authId
            }]
        }
        $scope.displayCategories(filterJson, '', true);
    }
    catch(err) {
        console.log("** ERROR in Category Display Component categoryAuthorChange method **"+err);
    }
}

$rootScope.getMoreCategoriesData = function() {
    try {
        if(!$rootScope.pageCount) {
            $scope.onLoad = true;
            $rootScope.pageCount = 1;
        }
        else {
            if($rootScope.totalPages > $rootScope.pageCount) {
                $scope.onLoad = false;
                $rootScope.pageCount = $rootScope.pageCount + 1;
            }
            else {
                $rootScope.catNoMore = true;
            }
        }
        if(!$rootScope.catNoMore) {
            var catObj;
            var nounObj;
            var authIdObj;
            var catBrand;
            localStorage.setItem('selCategory', $rootScope.category);
            catBrand = localStorage.getItem('categoryBrand')
            if($rootScope.category && ($rootScope.category == "Authors" || $rootScope.category == "Categories")) {
                catObj = {"key": "category", "value": "books"};
            }
            else {
                catObj = {"key": "category", "value": $rootScope.category};
            }

            if($rootScope.product != undefined && $rootScope.product != '') {
                var productURLArr = $rootScope.product.split("/");
                var productNoun = productURLArr[productURLArr.length-1];
                if($rootScope.category == 'Authors') {
                    nounObj = {"key": "authid", "value": productNoun};
                }
                else if($rootScope.category == "Categories") {
                    nounObj = {"key": "book_catid", "value": productNoun};
                    if($rootScope.authId) {
                        authIdObj = {"key": "authid", "value": $rootScope.authId};
                    }
                }
                else {
                    nounObj = {"key": "noun", "value": productNoun};
                }
                localStorage.setItem("productNoun", productNoun);
            }
            var filterJson = {
                "params": [{
                    "key": "no_products",
                    "value": 32
                },
                {
                    "key": "page",
                    "value": parseInt($rootScope.pageCount)
                }]
            }

            if(catObj != undefined) {
                filterJson['params'].push(catObj);
            }
            if(nounObj != undefined || nounObj != null) {
                filterJson['params'].push(nounObj);
            }
            if(authIdObj && $rootScope.category == "Categories") {
                filterJson['params'].push(authIdObj);
            }
            if(catBrand && $rootScope.category != "Authors" && $rootScope.category != "Categories") {
                filterJson['params'].push({"key": "brand", "value": catBrand});
            }
            var filters = [];
            if(localStorage.getItem('categoryFilters')) {
                filters = JSON.parse(localStorage.getItem('categoryFilters'));
                jQuery.each(filters, function(objKey, objVal) {
                    filterJson['params'].push(objVal);
                });
            }
            if($scope.onLoad) {
                $scope.displayCategories(filterJson, true, true);
            }
            else {
                $scope.displayCategories(filterJson);
            }
        }

    }
    catch(err) {
        console.log("** ERROR in Category Display Component getMoreCategoriesData method **"+err);
    }
}

$ionicPopover.fromTemplateUrl('category-brand-filter', {
    scope: $scope,
}).then(function(popoverBrandFilter) {
    $scope.popoverBrandFilter = popoverBrandFilter;
});

$scope.openPopoverBrandsFilter = function($event) {
    $scope.popoverBrandFilter.show($event);
};

$scope.closePopoverBrandsFilter = function() {
    $scope.popoverBrandFilter.hide();
};

$scope.$on('$destroy', function() {
    $scope.popoverBrandFilter.remove();
});

$scope.$on('popoverBrandFilter.hidden', function() {
    // Execute action
});

$scope.$on('popoverBrandFilter.removed', function() {
    // Execute action on remove popover
});

$ionicPopover.fromTemplateUrl('category-author-filter', {
    scope: $scope,
}).then(function(popoverAuthorFilter) {
    $scope.popoverAuthorFilter = popoverAuthorFilter;
});

$scope.openPopoverAuthorsFilter = function($event) {
    $scope.popoverAuthorFilter.show($event);
};

$scope.closePopoverAuthorsFilter = function() {
    $scope.popoverAuthorFilter.hide();
};

$scope.$on('$destroy', function() {
    $scope.popoverAuthorFilter.remove();
});

$scope.$on('popoverAuthorFilter.hidden', function() {
    // Execute action
});

$scope.$on('popoverAuthorFilter.removed', function() {
    // Execute action on remove popover
});

$ionicPopover.fromTemplateUrl('category-product-filter', {
    scope: $scope,
}).then(function(popoverCategoryFilter) {
    $scope.popoverCategoryFilter = popoverCategoryFilter;
});

$scope.openPopoverCategoryFilter = function($event) {
    $scope.popoverCategoryFilter.show($event);
};

$scope.closePopoverCategoryFilter = function() {
    $scope.popoverCategoryFilter.hide();
};

$scope.$on('$destroy', function() {
    $scope.popoverCategoryFilter.remove();
});

$scope.$on('popoverCategoryFilter.hidden', function() {
    // Execute action
});

$scope.$on('popoverCategoryFilter.removed', function() {
    // Execute action on remove popover
});

$scope.getCategoryFilters = function(obj, filterName, filterVal) {
    var brLoop = false;
    jQuery.each(obj, function(objKey, objVal) {
        if(objVal['key'] === filterName && objVal['value'] != filterVal) {
            obj.splice(objKey, 1);
            obj.push({"key": filterName, "value": filterVal});
            brLoop = true;
            return brLoop;
        }
        else if(objVal['key'] === filterName && objVal['value'] === filterVal) {
            brLoop = true;
            return brLoop;
        }
    });
    if(!brLoop) {
        obj.push({"key": filterName, "value": filterVal});
    }
    return obj;
}

$scope.getCategoryRemoveFilter = function(obj, filterName, filterVal) {
    jQuery.each(obj, function(objKey, objVal) {
        if(objVal['key'] === filterName && objVal['value'] === filterVal) {
            obj.splice(objKey, 1);
            return obj
        }
    });
    return obj;
}

$scope.unCheckFilter = function(isValChk) {
    for(var key in isValChk) {
        isValChk[key] = false;
    }
    return isValChk;
}
}])









// Controller for page 'EVERYBUY'
.controller('contentphonegappaybackappspaybackenhomeEVERYBUY', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/EVERYBUY.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'EVERYBUY';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/EVERYBUY';
		$rootScope.angularPageTitle = $('<div/>').html('EVERYBUY').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* smartsaver_0 component controller (path: content-par/smartsaver_0) */


data.then(function(response) {

});

var smartSaverPagePath = "/content/phonegap/payback/apps/payback/en/home";

$scope.headerHtml = "";
var share_msg= "";
var heart = "";
var formdata='';

$rootScope.smartSaverUID = localStorage.getItem("smartSaverUID");
if(!$rootScope.smartSaverUID) {
    $rootScope.goPartnerSite('smartSaver',$location.path(),'true');
}
console.log("smart saver uid ::"+$rootScope.smartSaverUID);
var url = "https://smartsaver.in/smart_saver_web/products/all_products2?u_id="+$rootScope.smartSaverUID;
if (isAuthor != 'true') {
    var smartSaverPromise =  externalService.callService(url,"",'GET');
    smartSaverPromise.then(function(obj){
        if(obj.success==1){
            var size = "";
            for (var i = 0; i < (obj.brands.length); i++) {
                var Brands = obj.brands[i].Brands;
                if(obj.favorite_brand_id){
                    if(Brands.id==63){
                        share_msg="I\'ve just started using Smart Saver, where you can earn rewards worth over Rs. 200 on your regular purchases every month. You can register through my personal link here, to get an additional joining bonus:"+obj.referral_link;
                    }else{
                        share_msg="Hey! I just earned rewards on my purchase of "+Brands.brand_name+" and several other brands on Smart Saver. Click on this link to become a Smart Saver too:"+obj.referral_link
                    }
                }
                //alert(obj.favorite_brand_id);
                if(obj.favorite_brand_id){
                    if($.inArray(obj.favorite_brand_id,Brands.id)){size=1;}else{ size=0;}
                }
                if($.inArray(Brands.id,obj.favorite_brand_id)){
                    heart = '';//'<i class="fa fa-heart" id="heart_'+Brands.id+'"></i>';
                }else{
                    heart = '';//'<i class="fa fa-heart-o" id="heart_'+Brands.id+'"></i>';
                }


                formdata+='<div class="col-md-3 col-xs-6 product-section product-row box-line">';
                formdata+='<div class="product-wrapper">';
                formdata+='<div class="row "><div class="product-top-bar">';
                formdata+='	<div class="share-option" id="share_option_'+Brands.id+'"><a id="fbshare" onclick="fbshare2();"><img style="border-radius: 15px;  margin-bottom: 50%;" src="https://smartsaver.in/smart_saver_web/app/webroot/img/FB_30.png"/></a><br>';
                formdata+='			<a class="share_product" id="share_'+Brands.id+'" brand_name="'+Brands.brand_name+'" data="'+share_msg+'" ng-click=share_product("share_'+Brands.id+'")>';
                formdata+='			<img src="https://smartsaver.in/smart_saver_web/app/webroot/img/email_30.png"/></a>';
                formdata+='<img class="arrow" id="arrow" style="margin-left: -60%;margin-top: 37%;position: absolute;width: 35%;" src="https://smartsaver.in/smart_saver_web/app/webroot/img/arrow_20.png"/></div>';
                //formdata+='	<a class="col-md-2 col-xs-2 text-center " id="share1_'+Brands.id+'" brand_name="'+Brands.brand_name+'" data="'+share_msg+'" ng-click=show_share("s_'+Brands.id+'")><img src="https://smartsaver.in/smart_saver_web/app/webroot/img/share.png" style="width:15;margin-top:0px"></a>';
                formdata+='	<a class="col-md-2 col-xs-2 text-center " id="share1_'+Brands.id+'" brand_name="'+Brands.brand_name+'" data="'+share_msg+'">';
                //formdata+='		<i class="fa fa-share-alt" id="s_'+Brands.id+'" ng-click=show_share("s_'+Brands.id+'")></i></a>';
                //formdata+='		<i class="fa fa-share-alt" id="s_'+Brands.id+'" ng-click=show_share("s_'+Brands.id+'")>Share</i></a>';
                formdata+='	<a class="col-md-8 col-xs-8 text-center product-title-wrapper" data="'+Brands.receipt_upload+'" brand_terms="'+Brands.brand_terms+'" dat1="'+obj.brands[i][0].start_date+'" dat2="'+obj.brands[i][0].end_date+'" bname="'+Brands.brand_name+'" data="'+Brands.receipt_upload+'" desc="" size="'+size+'" style="color:#666;" rel="'+Brands.brand_logo+'" id="name_'+Brands.id+'" ng-click=activity_popup("name_'+Brands.id+'")>'+Brands.brand_name+'</a>';
                formdata+='	 <a class="col-md-2 col-xs-2 text-center" id="like_'+Brands.id+'" onclick="like(this.id)">'+heart+'</a></div></div>';
                formdata+='	<div class="inner">';
                formdata+='		<center>';
                formdata+='			<div class="product_img" brand_terms="'+Brands.brand_terms+'" dat1="'+obj.brands[i][0].start_date+'" dat2="'+obj.brands[i][0].end_date+'" bname="'+Brands.brand_name+'" data="'+Brands.receipt_upload+'" desc="" size="'+size+'" rel="'+Brands.brand_logo+'" style="background:url(\'https://smartsaver.in/smart_saver/faayda_data/'+Brands.brand_logo+'\') center no-repeat;background-size:contain" id="image'+Brands.id+'" ng-click=activity_popup("name_'+Brands.id+'")></div>';
                formdata+='		</center>';
                formdata+='	<div class="clearfix center-block product-otherlinks-pats">';
                formdata+='		<div class="text-center productPrice center-block">';
                formdata+='			<font class="product_name" brand_terms="'+Brands.brand_terms+'" dat1="'+obj.brands[i][0].start_date+'" dat2="'+obj.brands[i][0].end_date+'" bname="'+Brands.brand_name+'" desc="" data="'+Brands.receipt_upload+'" size="'+size+'" rel="'+Brands.brand_logo+'" id="earn_'+Brands.id+'" ng-click=activity_popup("name_'+Brands.id+'")><a class="btn btn-primary-pats">'+Brands.reciept_amount+' points</a></font>';
                formdata+='		</div></div></div></div></div>';

                $("#fbshare_link").val(obj.referral_link);
                $("#usr").val(u_id);
                $scope.dynamicContent = $('<div/>').html(formdata);
                $scope.dynamicContent = $filter('rawHtml')($scope.dynamicContent);
                $scope.dynamicContent =  $compile($scope.dynamicContent)($scope);

                $('#all_product').html($scope.dynamicContent);
            }
            if(device) {
                if(device.platform == 'iOS') {
                    $("#file_upload").css('display','none');
                }
            }
        }
        else{
            //alert('else');
        }
	});
}





					$('#submit').click(function () {
                        $("#loading_img").css('display','block');
                        // $("#submit").css('display','none');
                        // $("#remove").css('display','none');
                        $(".upload_buttons").css('display','none');
                        var filepath = $('#imagePath').val();
                        upload_file(filepath);
                    });


    				$('#image_preview #remove').bind('click', function () {
    					$('#imagePath').val('');
    					$('#img_diplay').attr('src','');
    					$('#image_preview').css('background','#fff');
    					$("#im").css('display','none');
    					$('#receipt_text').css('display','block');
    					return false;
    				});







$scope.activity_popup = function(brand_id)
{

try{

/*var user_id=getCookie("user_id");
	$('#usr').val(user_id);
	if(user_id=='-1' || user_id==''){
		mLogin();
	}else{*/
		$(".loader").css('display','block');
		//var id=this.id;
		var id = brand_id.substring(5);
		$("#brand_id").val(id);
		//alert(id);return false;
		/*var upload=$(this).attr('data');
		var img=$(this).attr('rel');
		var like=$(this).attr('size');
		var description = $(this).attr('desc');
		var brand_name = $(this).attr('bname');
		var brand_terms = $(this).attr('brand_terms');*/
		var upload=$("#"+brand_id).attr('data');
		var img=$("#"+brand_id).attr('rel');
		var like=$("#"+brand_id).attr('size');
		var description = $("#"+brand_id).attr('desc');
		var brand_name = $("#"+brand_id).attr('bname');
		var brand_terms = $("#"+brand_id).attr('brand_terms');
		if(brand_terms !=''){$( "ol" ).append("<li>"+brand_terms+"</li>");}
		var d = new Date();
		var time = new Date();
		time.setDate(time.getDate()-90);
		var month = new Array();
		month[0] = "January";
		month[1] = "February";
		month[2] = "March";
		month[3] = "April";
		month[4] = "May";
		month[5] = "June";
		month[6] = "July";
		month[7] = "August";
		month[8] = "September";
		month[9] = "October";
		month[10] = "November";
		month[11] = "December";
		var content = "";
		$(".brand_name_text").html(brand_name);
		$("#brand_terms").html(brand_terms);
		var u_id = localStorage.getItem("smartSaverUID");
		var url = "https://smartsaver.in/smart_saver_web/products/productDetail";
			data = {"brand_id": id, "u_id": u_id,"payback_api": 1}
			content = '<center><div class="popoup-inner"><div class="model-body-inner">';

			//content += '<div class="action_container1 row1">';
			$.post(url,data).done(function(data) {
				var obj = JSON.parse(data);
				//alert(obj.step[0][0].Offer_step.step_type);
				var large_tick = 0;
				var act = obj.activities.length;
				var receipt_amount = obj.receipt_amount;
				var receipt_start_date = obj.receipt_start_date;
				var brand_description = obj.brand_description;
			content += '<div class="product_img" style="background:url(\'https://smartsaver.in/smart_saver/faayda_data/'+img+'\') center no-repeat;background-size:contain"></div><p>'+brand_description+'</p><div class="dev-line"></div>';

				var end_date =  d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
				$("#start_date_text").html(receipt_start_date);
				$("#end_date_text").html(end_date);

				var min_offers = obj.min_offers;
				//return false;
				//alert(receipt_amount);
				$("#activity_count").val(act);
				$("#activity_count").attr('rel',min_offers);
				var counter=0;
				var number_of_action = (obj.step.length)-1 ;
if(upload){ content +='<center><span class="upload-button" onclick="show_upload('+id+')">Upload receipt</span><p style="margin-top:3%;">Earn '+receipt_amount+' PAYBACK points</p></center><div class="dev-line"></div>';}
			content += '<center>';
			for (var i = 0,j=0 ; i < (obj.step.length); i++,j++) {
					var line='',done='',tick='';

					var offer_type = obj.step[i][0].Offer_step.step_type;
					var position = offer_type.indexOf("~");
					var offer_type = offer_type.substring(0,position);
					var offer_id = obj.offer_id[i];

					if(offer_type == 'video'){
						for (var j=0 ; j < act;j++) {
							tick = obj.activities[j].User_activity_tracking.offer_id;
							if(tick == offer_id){ done = '<img class="left popup-selected-tick" src="https://smartsaver.in/smart_saver_web/app/webroot/img/product_tick_30.png"/>';large_tick++; }
						}
						/*if(counter < number_of_action){ line = '<img class="left hide" height="100px" src="https://smartsaver.in/smart_saver_web/app/webroot/img/vertical_line.png"/>'; }*/

						content += '<div class="action_display col-md-4 col-xs-4" style="width: 33%;" id="'+offer_id+'" onclick="getAction(this.id)">'+done+'<div class="action" style="background:url(\'https://smartsaver.in/smart_saver_web/app/webroot/img/actions/video_45_p.png\') center no-repeat;background-size:contain"></div>watch video</div>'+line;
						counter++;
					}else if(offer_type == 'poll'){
						for (var j=0 ; j < act;j++) {
							tick = obj.activities[j].User_activity_tracking.offer_id;
							if(tick == offer_id){ done = '<img  class="left popup-selected-tick" src="https://smartsaver.in/smart_saver_web/app/webroot/img/product_tick_30.png"/>';large_tick++; }
						}
						/*if(counter < number_of_action){ line = '<img class="left hide" height="100px" src="https://smartsaver.in/smart_saver_web/app/webroot/img/vertical_line.png"/>'; }*/
						content += '<div class="action_display col-md-4 col-xs-4" style="width: 33%;" rel="'+obj.cash_amount[i]+'" id="'+offer_id+'" onclick="getAction(this.id)">'+done+'<div class="action" style="background:url(\'https://smartsaver.in/smart_saver_web/app/webroot/img/actions/poll_45_p.png\') center no-repeat;background-size:contain"></div>take a poll</div>'+line;
						counter++;
					}else if(offer_type == 'multi_poll'){
						for (var j=0 ; j < act;j++) {
							tick = obj.activities[j].User_activity_tracking.offer_id;
							if(tick == offer_id){ done = '<img class="left popup-selected-tick" src="https://smartsaver.in/smart_saver_web/app/webroot/img/product_tick_30.png"/>';large_tick++; }
						}
						/*if(counter < number_of_action){ line = '<img class="left hide" height="100px" src="https://smartsaver.in/smart_saver_web/app/webroot/img/vertical_line.png"/>'; }*/
						content += '<div class="action_display col-md-4 col-xs-4" style="width: 33%;" id="'+offer_id+'" onclick="getAction(this.id)">'+done+'<div class="action" style="background:url(\'https://smartsaver.in/smart_saver_web/app/webroot/img/actions/poll_45_p.png\') center no-repeat;background-size:contain"></div>take a poll</div>'+line;
						counter++;
					}else if(offer_type == 'text_article'){
						for (var j=0 ; j < act;j++) {
							tick = obj.activities[j].User_activity_tracking.offer_id;
							if(tick == offer_id){ done = '<img class="left popup-selected-tick" src="https://smartsaver.in/smart_saver_web/app/webroot/img/product_tick_30.png"/>';large_tick++; }
						}
						/*if(counter < number_of_action){ line = '<img class="left hide" height="100px" src="https://smartsaver.in/smart_saver_web/app/webroot/img/vertical_line.png"/>'; }*/
						content += '<div class="action_display col-md-4 col-xs-4" style="width: 33%;" id="'+offer_id+'" onclick="getAction(this.id)">'+done+'<div class="action" style="background:url(\'https://smartsaver.in/smart_saver_web/app/webroot/img/actions/article_45_p.png\') center no-repeat;background-size:contain"></div>learn a fact</div>'+line;
						counter++;
					}else if(offer_type == 'fb_share'){
						for (var j=0 ; j < act;j++) {
							tick = obj.activities[j].User_activity_tracking.offer_id;
							if(tick == offer_id){ done = '<img class="left popup-selected-tick" src="https://smartsaver.in/smart_saver_web/app/webroot/img/product_tick_30.png"/>';large_tick++;  }
						}
						/*if(counter < number_of_action){ line = '<img class="left hide" height="100px" src="https://smartsaver.in/smart_saver_web/app/webroot/img/vertical_line.png"/>'; }*/
						content += '<div class="action_display col-md-4 col-xs-4" style="width: 33%;" id="'+offer_id+'" onclick="getAction(this.id)"> '+done+'<div class="action" style="background:url(\'https://smartsaver.in/smart_saver_web/app/webroot/img/actions/facebook_45_p.png\') center no-repeat;background-size:contain"></div>like us</div>'+line;
						counter++;
					}else if(offer_type == 'que_ans'){
					//alert(offer_type);
						for (var j=0 ; j < act;j++) {
							tick = obj.activities[j].User_activity_tracking.offer_id;
							if(tick == offer_id){ done = '<img class="left popup-selected-tick" src="https://smartsaver.in/smart_saver_web/app/webroot/img/product_tick_30.png"/>';large_tick++;  }
						}
						/*if(counter < number_of_action){ line = '<img class="left hide" height="100px" src="https://smartsaver.in/smart_saver_web/app/webroot/img/vertical_line.png"/>'; }*/
						content += '<div class="action_display col-md-4 col-xs-4" style="width: 33%;" id="'+offer_id+'" onclick="getAction(this.id)">'+done+'<div class="action" style="background:url(\'https://smartsaver.in/smart_saver_web/app/webroot/img/actions/question_45_p.png\') center no-repeat;background-size:contain"></div>Q & A</div>'+line;
						counter++;
					}else if(offer_type == 'quiz'){
						for (var j=0 ; j < act;j++) {
							tick = obj.activities[j].User_activity_tracking.offer_id;
							if(tick == offer_id){ done = '<img class="left popup-selected-tick" src="https://smartsaver.in/smart_saver_web/app/webroot/img/product_tick_30.png"/>';large_tick++;  }
						}
						/*if(counter < number_of_action){ line = '<img class="left hide" height="100px" src="https://smartsaver.in/smart_saver_web/app/webroot/img/vertical_line.png"/>'; }*/
						content += '<div class="action_display col-md-4 col-xs-4" style="width: 33%;" id="'+offer_id+'" onclick="getAction(this.id)">'+done+'<div class="action" style="background:url(\'https://smartsaver.in/smart_saver_web/app/webroot/img/actions/quiz_45_p.png\') center no-repeat;background-size:contain"></div>take a quiz</div>'+line;
						counter++;
					}

				}
				content += '</center>';
				//if(large_tick == obj.step.length){	content +='<div id="large_tick"></div>';}
				/*if(like==1){ like='<img class="li_img'+id+' lik" src="https://smartsaver.in/smart_saver_web/app/webroot/img/liked_heart_20.png" title="remove from favorite"/>';}
				else{like='<img class="li_img'+id+' lik" src="https://smartsaver.in/smart_saver_web/app/webroot/img/heart_15.png" title="add to favorite"/>';}
				content +='<a class="col-md-1 col-xs-1 earn_text" id="like_'+id+'" onclick="like(this.id)">'+like+'</a></div></div></div></div></center>';*/
				content +='</div></div></center>';
				$(".loader").css('display','none');

				$scope.htmlcontent = $('<div/>').html(content);
				$scope.htmlcontent = $filter('rawHtml')($scope.htmlcontent);
				$scope.htmlcontent =  $compile($scope.htmlcontent)($scope);


				$("#product_display").html($scope.htmlcontent);
			});
			$("#product_popup").fadeIn( "slow" );
			//$('#product_popup').animate({scrollTop: 0},750);

			$ionicScrollDelegate.scrollTop(true);



}catch(activityPopUpErr)
{
console.log("activityPopUpErr ::"+activityPopUpErr);
}

};


$scope.show_share = function(id)
{
try{

	$(".share-option").css('display','none');
	id = id.substring(2);
	//alert(id);
	$("#share_option_"+id).css('display','block');


}catch(show_shareerr)
{
console.log("show_shareerr ::"+show_shareerr);
}

};



$scope.share_product = function(id)
{

try{


var data = $("#"+id).attr('data');
	var brand_name = $("#"+id).attr('brand_name');
	$("#fbshare_desc").val(data);
	$("#share_text").val(data);
	$("#fbshare_brand").val(brand_name);
	id = id.substring(7);
	$("#product_detail").val(id);
	$("#share-wrapper").fadeIn( "slow" );
	$("#product_popup").fadeIn( "slow" );
$ionicScrollDelegate.scrollTop(true);

}catch(share_productErr)
{
	console.log("share_productErr"+share_productErr);
}


};












    /* staticblock component controller (path: content-par/staticblock) */


    data.then(function(response) {
    var serverUrl = localStorage.getItem("serverUrl");
    $scope.htmlContent = response.data["content-par/staticblock"]["items"][0]["htmlText"];
    $scope.pageImage = response.data["content-par/staticblock"]["items"][0]["pageImage"];
    if(serverUrl != null || serverUrl != ' '){
        $scope.pageImage = serverUrl + $scope.pageImage
    }
    $scope.pageTitle = response.data["content-par/staticblock"]["items"][0]["pageTitle"];

	var div1 = document.getElementById("htmlContent");
	var decoded = $("<div/>").html($scope.htmlContent).text();


	$scope.decodedHtml =decoded;

	$scope.headerHtml ="<div class='policy-title col'>"+
                            "<img src='"+$scope.pageImage+"' alt='store' class='pop-icons'>"+
                            "<p>"+$scope.pageTitle+"</p>"+
                        "</div>";

});
$scope.headerHtml = "";
$scope.headerClass = "row row-center policy-header";
$scope.ionContentClass = "";
$rootScope.redirectedFromProductCoupons =  false;
}])









// Controller for page 'moto'
.controller('contentphonegappaybackappspaybackenhomemoto', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/moto.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'moto';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/moto';
		$rootScope.angularPageTitle = $('<div/>').html('moto').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	
	





    /* carousel component controller (path: content-par/carousel) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;


	





    /* carousel_0 component controller (path: content-par/carousel_0) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;


	





    /* gridcarousel component controller (path: content-par/gridcarousel) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/moto/jcr:content/content-par/gridcarousel";
    var gridCarouselDynamicPath = "content_par_gridcarousel";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}







$rootScope.redirectProductPath = '';
data.then(function(response){
    $scope.compoName = response.data["content-par/maincarousel"]["compName"];
    $rootScope.redirectProductPath = response.data["content-par/maincarousel"]["redirectProductPath"];

});
	





    /* gridcarousel_0 component controller (path: content-par/gridcarousel_0) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/moto/jcr:content/content-par/gridcarousel_0";
    var gridCarouselDynamicPath = "content_par_gridcarousel_0";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}

}])









// Controller for page 'offers'
.controller('contentphonegappaybackappspaybackenhomeoffers', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/offers.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'offers';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/offers';
		$rootScope.angularPageTitle = $('<div/>').html('Offers').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	






    /* homesearchbox component controller (path: content-par/homesearchbox) */
		$scope.editMode = 'DISABLED';
		
		$rootScope.redirectedFromProductCoupons =  false;
		$scope.redirectFromSearchBox = function() {
        	$rootScope.searchResultData = '';
        	$scope.go('/content/phonegap/payback/apps/payback/en/home/search');
        }

        $scope.unomerbuttonListener = function(){
            //window.unomerPhoneGapPlugin.unomerFetchSurvey(window.unomerPhoneGapPlugin.callBackFunction);
            $rootScope.weakTokenUnomer = localStorage.getItem('weaklogintoken');
            if($rootScope.weakTokenUnomer){
                window.unomerPhoneGapPlugin.unomerShowSurvey(window.unomerPhoneGapPlugin.callBackFunction);
            }else{
                $rootScope.weakLoginRedirection('/content/phonegap/payback/apps/payback/en/home');
                $scope.timeStampNum = 0;
                $rootScope.unosurveyStatus = true;
                $scope.unoSurvey = setInterval(function(){
                    $scope.timeStampNum += 1;
                    if($scope.timeStampNum > 3 && !$rootScope.unosurveyStatus){
                        clearInterval($scope.unoSurvey);
                    }
                    else{
                        if(localStorage.getItem('weaklogintoken') && $rootScope.unosurveyStatus){
                            $rootScope.unosurveyStatus = false;
                            console.log("Unomer Survey Fetch got started post login");
                            window.unomerPhoneGapPlugin.unomerShowSurvey(window.unomerPhoneGapPlugin.callBackFunction);
                        }
                    }
                }, 7000);
            }
        }	





    /* gridcarousel component controller (path: content-par/gridcarousel) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/offers/jcr:content/content-par/gridcarousel";
    var gridCarouselDynamicPath = "content_par_gridcarousel";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}
	





    /* gridcarousel_0 component controller (path: content-par/gridcarousel_0) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/offers/jcr:content/content-par/gridcarousel_0";
    var gridCarouselDynamicPath = "content_par_gridcarousel_0";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}
	





    /* carousel_0 component controller (path: content-par/carousel_0) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;



}])









// Controller for page 'Burn'
.controller('contentphonegappaybackappspaybackenhomeBurn', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/Burn.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'Burn';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/Burn';
		$rootScope.angularPageTitle = $('<div/>').html('Burn Points').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	
	





    /* carousel component controller (path: content-par/carousel) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;


	





    /* gridcarousel component controller (path: content-par/gridcarousel) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/Burn/jcr:content/content-par/gridcarousel";
    var gridCarouselDynamicPath = "content_par_gridcarousel";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}
	





    /* gridcarousel_0 component controller (path: content-par/gridcarousel_0) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/Burn/jcr:content/content-par/gridcarousel_0";
    var gridCarouselDynamicPath = "content_par_gridcarousel_0";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}
	





    /* gridcarousel_1 component controller (path: content-par/gridcarousel_1) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/Burn/jcr:content/content-par/gridcarousel_1";
    var gridCarouselDynamicPath = "content_par_gridcarousel_1";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}
	





    /* gridcarousel_2 component controller (path: content-par/gridcarousel_2) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/Burn/jcr:content/content-par/gridcarousel_2";
    var gridCarouselDynamicPath = "content_par_gridcarousel_2";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}
	





    /* carousel_1 component controller (path: content-par/carousel_1) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;


	





    /* carousel_0 component controller (path: content-par/carousel_0) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;


	





    /* carousel_2 component controller (path: content-par/carousel_2) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;



}])









// Controller for page 'earn-points'
.controller('contentphonegappaybackappspaybackenhomeearnpoints', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/earn-points.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'earn-points';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/earn-points';
		$rootScope.angularPageTitle = $('<div/>').html('Earn Points').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	
	





    /* carousel component controller (path: content-par/carousel) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;


	





    /* gridcarousel component controller (path: content-par/gridcarousel) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/earn-points/jcr:content/content-par/gridcarousel";
    var gridCarouselDynamicPath = "content_par_gridcarousel";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}






    /* staticblock component controller (path: content-par/staticblock) */


    data.then(function(response) {
    var serverUrl = localStorage.getItem("serverUrl");
    $scope.htmlContent = response.data["content-par/staticblock"]["items"][0]["htmlText"];
    $scope.pageImage = response.data["content-par/staticblock"]["items"][0]["pageImage"];
    if(serverUrl != null || serverUrl != ' '){
        $scope.pageImage = serverUrl + $scope.pageImage
    }
    $scope.pageTitle = response.data["content-par/staticblock"]["items"][0]["pageTitle"];

	var div1 = document.getElementById("htmlContent");
	var decoded = $("<div/>").html($scope.htmlContent).text();


	$scope.decodedHtml =decoded;

	$scope.headerHtml ="<div class='policy-title col'>"+
                            "<img src='"+$scope.pageImage+"' alt='store' class='pop-icons'>"+
                            "<p>"+$scope.pageTitle+"</p>"+
                        "</div>";

});
$scope.headerHtml = "";
$scope.headerClass = "row row-center policy-header";
$scope.ionContentClass = "";
$rootScope.redirectedFromProductCoupons =  false;	





    /* gridcarousel_0 component controller (path: content-par/gridcarousel_0) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/earn-points/jcr:content/content-par/gridcarousel_0";
    var gridCarouselDynamicPath = "content_par_gridcarousel_0";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}

}])









// Controller for page 'Notifications'
.controller('contentphonegappaybackappspaybackenhomeNotifications', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/Notifications.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'Notifications';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/Notifications';
		$rootScope.angularPageTitle = $('<div/>').html('Notifications').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	
	





    /* gridcarousel component controller (path: content-par/gridcarousel) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/Notifications/jcr:content/content-par/gridcarousel";
    var gridCarouselDynamicPath = "content_par_gridcarousel";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}

}])









// Controller for page 'Offer'
.controller('contentphonegappaybackappspaybackenhomeOffer', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home/Offer.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'Offer';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home/Offer';
		$rootScope.angularPageTitle = $('<div/>').html('Offer internal page').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en/home';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }

		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	
	





    /* carousel component controller (path: content-par/carousel) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;


	





    /* carousel_0 component controller (path: content-par/carousel_0) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;


	





    /* gridcarousel component controller (path: content-par/gridcarousel) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/Offer/jcr:content/content-par/gridcarousel";
    var gridCarouselDynamicPath = "content_par_gridcarousel";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}

}])









// Controller for page 'home'
.controller('contentphonegappaybackappspaybackenhome', ['$rootScope','$scope', '$http','$location','$ionicPopup','$sce','$ionicPopover','$filter','$log', '$q','branches','branchesWithCoupons','$window','$timeout','externalService','$ionicScrollDelegate','$ionicSlideBoxDelegate','$compile','$ionicModal','$ionicPlatform','$ionicHistory','$templateCache',
function($rootScope,$scope, $http,$location,$ionicPopup,$sce,$ionicPopover,$filter,$log,$q,branches,branchesWithCoupons,$window,$timeout,externalService,$ionicScrollDelegate,$ionicSlideBoxDelegate,$compile,$ionicModal,$ionicPlatform,$ionicHistory,$templateCache) {
    var data = $http.get('home.angular.json' + cacheKiller,{cache:$templateCache});
	 	$rootScope.pageName = 'home';
		$rootScope.currentpagePath = '/content/phonegap/payback/apps/payback/en/home';
		$rootScope.angularPageTitle = $('<div/>').html('Home').text();
		$rootScope.parentPagePath = '/content/phonegap/payback/apps/payback/en';
		// if(window.ADB){
  //           ADB.trackState($rootScope.pageName, {"pagePath" : $rootScope.currentpagePath});
  //       }

        if(window.ga)
        {
            window.ga.trackView($rootScope.pageName);
        }
        if (window.plugins && window.plugins.flurry) {
            var flurryParam = {pagename:""+$rootScope.pageName+"",pagepath:""+$rootScope.currentpagePath+""}
            window.plugins.flurry.logPageView(
                function(){console.log('Logged an PageView!')},
                function(){console.log('Error logging the Pageview')});
            window.plugins.flurry.logEventWithParameters("PageViews",flurryParam,
                function(){console.log('Logged an event with Params!')},
                function(){console.log('Error logging the event with Params')});
        }
        
		$scope.addHeaderClass($rootScope.currentpagePath);

		$scope.homeJsonCall();
		$rootScope.carouselData = "";
		$scope.getStrongToken();

		$ionicPlatform.registerBackButtonAction(function (event) {
		 if($ionicHistory) {
		  if ($ionicHistory.currentStateName() == "whatd do I put here?") {

		  } else {
			if($rootScope.currentpagePath == "/content/phonegap/payback/apps/payback/en/home"){
				event.preventDefault();
				navigator.app.exitApp();
				}

		   }
		   }
		   }, 100);

	
	





    /* headerpar component controller (path: headerpar) */


data.then(function(response) {

$rootScope.headerServerUrl = localStorage.getItem("serverUrl");
$rootScope.productRedirectionPath = response.data['headerpar']['productRedirectionPagePath'];

});

$scope.header = "";
$scope.headerClass = "bar-subheader home-subheader bar bar-header";






	 
	 
	 
	
	 
	 
    /* footerpar component controller (path: footerpar) */
    data.then(function(response) {

    	
        $scope.footerResponse = response.data["footerpar"].items;
        
       


      


    });

	$rootScope.redirectedFromProductCoupons =  false;





    





    /* homesearchbox component controller (path: content-par/homesearchbox) */
		$scope.editMode = 'DISABLED';
		
		$rootScope.redirectedFromProductCoupons =  false;
		$scope.redirectFromSearchBox = function() {
        	$rootScope.searchResultData = '';
        	$scope.go('/content/phonegap/payback/apps/payback/en/home/search');
        }

        $scope.unomerbuttonListener = function(){
            //window.unomerPhoneGapPlugin.unomerFetchSurvey(window.unomerPhoneGapPlugin.callBackFunction);
            $rootScope.weakTokenUnomer = localStorage.getItem('weaklogintoken');
            if($rootScope.weakTokenUnomer){
                window.unomerPhoneGapPlugin.unomerShowSurvey(window.unomerPhoneGapPlugin.callBackFunction);
            }else{
                $rootScope.weakLoginRedirection('/content/phonegap/payback/apps/payback/en/home');
                $scope.timeStampNum = 0;
                $rootScope.unosurveyStatus = true;
                $scope.unoSurvey = setInterval(function(){
                    $scope.timeStampNum += 1;
                    if($scope.timeStampNum > 3 && !$rootScope.unosurveyStatus){
                        clearInterval($scope.unoSurvey);
                    }
                    else{
                        if(localStorage.getItem('weaklogintoken') && $rootScope.unosurveyStatus){
                            $rootScope.unosurveyStatus = false;
                            console.log("Unomer Survey Fetch got started post login");
                            window.unomerPhoneGapPlugin.unomerShowSurvey(window.unomerPhoneGapPlugin.callBackFunction);
                        }
                    }
                }, 7000);
            }
        }	





    /* carousel_0 component controller (path: content-par/carousel_0) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;


	





    /* gridcarousel component controller (path: content-par/gridcarousel) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/jcr:content/content-par/gridcarousel";
    var gridCarouselDynamicPath = "content_par_gridcarousel";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}
	





    /* gridcarousel_0 component controller (path: content-par/gridcarousel_0) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/jcr:content/content-par/gridcarousel_0";
    var gridCarouselDynamicPath = "content_par_gridcarousel_0";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}
	





    /* carousel component controller (path: content-par/carousel) */


data.then(function(response) {


});




$rootScope.redirectedFromProductCoupons =  false;
$rootScope.showNetwork = false;


	





    /* gridcarousel_2 component controller (path: content-par/gridcarousel_2) */


data.then(function(response) {
});

    var gridCarouseResourceValue = "/content/phonegap/payback/apps/payback/en/home/jcr:content/content-par/gridcarousel_2";
    var gridCarouselDynamicPath = "content_par_gridcarousel_2";



$rootScope.redirectedFromProductCoupons =  false;

$scope.updateSlider = function () {
    $ionicSlideBoxDelegate.update(); //or just return the function
}

}])



}(angular, document));