package com.adobe.cq.mobile.payback;

import android.content.Context;

import com.unomer.sdk.Unomer;
import com.unomer.sdk.UnomerListener;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by mayank on 7/6/16.
 */
public class UnomerPhoneGapPlugin extends CordovaPlugin {

    private static final String TAG   = "CustomPlugin";

    private CallbackContext callbackContext = null;
    private MainActivity mainActivity = null; // MainActivity is the earlier PAYBACK class

    Unomer survey;
    UnomerListener uListener;
    Context context;
    String unomerPublisherID;
    String unomerAppID;

    /**
     * Override the plugin initialise method and set the Activity as an
     * instance variable.
     */
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView)
    {
        super.initialize(cordova, webView);

        // Set the Activity.
        this.mainActivity = (MainActivity) cordova.getActivity();
        context = mainActivity;

        //Real IDs
        unomerPublisherID   = "gn+6OPuyUb4T2duTSnxVzg==";
        unomerAppID         = "wjzCufhMrIpWE3PcVsTLoxqOiJGdB5";

        // REPLACE THE FOLLOWING TEST IDs WITH YOUR REAL IDs WHEN GOING LIVE
        //unomerPublisherID   = "fzwT6LXmQBgzHH9+qJP1dA==";
        //unomerAppID         = "CSAtYF4ep8zQEjmKiNOh1MuWG2DbXg";
        //unomerPublisherID   = "V6JESZQIn2PU6LO8HVlGwQ==";
        //unomerAppID         = "JDBlEXquzt9TMixLWj08dU7s3wkH5N";

        unomerInit();

    }

    /**
     * Here you can delegate any JavaScript methods. The "action" argument will contain the
     * name of the delegated method and the "args" will contain any arguments passed from the
     * JavaScript method.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException
    {
        this.callbackContext = callbackContext;

        if (action.equals("fetchUnomerSurvey")){
            this.fetchUnomerSurvey(args.getJSONObject(0).getString("location"));
        } else if(action.equals("showUnomerSurvey")){
            this.showUnomerSurvey(args.getJSONObject(0).getString("location"));
        } else if(action.equals("setLCN")){
	    survey.setParam("userName", args.getJSONObject(0).getString("LCN")); // pass your LCN here
	} else {
            return false;
        }

        return true;
    }


    private void callBackMethod(JSONObject parameter){
       PluginResult result = new PluginResult(PluginResult.Status.OK, parameter);
       result.setKeepCallback(true);
       callbackContext.sendPluginResult(result);
    }


    public void fetchUnomerSurvey(String location) {
        survey.fetchSurvey();

    }
    public void showUnomerSurvey(String location) {
        survey.showSurvey();
    }

    public void unomerInit() {

        if (survey == null) {



            uListener = new UnomerListener() {
                @Override
                public void unomerSurveyFetchStarted() {
                    // TODO Auto-generated method stub
                    makeResponseForCallBack("Default", "fetch_started", 0, "", "", false, 0, "", false);
                }

                @Override
                public void unomerSurveyFetchSuccess(boolean isRewardEnabledOnThisSurvey, int reward, String rewardCurrency, int noOfQuestions) {
                    // TODO Auto-generated method stub
                    makeResponseForCallBack("Default", "fetch_success", reward, rewardCurrency, "", false, noOfQuestions, "", isRewardEnabledOnThisSurvey);
                }

                @Override
                public void unomerSurveyFetchFailed(String eMsg) {
                    // TODO Auto-generated method stub
                    makeResponseForCallBack("Default", "fetch_failed", 0, "", "", false, 0, eMsg, false);
                }

                @Override
                public void unomerSurveyDisplayed() {
                    // TODO Auto-generated method stub
                    makeResponseForCallBack("Default", "survey_displayed", 0, "", "", false, 0, "", false);
                }

                @Override
                public void unomerSurveyClosed() {
                    // TODO Auto-generated method stub
                    makeResponseForCallBack("Default", "survey_closed", 0, "", "", false, 0, "", false);
                }

                @Override
                public void unomerUploadComplete(int reward, String rewardCurrency, String responseCode, boolean isRewarded) {
                    makeResponseForCallBack("Default", "survey_upload_complete", reward, rewardCurrency, responseCode, isRewarded, 0, "", false);
                }

                @Override
                public void unomerSurveyConfirmationDeclined() {
                    makeResponseForCallBack("Default", "survey_confirm_declined", 0, "", "", false, 0, "", false);
                }

                @Override
                public void unomerUploadFailed() {
                    makeResponseForCallBack("Default", "survey_upload_failed", 0, "", "", false, 0, "", false);
                }
                @Override
                public void unomerUserDisqualified(int reward, String rewardCurrency, String responseCode, boolean isRewarded) {
                    makeResponseForCallBack("Default", "survey_user_disqualified", reward, rewardCurrency, responseCode, isRewarded, 0, "", false);
                }


            };

            survey = new Unomer(this.mainActivity, unomerPublisherID, unomerAppID, uListener); // mayanks pc - live survey
            survey.setParam("showUploadConfirmationPopUp", "no");      // to switch off confirmation for uploading of survey response
            survey.setParam("confirmationOnIncentiveSurveys", "false"); // to switch off confirmation message for taking survey
            


        }
    }


    private void makeResponseForCallBack(String location, String status, int reward,
                                         String rewardCurrency, String responseCode,
                                         boolean isRewarded, int nuOfQuestions,
                                         String eMsg, boolean isRewardEnabledOnThisSurvey){
        try {
            JSONObject parameter = new JSONObject();
            parameter.put("location", "Default");
            parameter.put("status", status);
            parameter.put("reward", reward);
            parameter.put("rewardCurrency", rewardCurrency);
            parameter.put("responseCode", responseCode);
            parameter.put("isRewarded", isRewarded);
            parameter.put("numberOfQuestions", nuOfQuestions);
            parameter.put("message", eMsg);
            parameter.put("isRewardEnabledOnThisSurvey", isRewardEnabledOnThisSurvey);

            callBackMethod(parameter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
